package com.al.service.ServiceMandiAutomation;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import io.appium.java_client.android.AndroidDriver;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.net.URL;
import java.util.Set;
import org.openqa.selenium.By;
import java.util.concurrent.TimeUnit;

public class ServiceRequest {

	private static DesiredCapabilities desiredCapabilities;
	protected static AndroidDriver fleetManagerDriver = null;
	protected static WebDriverWait fmWait = null;

	@BeforeTest
	public void setup() throws IOException, InterruptedException {

		try {
			desiredCapabilities = new DesiredCapabilities();
			desiredCapabilities.setCapability("deviceName", "Samsung SM-J120G"); // Fleet
																					// Manager
			desiredCapabilities.setCapability("appium-version", "1.7.1");
			desiredCapabilities.setCapability(MobileCapabilityType.UDID, "32011c38412e3479"); // Deepa
			// desiredCapabilities.setCapability(MobileCapabilityType.UDID,
			// "42005e66ec7524d3"); //Santa
			// desiredCapabilities.setCapability(MobileCapabilityType.UDID,
			// "420015a3b2fa1449"); //Priya FM
			desiredCapabilities.setCapability("platformName", "Android");
			desiredCapabilities.setCapability("platformVersion", "5.1.1");
			desiredCapabilities.setCapability("recreateChromeDriverSessions", "true");
			desiredCapabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, "100000");
			desiredCapabilities.setCapability("appPackage", "com.servicemandi.fm");
			desiredCapabilities.setCapability("appActivity", "com.servicemandi.fm.MainActivity");
			desiredCapabilities.setCapability("automationName", "uiautomator2");
			desiredCapabilities.setCapability("autoAcceptAlerts", true);
			desiredCapabilities.setCapability("autoDismissAlerts", true);
			fleetManagerDriver = new AndroidDriver(new URL("http://0.0.0.0:4723/wd/hub"), desiredCapabilities);

			fleetManagerDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			fmWait = new WebDriverWait(fleetManagerDriver, 120);
		} catch (Exception e) {
		}
		Thread.sleep(5000);
		switchToFMWebView();

	}

	@Test
	public void flow() throws Exception {

		try {
			Thread.sleep(10000);
			fmWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[contains(text(), 'English')]")));
			fleetManagerDriver.findElement(By.xpath("//*[contains(text(), 'English')]")).click();
			Thread.sleep(2000);
			fmWait.until(ExpectedConditions
					.visibilityOfElementLocated(By.xpath("//html/body/ion-app/ng-component/ion-nav/page-intro/ion-footer/div/button")));
			fleetManagerDriver.findElement(By.xpath("//html/body/ion-app/ng-component/ion-nav/page-intro/ion-footer/div/button")).click();
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
	}

	public static void switchToFMWebView() {

		Set<String> availableContexts = fleetManagerDriver.getContextHandles();
		for (String context : availableContexts) {
			if (context.contains("WEBVIEW_com.servicemandi.fm")) {
				fleetManagerDriver.context(context);
				System.out.println("Context switched to web view ::: " + context);
				break;
			}
		}
	}

	@AfterMethod
	public void quit() {

	}
}
