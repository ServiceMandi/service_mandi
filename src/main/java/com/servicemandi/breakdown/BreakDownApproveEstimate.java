package com.servicemandi.breakdown;

import com.relevantcodes.extentreports.LogStatus;
import com.servicemandi.common.Configuration;
import com.servicemandi.common.FleetManager;
import com.servicemandi.common.LocatorPath;
import com.servicemandi.utils.ExcelInputData;
import com.servicemandi.utils.Utils;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import io.appium.java_client.android.AndroidKeyCode;

/**
 * Created by Shanthakumar on 30-01-2018.
 */

public class BreakDownApproveEstimate extends Configuration {

	public BreakDownApproveEstimate() {

	}

	public void approveEstimateWithPayCash(ExcelInputData excelInputData, FleetManager fleetManager) {

		try {

			fleetManager.selectLiveOrderSearch(excelInputData.getVehicleNumber());
			System.out.println("Entered Approve Estimate method ::");
			Thread.sleep(2000);
			fleetManagerWait(LocatorPath.approveEstimatePath);
			clickFMElement(LocatorType.XPATH, LocatorPath.approveEstimatePath);
			writeReport(LogStatus.PASS, "Fleet manager approve the estimate");
			Thread.sleep(1000);
			fleetManagerWait(LocatorPath.payCashButtonPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.payCashButtonPath);
			writeReport(LogStatus.PASS, "Fleet manager select pay cash method");
			fleetManagerWait(LocatorPath.liveOrderTab);

		} catch (Exception e) {
			writeReport(LogStatus.PASS, "Fleet manager select pay cash method failed");
		}
	}

	public void approveEstimateCancelAllJobs(ExcelInputData excelInputData, FleetManager fleetManager) {

		try {

			fleetManager.selectLiveOrderSearch(excelInputData.getVehicleNumber());
			System.out.println("Entered Approve Estimate method ::");
			Thread.sleep(3000);
			fleetManagerWait(LocatorPath.approveEstimatePath);
			System.out.println("Al Enter the cancel job method :::");
			int loopSize = Integer.parseInt(excelInputData.getMultiJobCount());
			for (int i = 1; i <= loopSize; i++) {

				String cancelCheckBox = "//ion-content/div[2]/ion-grid/div/ion-list/ion-row[" + i
						+ "]/ion-col[4]/ion-checkbox/div";
				System.out.println("AL cancel jobs path ::" + cancelCheckBox);
				WebElement cancelElement = fleetManagerDriver.findElement(By.xpath(cancelCheckBox));
				Actions action = new Actions(fleetManagerDriver);
				action.moveToElement(cancelElement).click().perform();
				Thread.sleep(500);
			}

			clickFMElement(LocatorType.XPATH, LocatorPath.approveEstimatePath);
			fleetManagerWait(LocatorPath.serviceYesBtn);
			clickFMElement(LocatorType.XPATH, LocatorPath.serviceYesBtn);
			writeReport(LogStatus.PASS, "Fleet manager cancel all jobs");
			fleetManagerWait(LocatorPath.liveOrderTab);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void approveEstimateCancelOneJobs(ExcelInputData excelInputData, FleetManager fleetManager) {

		try {

			fleetManager.selectLiveOrderSearch(excelInputData.getVehicleNumber());
			System.out.println("Entered Approve Estimate method ::");
			Thread.sleep(3000);
			fleetManagerWait(LocatorPath.approveEstimatePath);
			System.out.println("Al Enter the cancel job method :::");

			String cancelCheckBox = "//ion-content/div[2]/ion-grid/div/ion-list/ion-row[1]/ion-col[4]/ion-checkbox/div";
			System.out.println("AL cancel jobs path ::" + cancelCheckBox);
			WebElement cancelElement = fleetManagerDriver.findElement(By.xpath(cancelCheckBox));
			Actions action = new Actions(fleetManagerDriver);
			action.moveToElement(cancelElement).click().perform();
			Thread.sleep(500);

			clickFMElement(LocatorType.XPATH, LocatorPath.approveEstimatePath);
			writeReport(LogStatus.PASS, "Fleet manager cancel one jobs");
			fleetManagerWait(LocatorPath.liveOrderTab);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void approveEstimateWithoutPayCash(ExcelInputData excelInputData, FleetManager fleetManager) {

		try {
			fleetManager.selectLiveOrderSearch(excelInputData.getVehicleNumber());
			System.out.println("Entered Approve Estimate method ::");
			Thread.sleep(3000);
			fleetManagerWait(LocatorPath.approveEstimatePath);
			clickFMElement(LocatorType.XPATH, LocatorPath.approveEstimatePath);
			writeReport(LogStatus.PASS, "Fleet manager approved the estimate");
			Thread.sleep(1000);
			fleetManagerWait(LocatorPath.liveOrderTab);

		} catch (Exception e) {
			writeReport(LogStatus.PASS, "Fleet manager select pay cash method failed");
		}
	}

	public void approveEstimateCancelOrder(ExcelInputData excelInputData, FleetManager fleetManager) {

		try {
			fleetManager.selectLiveOrderSearch(excelInputData.getVehicleNumber());
			System.out.println("Entered Cancel Order method ::");
			Thread.sleep(2000);
			fleetManagerWait(LocatorPath.ApproveEstChkbox);
			clickFMElement(LocatorType.XPATH, LocatorPath.ApproveEstChkbox);
			try {
				Thread.sleep(2000);
				WebElement ApproveestChkboxElement = fleetManagerDriver
						.findElement(By.xpath(LocatorPath.ApproveEstChkbox));
				Thread.sleep(2000);
				if (ApproveestChkboxElement.isSelected()) {
					System.out.println("Approve Estimate checkbox is Unchecked");
				}

			} catch (Exception e) {
				writeReport(LogStatus.FAIL, "Fleet manager Approve estimate Cancel Order checkbox click failed");
			}
			fleetManagerWait(LocatorPath.CancelOrder);
			clickFMElement(LocatorType.XPATH, LocatorPath.CancelOrder);
			String yesBtn = "//html/body/ion-app/ion-alert/div/div[3]/button[1]/span";
			fleetManagerWait(yesBtn);
			clickFMElement(LocatorType.XPATH, yesBtn);
			Thread.sleep(2000);
			fleetManagerDriver.pressKeyCode(AndroidKeyCode.ENTER);
			writeReport(LogStatus.PASS, "Fleet manager Cancelled Order at Approved Estimate");
			Thread.sleep(1000);
			fleetManagerWait(LocatorPath.liveOrderTab);

		} catch (Exception e) {
			writeReport(LogStatus.FAIL, "Fleet manager select pay cash method failed");
		}
	}

	public void approveEstimateWithAlert(ExcelInputData excelInputData, FleetManager fleetManager) {

		try {
			Utils.sleepTimeLow();
			fleetManager.selectLiveOrderSearch(excelInputData.getVehicleNumber());
			System.out.println("Entered Approve Estimate method ::");
			Thread.sleep(2000);
			fleetManagerWait(LocatorPath.approveEstimatePath);
			clickFMElement(LocatorType.XPATH, LocatorPath.approveEstimatePath);
			writeReport(LogStatus.PASS, "Fleet manager approve the estimate");
			Thread.sleep(2000);
			fleetManagerWait(LocatorPath.noRetailerAlertPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.noRetailerAlertPath);
			Thread.sleep(2000);
			writeReport(LogStatus.PASS, "Fleet manager select pay cash method");
			fleetManagerWait(LocatorPath.liveOrderTab);
		} catch (Exception e) {

		}
	}

	public void ApproveRevisedEstimateWithPayCash(ExcelInputData excelInputData, FleetManager fleetManager) {

		try {

			fleetManager.selectLiveOrderSearch(excelInputData.getVehicleNumber());
			System.out.println("Entered Approve Revised Estimate Without Pay method ::");
			Thread.sleep(3000);
			fleetManagerWait(LocatorPath.approveRevisedEstimatePath);
			clickFMElement(LocatorType.XPATH, LocatorPath.approveRevisedEstimatePath);
			Thread.sleep(2000);
			fleetManagerWait(LocatorPath.payCashButtonPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.payCashButtonPath);
			fleetManagerWait(LocatorPath.liveOrderTab);
		} catch (Exception e) {

		}
	}

	public void ApproveRevisedEstimateWithAlert(ExcelInputData excelInputData, FleetManager fleetManager) {

		try {

			fleetManager.selectLiveOrderSearch(excelInputData.getVehicleNumber());
			System.out.println("Entered Approve Revised Estimate Without Pay method ::");
			Thread.sleep(3000);
			fleetManagerWait(LocatorPath.approveRevisedEstimatePath);
			clickFMElement(LocatorType.XPATH, LocatorPath.approveRevisedEstimatePath);
			Utils.sleepTimeLow();
			fleetManagerWait(LocatorPath.noRetailerAlertPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.noRetailerAlertPath);
			Thread.sleep(2000);
			writeReport(LogStatus.PASS, "Fleet manager select pay cash method");
			fleetManagerWait(LocatorPath.liveOrderTab);
		} catch (Exception e) {

		}
	}

	public void ApproveRevisedEstimateWithoutPay(ExcelInputData excelInputData, FleetManager fleetManager) {

		try {
			fleetManager.selectLiveOrderSearch(excelInputData.getVehicleNumber());
			System.out.println("Entered Approve Revised Estimate Without Pay method ::");
			Thread.sleep(3000);
			fleetManagerWait(LocatorPath.approveRevisedEstimatePath);
			clickFMElement(LocatorType.XPATH, LocatorPath.approveRevisedEstimatePath);
			fleetManagerWait(LocatorPath.liveOrderTab);
		} catch (Exception e) {

		}
	}
}
