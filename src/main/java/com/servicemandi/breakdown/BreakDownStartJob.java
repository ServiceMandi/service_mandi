package com.servicemandi.breakdown;

import com.relevantcodes.extentreports.LogStatus;
import com.servicemandi.common.Configuration;
import com.servicemandi.common.LocatorPath;
import com.servicemandi.common.Mechanic;
import com.servicemandi.utils.ExcelInputData;
import com.servicemandi.utils.Utils;
import java.util.List;

import io.appium.java_client.android.AndroidKeyCode;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 * Created by Shanthakumar on 30-01-2018.
 */

public class BreakDownStartJob extends Configuration {

	public BreakDownStartJob() {
	}

	public void startJobs(ExcelInputData excelInputData, Mechanic mechanic) {

		try {

			mechanic.onGoingOrderPathClick(excelInputData.getVehicleNumber());
			Utils.sleepTimeLow();
			System.out.println("Enter the service request start jobs method :::");
			mechanicWait(LocatorPath.jobsCompletePath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.jobsCompletePath);
			writeReport(LogStatus.PASS, "Mechanic jobs completed");
			Utils.sleepTimeLow();
			System.out.println("Al Enter the submit bill path ::: ");
			/*
			 * mechanicWait(LocatorPath.jobSubmitBillPath);jobSubmitButtonPath
			 * clickMechanicElement(LocatorType.XPATH,
			 * LocatorPath.jobSubmitBillPath);
			 */
			mechanicWait(LocatorPath.jobSubmitButtonPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.jobSubmitButtonPath);
			Thread.sleep(2000);
			mechanicWait(LocatorPath.alertOkPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.alertOkPath);
			writeReport(LogStatus.PASS, "Mechanic jobs submitted");
		} catch (Exception e) {
			writeReport(LogStatus.FAIL, "Mechanic jobs submitted failed");
		}
	}

	public void revisedEstimationWithParts(ExcelInputData excelInputData, Mechanic mechanic) {

		try {

			mechanic.onGoingOrderPathClick(excelInputData.getVehicleNumber());
			Utils.sleepTimeLow();
			System.out.println("Enter the service request start jobs method :::");
			mechanicWait(LocatorPath.addJobsLinkPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.addJobsLinkPath);
			Thread.sleep(1000);
			mechanicWait(LocatorPath.selectJobPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.selectJobPath);
			Thread.sleep(1000);
			mechanicWait(LocatorPath.totalJobListPath);
			List<WebElement> jobList = mechanicDriver.findElements(By.xpath(LocatorPath.totalJobListPath));

			for (int i = 0; i < jobList.size(); i++) {
				String selectJob = jobList.get(i).getText();
				System.out.println("Inside for loop::::" + selectJob);
				if (selectJob.equalsIgnoreCase(excelInputData.getBdJobSelection())) {
					jobList.get(i - 1).click();
					writeReport(LogStatus.PASS, "Mechanic select job :-" + excelInputData.getBdJobSelection());
					Thread.sleep(1000);
					break;
				}
			}

			Thread.sleep(1000);
			mechanicDriver.pressKeyCode(AndroidKeyCode.ENTER);
			Thread.sleep(1000);

			// EnterLabour:
			String labourPath = "//*[@id=\"nav\"]/page-addjobs/ion-content/div[2]/ion-card[2]/div/ion-row[2]/ion-col[2]/input";

			mechanicWait(labourPath);
			Thread.sleep(1000);
			clickMechanicElement(LocatorType.XPATH, labourPath);
			enterValueMechanic(LocatorType.XPATH, labourPath, excelInputData.getLabourAmount());
			Thread.sleep(1000);

			// EnterParts:
			String partsPath = "//*[@id=\"nav\"]/page-addjobs/ion-content/div[2]/ion-card[2]/div/ion-row[2]/ion-col[3]/input";
			mechanicWait(partsPath);
			Thread.sleep(1000);
			clickMechanicElement(LocatorType.XPATH, partsPath);
			Thread.sleep(1000);
			enterValueMechanic(LocatorType.XPATH, partsPath, excelInputData.getEnterParts());
			hideKeyBoard(mechanicDriver);

			mechanicWait(LocatorPath.submitRevisedEstimatePath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.submitRevisedEstimatePath);
			System.out.println("Enter the Break down parts :::");
			writeReport(LogStatus.PASS, " Revised Estimate with patrs " + excelInputData.getEnterParts() + " entered ");
			/*
			 * Utils.sleepTimeLow();
			 * mechanicWait(LocatorPath.revisedPaymentRequestOk);
			 * clickMechanicElement(LocatorType.XPATH,
			 * LocatorPath.revisedPaymentRequestOk); Thread.sleep(3000);
			 */

		} catch (Exception e) {

		}
	}

	public void revisedEstimationWithOutParts(ExcelInputData excelInputData, Mechanic mechanic) {

		try {

			mechanic.onGoingOrderPathClick(excelInputData.getVehicleNumber());
			Thread.sleep(5000);
			System.out.println("Enter the service request start jobs method :::");
			mechanicWait(LocatorPath.addJobsLinkPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.addJobsLinkPath);
			Thread.sleep(2000);
			mechanicWait(LocatorPath.selectJobPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.selectJobPath);
			Thread.sleep(1000);
			mechanicWait(LocatorPath.totalJobListPath);
			List<WebElement> jobList = mechanicDriver.findElements(By.xpath(LocatorPath.totalJobListPath));

			for (int i = 0; i < jobList.size(); i++) {
				String selectJob = jobList.get(i).getText();
				System.out.println("Inside for loop::::" + selectJob);
				if (selectJob.equalsIgnoreCase(excelInputData.getBdJobSelection())) {
					jobList.get(i - 1).click();
					writeReport(LogStatus.PASS, "Mechanic select job :- " + excelInputData.getBdJobSelection());
					Thread.sleep(1000);
					break;
				}
			}

			Thread.sleep(1000);
			mechanicDriver.pressKeyCode(AndroidKeyCode.ENTER);
			Thread.sleep(2000);

			// EnterLabour:
			String labourPath = "//*[@id=\"nav\"]/page-addjobs/ion-content/div[2]/ion-card[2]/div/ion-row[2]/ion-col[2]/input";

			mechanicWait(labourPath);
			Thread.sleep(1000);
			clickMechanicElement(LocatorType.XPATH, labourPath);
			enterValueMechanic(LocatorType.XPATH, labourPath, excelInputData.getLabourAmount());
			Thread.sleep(1000);

			/*
			 * //EnterParts: String partsPath = "/
			 *//*
				 * [@id=\
				 * "nav\"]/page-addjobs/ion-content/div[2]/ion-card[2]/div/ion-row[2]/ion-col[3]/input";
				 * mechanicWait(partsPath); Thread.sleep(1000);
				 * clickMechanicElement(LocatorType.XPATH, partsPath);
				 * Thread.sleep(1000); enterValueMechanic(LocatorType.XPATH,
				 * partsPath, "300");
				 */
			hideKeyBoard(mechanicDriver);

			mechanicWait(LocatorPath.submitRevisedEstimatePath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.submitRevisedEstimatePath);
			System.out.println("Enter the Break down parts :::");
			writeReport(LogStatus.PASS, " Revised Estimate with out patrs ");

			/*
			 * Thread.sleep(2000);
			 * mechanicWait(LocatorPath.revisedPaymentRequestOk);
			 * clickMechanicElement(LocatorType.XPATH,
			 * LocatorPath.revisedPaymentRequestOk);
			 */

		} catch (Exception e) {

		}
	}
}
