package com.servicemandi.breakdown;

import com.relevantcodes.extentreports.LogStatus;
import com.servicemandi.common.Configuration;
import com.servicemandi.common.LocatorPath;
import com.servicemandi.common.Mechanic;
import com.servicemandi.common.Configuration.LocatorType;
import com.servicemandi.utils.ExcelInputData;
import com.servicemandi.utils.Utils;
import java.util.List;

import io.appium.java_client.android.AndroidKeyCode;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 * Created by Shanthakumar on 30-01-2018.
 */

public class BreakDownTrackLocation extends Configuration {

	public BreakDownTrackLocation() {

	}

	public void giveEstimate(ExcelInputData excelInputData, Mechanic mechanic) {

		try {

			System.out.println("Al Enter the give Estimate method :::");
			mechanic.onGoingOrderPathClick(excelInputData.getVehicleNumber());
			Thread.sleep(2000);
			mechanicWait(LocatorPath.giveEstimatePath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.giveEstimatePath);
			writeReport(LogStatus.PASS, "Mechanic Track location completed ");
		} catch (Exception e) {
			writeReport(LogStatus.FAIL, "Mechanic Track location failed ");
		}
	}

	public void trackLocationWithParts(ExcelInputData excelInputData) {

		try {

			Thread.sleep(3000);
			mechanicWait(LocatorPath.selectJobPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.selectJobPath);
			Thread.sleep(1000);
			mechanicWait(LocatorPath.totalJobListPath);
			List<WebElement> jobList = mechanicDriver.findElements(By.xpath(LocatorPath.totalJobListPath));

			for (int i = 0; i < jobList.size(); i++) {
				String selectJob = jobList.get(i).getText();
				System.out.println("Inside for loop::::" + selectJob);
				if (selectJob.equalsIgnoreCase(excelInputData.getBdJobSelection())) {
					jobList.get(i).click();
					writeReport(LogStatus.PASS, "Mechanic select job :- " + excelInputData.getBdJobSelection());
					Thread.sleep(1000);
					break;
				}
			}

			Thread.sleep(1000);
			mechanicDriver.pressKeyCode(AndroidKeyCode.ENTER);
			Thread.sleep(1000);

			mechanicWait(LocatorPath.trackLocationLabourPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.trackLocationLabourPath);
			enterValueMechanic(LocatorType.XPATH, LocatorPath.trackLocationLabourPath,
					excelInputData.getLabourAmount());
			Thread.sleep(1000);
			writeReport(LogStatus.PASS, "Mechanic enter the labour amount :- "
					+ getMechanicWebElement(LocatorType.XPATH, LocatorPath.trackLocationLabourPath).getText());

			mechanicWait(LocatorPath.trackLocationPartsPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.trackLocationPartsPath);
			enterValueMechanic(LocatorType.XPATH, LocatorPath.trackLocationPartsPath, excelInputData.getEnterParts());
			hideKeyBoard(mechanicDriver);
			writeReport(LogStatus.PASS, "Mechanic enter the parts value "
					+ getMechanicWebElement(LocatorType.XPATH, LocatorPath.trackLocationPartsPath).getText());

			mechanicWait(LocatorPath.partsSubmitPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.partsSubmitPath);
			System.out.println("Enter the Break down parts :::");
			writeReport(LogStatus.PASS, "Service request parts :- " + excelInputData.getEnterParts() + " entered ");
		} catch (Exception e) {
			writeReport(LogStatus.ERROR, " Track Location  failed");
		}
	}

	public void trackLocationWithMultiJobs(ExcelInputData excelInputData) {

		try {

			Thread.sleep(3000);
			mechanicWait(LocatorPath.selectJobPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.selectJobPath);
			Thread.sleep(1000);
			int loopSize = Integer.parseInt(excelInputData.getMultiJobCount());
			for (int j = 0; j < loopSize; j++) {

				mechanicWait(LocatorPath.totalJobListPath);
				List<WebElement> jobList = mechanicDriver.findElements(By.xpath(LocatorPath.totalJobListPath));

				for (int i = 0; i < jobList.size(); i++) {
					String selectJob = jobList.get(i).getText();
					System.out.println("Inside for loop::::" + selectJob);

					if (i == j) {
						jobList.get(i).click();
						Thread.sleep(500);
						writeReport(LogStatus.PASS, "Mechanic select job :- " + jobList.get(i).getText());
						break;
					}
				}

				Thread.sleep(500);
				mechanicDriver.pressKeyCode(AndroidKeyCode.ENTER);
				Thread.sleep(500);

				mechanicWait(LocatorPath.trackLocationLabourPath);
				clickMechanicElement(LocatorType.XPATH, LocatorPath.trackLocationLabourPath);
				enterValueMechanic(LocatorType.XPATH, LocatorPath.trackLocationLabourPath,
						excelInputData.getLabourAmount());
				Thread.sleep(500);
				writeReport(LogStatus.PASS, "Mechanic enter the labour amount "
						+ getMechanicWebElement(LocatorType.XPATH, LocatorPath.trackLocationLabourPath).getText());

				mechanicWait(LocatorPath.trackLocationPartsPath);
				clickMechanicElement(LocatorType.XPATH, LocatorPath.trackLocationPartsPath);
				enterValueMechanic(LocatorType.XPATH, LocatorPath.trackLocationPartsPath,
						excelInputData.getEnterParts());
				hideKeyBoard(mechanicDriver);
				writeReport(LogStatus.PASS, "Mechanic enter the parts value "
						+ getMechanicWebElement(LocatorType.XPATH, LocatorPath.trackLocationPartsPath).getText());

				if (j < (loopSize - 1)) {
					Thread.sleep(1000);
					String addJobButton = "//*[@id=\"nav\"]/page-addjobs/ion-footer/ion-row";
					clickMechanicElement(LocatorType.XPATH, addJobButton);
					Thread.sleep(2000);
					mechanicWait(LocatorPath.selectJobPath);
					clickMechanicElement(LocatorType.XPATH, LocatorPath.selectJobPath);
				}
			}

			Thread.sleep(1000);
			mechanicWait(LocatorPath.partsSubmitPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.partsSubmitPath);
			Thread.sleep(3000);

		} catch (Exception e) {

		}
	}

	public void trackLocationWithMultiJobsWithOutParts(ExcelInputData excelInputData) {

		try {

			Thread.sleep(3000);
			mechanicWait(LocatorPath.selectJobPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.selectJobPath);
			Thread.sleep(1000);
			int loopSize = Integer.parseInt(excelInputData.getMultiJobCount());
			for (int j = 0; j < loopSize; j++) {

				mechanicWait(LocatorPath.totalJobListPath);
				List<WebElement> jobList = mechanicDriver.findElements(By.xpath(LocatorPath.totalJobListPath));

				for (int i = 0; i < jobList.size(); i++) {
					String selectJob = jobList.get(i).getText();
					System.out.println("Inside for loop::::" + selectJob);

					if (i == j) {
						jobList.get(i).click();
						Thread.sleep(500);
						writeReport(LogStatus.PASS, "Mechanic select job :- " + jobList.get(i).getText());
						break;
					}
				}

				Thread.sleep(500);
				mechanicDriver.pressKeyCode(AndroidKeyCode.ENTER);
				Thread.sleep(500);

				mechanicWait(LocatorPath.trackLocationLabourPath);
				clickMechanicElement(LocatorType.XPATH, LocatorPath.trackLocationLabourPath);
				enterValueMechanic(LocatorType.XPATH, LocatorPath.trackLocationLabourPath,
						excelInputData.getLabourAmount());
				Thread.sleep(500);

				writeReport(LogStatus.PASS, "Mechanic enter the labour amount "
						+ getMechanicWebElement(LocatorType.XPATH, LocatorPath.trackLocationLabourPath).getText());

				/*
				 * mechanicWait(LocatorPath.trackLocationPartsPath);
				 * clickMechanicElement(LocatorType.XPATH,
				 * LocatorPath.trackLocationPartsPath);
				 * enterValueMechanic(LocatorType.XPATH,
				 * LocatorPath.trackLocationPartsPath,
				 * excelInputData.getEnterParts());
				 * hideKeyBoard(mechanicDriver); writeReport(LogStatus.PASS,
				 * "Mechanic enter the parts value " +
				 * getMechanicWebElement(LocatorType.XPATH,
				 * LocatorPath.trackLocationPartsPath).getText());
				 */

				if (j < (loopSize - 1)) {
					Thread.sleep(1000);
					String addJobButton = "//*[@id=\"nav\"]/page-addjobs/ion-footer/ion-row";
					clickMechanicElement(LocatorType.XPATH, addJobButton);
					Thread.sleep(2000);
					mechanicWait(LocatorPath.selectJobPath);
					clickMechanicElement(LocatorType.XPATH, LocatorPath.selectJobPath);
				}
			}

			Thread.sleep(1000);
			mechanicWait(LocatorPath.partsSubmitPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.partsSubmitPath);
			Thread.sleep(3000);

		} catch (Exception e) {

		}
	}

	public void trackLocationWithOutParts(ExcelInputData excelInputData) {

		try {
			Utils.sleepTimeLow();
			mechanicWait(LocatorPath.selectJobPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.selectJobPath);
			Thread.sleep(1000);
			mechanicWait(LocatorPath.totalJobListPath);
			List<WebElement> jobList = mechanicDriver.findElements(By.xpath(LocatorPath.totalJobListPath));
			for (int i = 0; i < jobList.size(); i++) {
				String selectJob = jobList.get(i).getText();
				System.out.println("Inside for loop::::" + selectJob);
				if (selectJob.equalsIgnoreCase(excelInputData.getBdJobSelection())) {
					jobList.get(i).click();
					writeReport(LogStatus.PASS, "Mechanic select job :- " + excelInputData.getBdJobSelection());
					Thread.sleep(1000);
					break;
				}
			}

			Thread.sleep(1000);
			mechanicDriver.pressKeyCode(AndroidKeyCode.ENTER);
			Thread.sleep(1000);

			mechanicWait(LocatorPath.trackLocationLabourPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.trackLocationLabourPath);
			enterValueMechanic(LocatorType.XPATH, LocatorPath.trackLocationLabourPath,
					excelInputData.getLabourAmount());
			Thread.sleep(1000);
			writeReport(LogStatus.PASS, "Mechanic enter the labour amount "
					+ getMechanicWebElement(LocatorType.XPATH, LocatorPath.trackLocationLabourPath).getText());

			/*
			 * mechanicWait(LocatorPath.trackLocationPartsPath);
			 * clickMechanicElement(LocatorType.XPATH,
			 * LocatorPath.trackLocationPartsPath);
			 * enterValueMechanic(LocatorType.XPATH,
			 * LocatorPath.trackLocationPartsPath,
			 * excelInputData.getEnterParts()); hideKeyBoard(mechanicDriver);
			 * writeReport(LogStatus.PASS, "Mechanic enter the parts value " +
			 * getMechanicWebElement(LocatorType.XPATH,
			 * LocatorPath.trackLocationPartsPath).getText());
			 */

			mechanicWait(LocatorPath.partsSubmitPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.partsSubmitPath);
			System.out.println("Enter the Break down parts :::");
			writeReport(LogStatus.PASS, "Service request parts " + excelInputData.getEnterParts() + " entered ");
		} catch (Exception e) {
		}
	}

	public void UpdateJobStatus(ExcelInputData excelInputData, Mechanic mechanic) {

		try {

			mechanic.onGoingOrderPathClick(excelInputData.getVehicleNumber());
			Thread.sleep(2000);
			System.out.println("Enter the service request Update Job Status method :::");
			mechanicWait(LocatorPath.completeJobPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.completeJobPath);
			Thread.sleep(5000);
			mechanicWait(LocatorPath.submitBillPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.submitBillPath);
			Thread.sleep(2000);
			mechanicWait(LocatorPath.paymentRequestOk);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.paymentRequestOk);

		} catch (Exception e) {

		}
	}

	public void UpdateJobStatusWithRevisedBill(ExcelInputData excelInputData, Mechanic mechanic) {

		try {

			mechanic.onGoingOrderPathClick(excelInputData.getVehicleNumber());
			Thread.sleep(2000);
			System.out.println("Inside UpdateJobStatusWithRevisedBill");
			Thread.sleep(2000);
			mechanicWait(LocatorPath.addJobsLinkPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.addJobsLinkPath);
			Thread.sleep(2000);
			mechanicWait(LocatorPath.selectJobPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.selectJobPath);
			Thread.sleep(1000);
			mechanicWait(LocatorPath.totalJobListPath);
			List<WebElement> jobList = mechanicDriver.findElements(By.xpath(LocatorPath.totalJobListPath));

			for (int i = 0; i < jobList.size(); i++) {
				String selectJob = jobList.get(i).getText();
				System.out.println("Inside for loop::::" + selectJob);
				if (selectJob.equalsIgnoreCase(excelInputData.getBdJobSelection())) {
					jobList.get(i).click();
					writeReport(LogStatus.PASS, "Mechanic select job :- " + excelInputData.getBdJobSelection());
					Thread.sleep(1000);
					break;
				}
			}

			Thread.sleep(1000);
			mechanicDriver.pressKeyCode(AndroidKeyCode.ENTER);
			Thread.sleep(1000);

			// EnterLabour:
			String labourPath = "//*[@id=\"nav\"]/page-addjobs/ion-content/div[2]/ion-card[2]/div/ion-row[2]/ion-col[2]/input";

			mechanicWait(labourPath);
			Thread.sleep(1000);
			clickMechanicElement(LocatorType.XPATH, labourPath);
			enterValueMechanic(LocatorType.XPATH, labourPath, excelInputData.getLabourAmount());
			Thread.sleep(1000);

			/*
			 * //EnterParts: String partsPath = "/
			 *//*
				 * [@id=\
				 * "nav\"]/page-addjobs/ion-content/div[2]/ion-card[2]/div/ion-row[2]/ion-col[3]/input";
				 * mechanicWait(partsPath); Thread.sleep(1000);
				 * clickMechanicElement(LocatorType.XPATH, partsPath);
				 * Thread.sleep(1000); enterValueMechanic(LocatorType.XPATH,
				 * partsPath, "300");
				 */
			hideKeyBoard(mechanicDriver);

			mechanicWait(LocatorPath.submitRevisedEstimatePath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.submitRevisedEstimatePath);
			Thread.sleep(2000);
			mechanicWait(LocatorPath.paymentRequestOk);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.paymentRequestOk);

		} catch (Exception e) {

		}
	}

	public void withOutRetailer(ExcelInputData excelInputData) {

		try {

			Utils.sleepTimeLow();
			System.out.println("Enter the service request retailer method :::");
			mechanicWait(LocatorPath.retailersListPath);
			Thread.sleep(2000);
			mechanicClassPathWait(LocatorPath.noRetailerPath);
			clickMechanicElement(LocatorType.CLASS, LocatorPath.noRetailerPath);
			mechanicWait(LocatorPath.retailerConfirmPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.retailerConfirmPath);
			writeReport(LogStatus.PASS, "Mechanic submit estimate - with out retailer ");
		} catch (Exception e) {
		}
	}

	public void UpdateJobStatusRevisedBill(ExcelInputData excelInputData, Mechanic mechanic) {

		try {

			mechanic.onGoingOrderPathClick(excelInputData.getVehicleNumber());
			Thread.sleep(2000);
			System.out.println("Enter the service request Update Job revised bill Status method :::");
			mechanicWait(LocatorPath.completeJobPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.completeJobPath);
			Thread.sleep(3000);

			mechanicWait(LocatorPath.jobSubmitButtonPath);
			Thread.sleep(2000);
			WebElement revisedBill = mechanicDriver.findElement(By.xpath(LocatorPath.revisedBillPath));
			revisedBill.clear();
			revisedBill.clear();
			revisedBill.sendKeys(excelInputData.getRevisedBillValue1());

			Thread.sleep(1000);
			WebElement revisedBillElement = mechanicDriver
					.findElement(By.xpath(LocatorPath.revisedEstimateRevisedBillPath));
			revisedBillElement.clear();
			revisedBillElement.clear();
			revisedBillElement.sendKeys(excelInputData.getRevisedBillValue2());

			mechanicWait(LocatorPath.submitBillPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.submitBillPath);
			Thread.sleep(2000);
			mechanicWait(LocatorPath.paymentRequestOk);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.paymentRequestOk);
			writeReport(LogStatus.PASS, "Mechanic :- Revised bill");

		} catch (Exception e) {

		}
	}

	public void selectRetailer(ExcelInputData excelInputData) {

		try {
			Utils.sleepTimeLow();
			System.out.println("Enter the Break Down retailer method :::");
			mechanicWait(LocatorPath.retailersListPath);
			List<WebElement> retailersList = mechanicDriver.findElements(By.xpath(LocatorPath.retailersListPath));
			int loopSize = retailersList.size() - 3;

			boolean isClicked = false;
			for (int i = 1; i <= loopSize; i++) {
				String insideRetailerPath = "//*[@id=\"nav\"]/page-select-retailer/ion-content/div[2]/ion-card[" + i
						+ "]" + "/*";
				List<WebElement> retailersInsideList = mechanicDriver.findElements(By.xpath(insideRetailerPath));

				if (!isClicked) {
					for (int j = 0; j < retailersInsideList.size(); j++) {
						System.out.println("Al retailer name :::" + retailersInsideList.get(j).getText());
						if (retailersInsideList.get(j).getText().equalsIgnoreCase(excelInputData.getSelectRetailer())) {
							retailersInsideList.get((j + 1)).click();
							writeReport(LogStatus.PASS,
									"Break down retailer :- " + excelInputData.getSelectRetailer() + " Selected ");
							isClicked = true;
							break;
						}
					}
				} else {
					break;
				}
			}

			mechanicWait(LocatorPath.retailerConfirmPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.retailerConfirmPath);
			writeReport(LogStatus.PASS, "Mechanic retailer confirmed ");

		} catch (Exception e) {
			writeReport(LogStatus.FAIL, "Mechanic retailer confirmed failed");
		}
	}

	public void giveEstimateReturnBack(ExcelInputData excelInputData, Mechanic mechanic) {

		try {

			System.out.println("Al Enter the give Estimate method :::");
			mechanic.onGoingOrderPathClick(excelInputData.getVehicleNumber());
			Thread.sleep(2000);
			mechanicWait(LocatorPath.giveEstimatePath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.giveEstimatePath);
			Thread.sleep(5000);
			mechanicDriver.pressKeyCode(AndroidKeyCode.BACK);
			Thread.sleep(5000);
			mechanicDriver.pressKeyCode(AndroidKeyCode.BACK);
			Thread.sleep(5000);

			writeReport(LogStatus.PASS, "Mechanic: Returned form Give Estimate ");
		} catch (Exception e) {
			writeReport(LogStatus.FAIL, "Mechanic: Return form Give Estimate failed ");
		}
	}

}
