package com.servicemandi.breakdown;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.relevantcodes.extentreports.LogStatus;
import com.servicemandi.common.Configuration;
import com.servicemandi.common.LocatorPath;
import com.servicemandi.utils.ExcelInputData;
import com.servicemandi.utils.Utils;

import io.appium.java_client.android.AndroidKeyCode;

/**
 * Created by Shanthakumar on 29-01-2018.
 */

public class CreateBreakDownRequest extends Configuration {

	public CreateBreakDownRequest() {

	}

	public String breakDownNewRequest(ExcelInputData excelInputData) {

		try {

			Thread.sleep(5000);
			String orderId = "";
			System.out.println("Entered Create New Break Down Request method ::: ");
			fleetManagerWait(LocatorPath.createNewRequest);
			clickFMElement(Configuration.LocatorType.XPATH, LocatorPath.createNewRequest);
			Thread.sleep(4000);
			fleetManagerWait(LocatorPath.selectVehicle);
			clickFMElement(Configuration.LocatorType.XPATH, LocatorPath.selectVehicle);
			enterFMValue(Configuration.LocatorType.XPATH, LocatorPath.selectVehicle, excelInputData.getVehicleNumber());
			Thread.sleep(1000);
			clickFMElement(Configuration.LocatorType.XPATH, LocatorPath.selectVehicleNo);
			System.out.println("Entered Create New Request vehicle selected  ::: ");
			fleetManagerWait(LocatorPath.selectDriver);
			clickFMElement(Configuration.LocatorType.XPATH, LocatorPath.selectDriver);
			enterFMValue(Configuration.LocatorType.XPATH, LocatorPath.selectDriver, excelInputData.getDriverName());
			Thread.sleep(1000);
			clickFMElement(Configuration.LocatorType.XPATH, LocatorPath.selectDriverNo);
			System.out.println("Entered Create New Request driver selected  ::: ");
			fleetManagerWait(LocatorPath.selectLocation);
			clickFMElement(Configuration.LocatorType.XPATH, LocatorPath.selectLocation);
			enterFMValue(Configuration.LocatorType.XPATH, LocatorPath.selectLocation, excelInputData.getLocation());
			Thread.sleep(1000);
			hideKeyBoard(fleetManagerDriver);
			clickFMElement(Configuration.LocatorType.XPATH, LocatorPath.selectLocationValue);
			System.out.println("Entered Create New Request location selected  ::: ");

			Thread.sleep(1000);
			clickFMElement(Configuration.LocatorType.XPATH, LocatorPath.breakdownButton);
			fleetManagerWait(LocatorPath.serviceYesBtn);
			clickFMElement(Configuration.LocatorType.XPATH, LocatorPath.serviceYesBtn);
			Thread.sleep(2000);

			fleetManagerWait(LocatorPath.requestOrderIdPath);
			orderId = getFMWebElement(LocatorType.XPATH, LocatorPath.requestOrderIdPath).getText().replaceAll("[^0-9]",
					"");

			writeReport(LogStatus.PASS, "Break Down Order Id ::" + orderId);
			fleetManagerWait(LocatorPath.manageOrder);
			clickFMElement(LocatorType.XPATH, LocatorPath.manageOrder);
			Thread.sleep(1000);
			writeReport(LogStatus.PASS, "New Break Down created");
			return orderId;
		} catch (Exception e) {
			writeReport(LogStatus.ERROR, "Fleet Manager New request Failed");
		}
		return "";
	}

	public String createNewBreakDownInvalidVehicleNo(ExcelInputData excelInputData) throws InterruptedException {

		String orderId = "";
		try {
			Thread.sleep(10000);
			System.out.println("Entered createNewRequest-breakdown method ::: ");
			fleetManagerWait(LocatorPath.createNewRequest);
			clickFMElement(LocatorType.XPATH, LocatorPath.createNewRequest);
			Thread.sleep(8000);
			fleetManagerWait(LocatorPath.selectVehicle);
			clickFMElement(LocatorType.XPATH, LocatorPath.selectVehicle);
			enterFMValue(LocatorType.XPATH, LocatorPath.selectVehicle, excelInputData.getVehicleNumber());
			Thread.sleep(2000);
			clickFMElement(LocatorType.XPATH, LocatorPath.selectVehicleNo);
			Thread.sleep(3000);

			// Vehicle Number validation:

			WebElement vehicleValidnMsgElement = fleetManagerDriver.findElement(
					By.xpath("//html/body/ion-app/ng-component/ion-nav/page-create-service/ion-content/div[2]/p"));
			if (vehicleValidnMsgElement.isDisplayed()) {
				String vehicleValidnMsg = fleetManagerDriver
						.findElement(By.xpath(
								"//html/body/ion-app/ng-component/ion-nav/page-create-service/ion-content/div[2]/p"))
						.getText();

				if (vehicleValidnMsg.contains("Vehicle does not exist.")) {// Msg can be taken to excel
					System.out.println("vehicleValidnMsg" + vehicleValidnMsg);
					fleetManagerDriver.pressKeyCode(AndroidKeyCode.BACK);
					Thread.sleep(2000);
					fleetManagerDriver.pressKeyCode(AndroidKeyCode.BACK);
					writeReport(LogStatus.FAIL,
							"FM: Vehicle Number ::" + excelInputData.getVehicleNumber() + "  doesnt exists in DB");

				}

			}
		} catch (Exception e) {
			e.printStackTrace();
			writeReport(LogStatus.FAIL, "FM: BD: Failed on Entering Invalid Vehicle Number");
		}
		return orderId;
	}

	public String createNewBreakDownVehicleNoAlreadyExists(ExcelInputData excelInputData) throws InterruptedException {

		String orderId = "";
		try {
			Thread.sleep(8000);
			System.out.println("Entered createNewRequest-breakdown method ::: ");
			fleetManagerWait(LocatorPath.createNewRequest);
			clickFMElement(LocatorType.XPATH, LocatorPath.createNewRequest);
			Thread.sleep(8000);
			fleetManagerWait(LocatorPath.selectVehicle);
			clickFMElement(LocatorType.XPATH, LocatorPath.selectVehicle);
			enterFMValue(LocatorType.XPATH, LocatorPath.selectVehicle, excelInputData.getVehicleNumber());
			Thread.sleep(2000);
			clickFMElement(LocatorType.XPATH, LocatorPath.selectVehicleNo);
			Thread.sleep(3000);

			Thread.sleep(2000);
			clickFMElement(LocatorType.XPATH, LocatorPath.selectDriver);
			enterFMValue(LocatorType.XPATH, LocatorPath.selectDriver, excelInputData.getDriverName());
			Thread.sleep(1000);
			clickFMElement(LocatorType.XPATH, LocatorPath.selectDriverNo);
			Thread.sleep(1000);
			clickFMElement(LocatorType.XPATH, LocatorPath.selectLocation);
			enterFMValue(LocatorType.XPATH, LocatorPath.selectLocation, excelInputData.getLocation());
			hideKeyBoard(fleetManagerDriver);
			clickFMElement(LocatorType.XPATH, LocatorPath.selectLocationValue);
			Thread.sleep(1000);
			// hideKeyBoard(fmDriver);

			Thread.sleep(1000);
			fleetManagerWait(LocatorPath.breakdownButton);
			clickFMElement(LocatorType.XPATH, LocatorPath.breakdownButton);
			Thread.sleep(2000);
			fleetManagerWait(LocatorPath.serviceYesBtn);
			clickFMElement(LocatorType.XPATH, LocatorPath.serviceYesBtn);
			Thread.sleep(8000);

			// Vehicle Number already in Progress validation:

			String Duplicatemsg = "/html/body/ion-app/ion-alert/div";
			fleetManagerWait(Duplicatemsg);
			String Duplicatemsg1 = fleetManagerDriver.findElement(By.xpath(Duplicatemsg)).getText();
			WebElement AlertMsg1 = fleetManagerDriver.findElement(By.xpath(Duplicatemsg));
			if (AlertMsg1.isDisplayed()) {// Msg can be taken to excel
				System.out.println("Inside Alert");
				fleetManagerDriver.pressKeyCode(AndroidKeyCode.ENTER);
				Thread.sleep(2000);
				fleetManagerDriver.pressKeyCode(AndroidKeyCode.BACK);
				Thread.sleep(6000);

				writeReport(LogStatus.FAIL, "FM: BREAKDOWN -CREATE NEW REQUEST-Vehicle No "
						+ excelInputData.getVehicleNumber() + "Already Exists. Message :: " + Duplicatemsg1 + "");
			}
		} catch (Exception e) {
			e.printStackTrace();
			writeReport(LogStatus.FAIL,
					"FM: BREAKDOWN -NEW REQUEST-Vehicle No " + excelInputData.getVehicleNumber() + "Already Exists");
		}
		return orderId;
	}
}
