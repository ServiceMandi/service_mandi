package com.servicemandi.common;

import com.relevantcodes.extentreports.DisplayOrder;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.servicemandi.utils.Utils;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;

/**
 * Created by Shanthakumar on 29-01-2018.
 */

public class Configuration {

	private static DesiredCapabilities desiredCapabilities;
	private WebElement element;
	public static AndroidDriver fleetManagerDriver = null;
	public static AndroidDriver mechanicDriver = null;
	protected static WebDriverWait fmWait = null;
	protected static WebDriverWait mechWait = null;
	public static ExtentReports extentReports;
	public static ExtentTest eLogger;
	public static String fmAppPackage = "WEBVIEW_com.servicemandi.fm";
	public static String mechAppPackage = "WEBVIEW_com.servicemandi.mechanic";
	public static String destDir;
	public static DateFormat dateFormat;
	public static File scrFile;

	public enum LocatorType {
		XPATH, ID, CLASS, NAME, CSS
	}

	public static void fleetManagerCapabilities() {

		try {
			desiredCapabilities = new DesiredCapabilities();
			desiredCapabilities.setCapability("deviceName", "Samsung SM-J120G");

			desiredCapabilities.setCapability("appium-version", "1.7.1");

			desiredCapabilities.setCapability(MobileCapabilityType.UDID, "32011c38412e3479");
			// Santa
			// desiredCapabilities.setCapability(MobileCapabilityType.UDID,"42005e66ec7524d3");
			// desiredCapabilities.setCapability(MobileCapabilityType.UDID,
			// "420015a3b2fa1449"); // Priya
			// FM
			desiredCapabilities.setCapability("platformName", "Android");
			// desiredCapabilities.setCapability("platformVersion", "5.1.1");
			desiredCapabilities.setCapability("recreateChromeDriverSessions", "true");
			desiredCapabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, "100000");
			desiredCapabilities.setCapability("appPackage", "com.servicemandi.fm");
			desiredCapabilities.setCapability("appActivity", "com.servicemandi.fm.MainActivity");
			desiredCapabilities.setCapability("automationName", "appium");
			desiredCapabilities.setCapability("autoAcceptAlerts", true);
			desiredCapabilities.setCapability("autoDismissAlerts", true);
			fleetManagerDriver = new AndroidDriver(new URL("http://0.0.0.0:4723/wd/hub"), desiredCapabilities);

			fleetManagerDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			fmWait = new WebDriverWait(fleetManagerDriver, 120);

		} catch (Exception e) {
		}
	}

	public static AndroidDriver getFleetManagerInstance() {

		if (fleetManagerDriver != null) {
			return fleetManagerDriver;
		}
		return null;
	}

	public static AndroidDriver getMechanicDriver() {

		if (mechanicDriver != null) {
			return mechanicDriver;
		}
		return null;
	}

	public static void mechanicCapabilities() {

		try {

			desiredCapabilities = new DesiredCapabilities();
			desiredCapabilities.setCapability("deviceName", "Samsung SM-J120G");

			desiredCapabilities.setCapability("appium-version", "1.7.1");
			// Deepa
			desiredCapabilities.setCapability(MobileCapabilityType.UDID, "4200145bf0e11499");

			// Santa
			 //desiredCapabilities.setCapability(MobileCapabilityType.UDID, "4200d9904ed114f3");

			// Priya Mech
			// desiredCapabilities.setCapability(MobileCapabilityType.UDID,
			// "42005e40ec9e2415");
			desiredCapabilities.setCapability("platformName", "Android");
			desiredCapabilities.setCapability("platformVersion", "5.1.1");
			desiredCapabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, "100000");
			desiredCapabilities.setCapability("recreateChromeDriverSessions", "true");
			desiredCapabilities.setCapability("appPackage", "com.servicemandi.mechanic");
			desiredCapabilities.setCapability("appActivity", "com.servicemandi.mechanic.MainActivity");
			desiredCapabilities.setCapability("automationName", "appium");
			desiredCapabilities.setCapability("autoAcceptAlerts", true);
			desiredCapabilities.setCapability("autoDismissAlerts", true);
			mechanicDriver = new AndroidDriver(new URL("http://127.0.0.1:4273/wd/hub"), desiredCapabilities);
			mechanicDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			mechWait = new WebDriverWait(mechanicDriver, 120);
		} catch (Exception e) {
		}
	}

	public static void reportConfiguration() {

		if (extentReports == null) {
			extentReports = new ExtentReports(
					System.getProperty("user.dir") + File.separator + "Server_Mandi_Reports.html", true,
					DisplayOrder.OLDEST_FIRST);
		}

	}

	public static void closeWorkFlowReport() {
		extentReports.endTest(eLogger);
	}

	public static void createNewWorkFlowReport(String newWorkFlow) {

		eLogger = extentReports.startTest(newWorkFlow);
	}

	public WebElement getFMWebElement(LocatorType type, String locator) {
		element = null;
		switch (type) {
		case XPATH:
			if (fleetManagerDriver.findElements(By.xpath(locator)).size() > 0) {
				element = fleetManagerDriver.findElement(By.xpath(locator));
			} else {
				element = null;
			}
			break;
		case NAME:
			if (fleetManagerDriver.findElements(By.name(locator)).size() > 0) {
				element = fleetManagerDriver.findElement(By.name(locator));
			} else {
				element = null;
			}
			break;
		case CLASS:
			if (fleetManagerDriver.findElements(By.className(locator)).size() > 0) {
				element = fleetManagerDriver.findElement(By.className(locator));
			} else {
				element = null;
			}
			break;
		case CSS:
			if (fleetManagerDriver.findElements(By.cssSelector(locator)).size() > 0) {
				element = fleetManagerDriver.findElement(By.cssSelector(locator));
			} else {
				element = null;
			}
			break;
		}
		return element;
	}

	public void clickFMElement(LocatorType type, String locator) {

		try {
			WebElement elem = getFMWebElement(type, locator);
			if (elem != null) {
				elem.click();
			}
		} catch (Exception e) {
		}
	}

	public void enterFMValue(LocatorType type, String locator, String data) {

		try {
			WebElement elem = getFMWebElement(type, locator);
			if (elem != null) {
				elem.click();
				elem.clear();
				elem.sendKeys(data);
			}
		} catch (Exception e) {
		}
	}

	/**
	 * This method switches to web view context.
	 */
	public static void switchToFMWebView() {

		try {
			Set<String> availableContexts = fleetManagerDriver.getContextHandles();
			for (String context : availableContexts) {
				if (context.contains(fmAppPackage)) {
					fleetManagerDriver.context(context);
					System.out.println("Context switched to web view ::: " + context);
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void fleetManagerWait(String locator) {

		try {
			fmWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locator)));
		} catch (Exception e) {

		}
	}

	public WebElement getMechanicWebElement(LocatorType type, String locator) {
		element = null;
		switch (type) {
		case XPATH:
			if (mechanicDriver.findElements(By.xpath(locator)).size() > 0) {
				element = mechanicDriver.findElement(By.xpath(locator));
			} else {
				element = null;
			}
			break;
		case NAME:
			if (mechanicDriver.findElements(By.name(locator)).size() > 0) {
				element = mechanicDriver.findElement(By.name(locator));
			} else {
				element = null;
			}
			break;
		case CLASS:
			if (mechanicDriver.findElements(By.className(locator)).size() > 0) {
				element = mechanicDriver.findElement(By.className(locator));
			} else {
				element = null;
			}
			break;
		case CSS:
			if (mechanicDriver.findElements(By.cssSelector(locator)).size() > 0) {
				element = mechanicDriver.findElement(By.cssSelector(locator));
			} else {
				element = null;
			}
			break;
		}
		return element;
	}

	public void clickMechanicElement(LocatorType type, String locator) {

		try {
			WebElement elem = getMechanicWebElement(type, locator);
			if (elem != null) {
				elem.click();
			}
		} catch (Exception e) {
		}
	}

	public void enterValueMechanic(LocatorType type, String locator, String data) {

		try {
			WebElement elem = getMechanicWebElement(type, locator);
			if (elem != null) {
				elem.click();
				elem.click();
				elem.clear();
				elem.sendKeys(data);
			}
		} catch (Exception e) {
		}
	}

	public void switchToMechanicWebView() {

		try {
			Set<String> availableContexts = mechanicDriver.getContextHandles();
			// switchToNativeView();
			for (String context : availableContexts) {
				if (context.contains(mechAppPackage)) {
					mechanicDriver.context(context);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void mechanicWait(String locator) {

		try {
			mechWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locator)));
		} catch (Exception e) {

		}
	}

	public static void mechanicClassPathWait(String locator) {

		try {
			mechWait.until(ExpectedConditions.visibilityOfElementLocated(By.className(locator)));
		} catch (Exception e) {

		}
	}

	/**
	 * This method destroys the created android driver instance.
	 */
	public static void tearDownFleetManager() {
		try {
			Thread.sleep(5000);
			fleetManagerDriver.quit();
		} catch (InterruptedException e) {
		}
	}

	public static void saveReport() {

		eLogger.log(LogStatus.PASS, "End of Work Flow");
		extentReports.endTest(eLogger);
		extentReports.flush();
		extentReports.close();
	}

	/**
	 * This method destroys the created android driver instance.
	 */
	public static void tearDownMechanic() {
		try {
			Thread.sleep(2000);
			mechanicDriver.quit();
		} catch (InterruptedException e) {
		}
	}

	public static String getElementTextValue(AndroidDriver driver, String locator) {

		try {
			return driver.findElement(By.xpath(locator)).getText();
		} catch (Exception e) {

		}
		return "";
	}

	public static String getElementAttributeValue(AndroidDriver driver, String locator) {

		try {
			return driver.findElement(By.xpath(locator)).getAttribute("value");
		} catch (Exception e) {

		}
		return "";
	}

	public static void writeReport(LogStatus logStatus, String status) {

		if (eLogger == null) {
			reportConfiguration();
		}

		status = status + " : " + Utils.timeCalculation();
		switch (logStatus) {

		case PASS:
			eLogger.log(LogStatus.PASS, status);
			break;
		case FAIL:
			eLogger.log(LogStatus.FAIL, status);
			break;
		case INFO:
			eLogger.log(LogStatus.INFO, status);
		}
	}

	public static void hideKeyBoard(AndroidDriver androidDriver) {

		try {
			androidDriver.hideKeyboard();
			Thread.sleep(1000);
		} catch (Exception e) {
			// Code change done by Shanthakumar
			e.printStackTrace();
		}
	}

	public static void takeScreenShotFleetManager() {
		destDir = "screenshots";
		System.out.println("Start Take ScreenShot");
		try {
			scrFile = ((TakesScreenshot) fleetManagerDriver).getScreenshotAs(OutputType.FILE);
		} catch (Exception e) {
			System.out.println("FleetManager :" + e.getMessage());
		}
		System.out.println("Inprogress Take ScreenShot");
		dateFormat = new SimpleDateFormat("dd-MMM-yyyy__hh_mm_ssaa");
		new File(destDir).mkdirs();
		String destFile = dateFormat.format(new Date()) + ".png";
		try {
			FileUtils.copyFile(scrFile, new File(destDir + "FleetManager" + "/" + destFile));
			System.out.println("Start Take ScreenShot Completed for fleet manager");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void takeScreenShotMechanicDriver() {
		destDir = "screenshots";
		System.out.println("Start Take ScreenShot");
		try {
			scrFile = ((TakesScreenshot) mechanicDriver).getScreenshotAs(OutputType.FILE);
		} catch (Exception e) {
			System.out.println("Mechenic :" + e.getMessage());
		}
		System.out.println("Inprogress Take ScreenShot");
		dateFormat = new SimpleDateFormat("dd-MMM-yyyy__hh_mm_ssaa");
		new File(destDir).mkdirs();
		String destFile = dateFormat.format(new Date()) + ".png";
		try {
			FileUtils.copyFile(scrFile, new File(destDir + "Mechenic" + "/" + destFile));
			System.out.println("Start Take ScreenShot Completed for fleet manager");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
