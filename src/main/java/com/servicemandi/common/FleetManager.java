package com.servicemandi.common;

import com.relevantcodes.extentreports.LogStatus;
import com.servicemandi.utils.ExcelInputData;
import com.servicemandi.utils.Utils;

import java.awt.AWTException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;

/**
 * Created by Shanthakumar on 29-01-2018.
 */

public class FleetManager extends Configuration {

	public FleetManager() {

	}

	public void login(ExcelInputData excelInputData) {

		try {
			fleetManagerCapabilities();
			Utils.sleepTimeLow();
			switchToFMWebView();
			Utils.sleepTimeLow();
			String BuildVersion = getElementTextValue(fleetManagerDriver, LocatorPath.buildVerion);
			fleetManagerWait(LocatorPath.languagePath);
			clickFMElement(LocatorType.XPATH, LocatorPath.languagePath);
			fleetManagerWait(LocatorPath.signUpPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.signUpPath);
			/*
			 * try{ Thread.sleep(2000);
			 * getScreenshot(fleetManagerDriver,"SampleScreenshot"); }catch(Exception e){
			 * e.printStackTrace(); }
			 */
			Thread.sleep(1000);

			fleetManagerWait(LocatorPath.mobilePath);
			clickFMElement(LocatorType.XPATH, LocatorPath.mobilePath);
			Thread.sleep(2000);
			enterFMValue(LocatorType.XPATH, LocatorPath.mobilePath, excelInputData.getMobileNumber());
			// takeScreenShotFleetManager();
			clickFMElement(LocatorType.XPATH, LocatorPath.requestOTP);
			fleetManagerWait(LocatorPath.loginPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.loginPath);
			// takeScreenShotFleetManager();
			System.out.println("Al Build version number :::" + BuildVersion);
			// writeReport(LogStatus.PASS, "Build Version Number :-> " + BuildVersion);
			writeReport(LogStatus.PASS, "Fleet Manager Login Passed");
		} catch (Exception e) {
			writeReport(LogStatus.ERROR, "Fleet Manager Login Failed");
		}
	}

	public void selectLiveOrderSearch(String vehicleNo) {

		try {

			Thread.sleep(1000);
			fleetManagerWait(LocatorPath.liveOrderTab);
			clickFMElement(LocatorType.XPATH, LocatorPath.liveOrderTab);
			Utils.sleepTimeLow();
			String liveOrderPath = "//*[contains(text(), '" + vehicleNo + "')]";
			System.out.println("The live Order path :::" + liveOrderPath);
			fleetManagerWait(liveOrderPath);
			WebElement liveOrderElement = getFMWebElement(LocatorType.XPATH, liveOrderPath);

			if (liveOrderElement.getText().equalsIgnoreCase(vehicleNo)) {
				fleetManagerWait(liveOrderPath);
				liveOrderElement.click();
				System.out.println("AL Click FM Live order state ::" + liveOrderElement.getText());
				Thread.sleep(1000);
			}

		} catch (Exception e) {

		}
	}

	public void viewBill(ExcelInputData excelInputData) throws InterruptedException {

		selectLiveOrderSearch(excelInputData.getVehicleNumber());
		System.out.println("Entered View Bill method ::");
		Thread.sleep(3000);
		fleetManagerWait(LocatorPath.payCashButtonPath);
		// Utils.checkBillWithOutGST();
		// Utils.checkBillWithIGST();
		// Utils.checkBillWithGST();
		clickFMElement(LocatorType.XPATH, LocatorPath.payCashButtonPath);
		writeReport(LogStatus.PASS, "Fleet manger view bill & Pay cash");
		fleetManagerWait(LocatorPath.liveOrderTab);

	}

	public void viewRevisedBill(ExcelInputData excelInputData) {

		try {

			selectLiveOrderSearch(excelInputData.getVehicleNumber());
			System.out.println("Al Entered view bill method ::");
			Thread.sleep(2000);
			fleetManagerWait(LocatorPath.detailedBillPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.detailedBillPath);
			Thread.sleep(1000);
			fleetManagerWait(LocatorPath.payCashButtonPath);
			// Utils.checkBillWithOutGST(); //Utils.checkBillWithIGST();
			// Utils.checkBillWithGST();
			clickFMElement(LocatorType.XPATH, LocatorPath.payCashButtonPath);
			writeReport(LogStatus.PASS, "Fleet manager View bill & Pay cash");
			fleetManagerWait(LocatorPath.liveOrderTab);
		} catch (Exception e) {
			writeReport(LogStatus.FAIL, "Fleet manager View bill & Pay cash failed");
		}
	}

	public void fleetManagerRating(ExcelInputData excelInputData) {

		try {

			selectLiveOrderSearch(excelInputData.getVehicleNumber());
			System.out.println("Al Entered Fleet manager rating method ::");
			fleetManagerWait(LocatorPath.fleetManagerRatingPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.fleetManagerRatingPath);
			Thread.sleep(2000);
			fleetManagerWait(LocatorPath.fleetManagerFeedBackPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.fleetManagerFeedBackPath);
			Thread.sleep(1000);
			fleetManagerWait(LocatorPath.fleetManagerRatingSubmit);
			clickFMElement(LocatorType.XPATH, LocatorPath.fleetManagerRatingSubmit);
			writeReport(LogStatus.PASS, "Fleet manger rating the order");
			fleetManagerWait(LocatorPath.liveOrderTab);
		} catch (Exception e) {
			writeReport(LogStatus.FAIL, "Fleet manger rating the order failed");
		}
	}

	public void fleetManagerConfirmGarage(ExcelInputData excelInputData) {

		try {

			selectLiveOrderSearch(excelInputData.getVehicleNumber());
			Thread.sleep(2000);
			fleetManagerWait(LocatorPath.confirmGarageButtonPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.confirmGarageButtonPath);
			writeReport(LogStatus.PASS, "Fleet manager confirm the garage  ");
			Thread.sleep(2000);
			fleetManagerWait(LocatorPath.TrackLocationPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.TrackLocationPath);
			writeReport(LogStatus.PASS, "Fleet manager checked track location ");
			Thread.sleep(2000);
			fleetManagerDriver.pressKeyCode(AndroidKeyCode.BACK);

		} catch (Exception e) {
			writeReport(LogStatus.ERROR, "Fleet Manager confirm Failed");
		}
	}

	public void cancelOrder() {

		try {

			Thread.sleep(1000);
			fleetManagerWait(LocatorPath.liveOrderTab);
			clickFMElement(LocatorType.XPATH, LocatorPath.liveOrderTab);
			Utils.sleepTimeMedium();
			fleetManagerWait(LocatorPath.cancelOrderPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.cancelOrderPath);
			Utils.sleepTimeLow();
			String yesBtn = "//html/body/ion-app/ion-alert/div/div[3]/button[1]/span";
			fleetManagerWait(yesBtn);
			clickFMElement(LocatorType.XPATH, yesBtn);
			Thread.sleep(2000);
			fleetManagerDriver.pressKeyCode(AndroidKeyCode.ENTER);
			writeReport(LogStatus.PASS, "FleetManager: Job is Cancelled");

		} catch (Exception e) {

		}
	}

	public void fmLoginWithIncorrectMobileNo(ExcelInputData excelInputData) {

		try {

			fleetManagerCapabilities();
			Utils.sleepTimeLow();
			switchToFMWebView();
			Utils.sleepTimeLow();

			fleetManagerWait(LocatorPath.languagePath);
			clickFMElement(LocatorType.XPATH, LocatorPath.languagePath);
			fleetManagerWait(LocatorPath.signUpPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.signUpPath);
			fleetManagerWait(LocatorPath.mobilePath);

			clickFMElement(LocatorType.XPATH, LocatorPath.mobilePath);
			Thread.sleep(2000);
			enterFMValue(LocatorType.XPATH, LocatorPath.mobilePath, excelInputData.getInvalidMobileNo());
			fleetManagerDriver.pressKeyCode(AndroidKeyCode.BACK);

			Thread.sleep(2000);
			String mobileErrMsg = "/html/body/ion-app/ng-component/ion-nav/page-otp/ion-content/div[2]/div/form/p";
			String mobileMsg = getFMWebElement(LocatorType.XPATH, mobileErrMsg).getText();

			try {
				if (mobileMsg.contentEquals("Please enter a valid mobile number")) {
					// logger.info("**************FM: INVALID MOBILE NUMBER is
					// ENTERED**********************");
					writeReport(LogStatus.FAIL, "FM INVALID MOBILE NUMBER " + mobileMsg + "is ENTERED- Verified");
					Thread.sleep(2000);
					fleetManagerDriver.pressKeyCode(AndroidKeyCode.BACK);
					fleetManagerWait(LocatorPath.signUpPath);
					clickFMElement(LocatorType.XPATH, LocatorPath.signUpPath);
					fleetManagerWait(LocatorPath.mobilePath);
					clickFMElement(LocatorType.XPATH, LocatorPath.mobilePath);
					enterFMValue(LocatorType.XPATH, LocatorPath.mobilePath, excelInputData.getMobileNumber());

				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			clickFMElement(LocatorType.XPATH, LocatorPath.requestOTP);
			fleetManagerWait(LocatorPath.loginPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.loginPath);
			Thread.sleep(2000);
			// logger.info("**************FM: Login Passed**********************");
			writeReport(LogStatus.PASS, "FM Login Passed");
		} catch (Exception e) {

		}
	}

	public void fleetManagerScreenShot() {
		// takeScreenShotFleetManager();
	}

}
