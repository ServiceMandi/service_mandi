package com.servicemandi.common;

/**
 * Created by Shanthakumar on 29-01-2018.
 */

public interface LocatorPath {

	String languagePath = "//*[contains(text(), 'English')]";
	String signUpPath = "//*[contains(text(), 'LOGIN / SIGN UP')]";
	String mobilePath = "//input[@class='text-input text-input-md ng-untouched ng-pristine ng-invalid']";
	String requestOTP = "//*[contains(text(), 'REQUEST OTP')]";
	String loginPath = "//html/body/ion-app/ng-component/ion-nav/page-login/ion-content/div[2]/div/form/button/span";
	String mechanicMobilePath = "//input[@class='text-input text-input-md ng-untouched ng-pristine ng-invalid']";
	String mechanicLoginPath = "//*[@id=\"nav\"]/page-login/ion-content/div[2]/div[2]/form/button/span";
	String onlineOffLinePath = "//*[@id=\"nav\"]/page-home/ion-header/div/ion-card/ion-item/div[1]/div/ion-label/div[2]/label/span[1]";
	String createNewRequest = "//html/body/ion-app/ng-component/ion-nav/page-live-orders/ion-footer";
	String selectVehicle = "//html/body/ion-app/ng-component/ion-nav/page-create-service/ion-content/div[2]/form/div/ion-searchbar/div/input";
	String selectVehicleNo = "//html/body/ion-app/ng-component/ion-nav/page-create-service/ion-content/div[2]/form/div/div/ul/li";
	String selectDriver = "//html/body/ion-app/ng-component/ion-nav/page-create-service/ion-content/div[2]/ion-searchbar[1]/div/input";
	String selectDriverNo = "//html/body/ion-app/ng-component/ion-nav/page-create-service/ion-content/div[2]/div/ul/li";
	String selectLocation = "//*[@id=\"googlePlaces\"]/div/input";
	String selectLocationValue = "//html/body/ion-app/ng-component/ion-nav/page-create-service/ion-content/div[2]/ion-list/ion-item/div[1]/div/ion-label";
	String serviceRepairsButton = "//html/body/ion-app/ng-component/ion-nav/page-create-service/ion-footer/div/button[2]/span";
	String serviceYesBtn = "//html/body/ion-app/ion-alert/div/div[3]/button[1]/span";
	String selectJob = "//html/body/ion-app/ng-component/ion-nav/page-select-service/ion-content/div[2]/ion-list/div[1]/ion-item/div[1]/div/ion-label";
	String acJobSelectPath = "//div[2]/ion-list/div[1]/div/ion-list";
	String searchGaragesPath = "//*[contains(text(), 'Search Garages')]";
	String garageListPath = "//*[@id=\"garagebox\"]/*";
	String confirmGaragePath = "//html/body/ion-app/ng-component/ion-nav/page-select-garage/ion-footer/button";
	String requestOrderIdPath = "//html/body/ion-app/ng-component/ion-nav/page-new-request/ion-content/div[2]/h3";
	String manageOrder = "//*[contains(text(), 'MANAGE OTHER ORDERS')]";

	String approveEstimateCancelButton = "//html/body/ion-app/ng-component/ion-nav/page-estimate/ion-footer/button";
	String approveEstimateCancelJob = "//page-estimate/ion-content/div[2]/ion-grid/div/ion-list/ion-row/ion-col[4]/ion-checkbox";
	String mechanicNewRequest = "//*[@id=\"nav\"]/page-home/ion-header/ion-segment/ion-segment-button[1]";
	String mechanicAcceptOrderPath = "//*[@id=\"nav\"]/page-home/ion-content/div[2]/div/div/div[2]/ion-card/ion-row/ion-col/ion-list/ion-row[2]/button[2]";
	// String onGoingOrdersTab = "//*[contains(text(), 'ONGOING')]";
	String onGoingOrdersTab = "//*[@id=\"nav\"]/page-home/ion-header/ion-segment/ion-segment-button[2]";
	String giveEstimatePath = "//*[@id=\"nav\"]/page-track-order/ion-footer/div/button";
	String trackLocationPartsPath = "//*[@id=\"nav\"]/page-addjobs/ion-content/div[2]/ion-card/div/ion-row[2]/ion-col[3]/input";
	String partsSubmitPath = "//*[@id=\"nav\"]/page-addjobs/ion-footer/div/button[1]";
	String retailersListPath = "//*[@id=\"nav\"]/page-select-retailer/ion-content/div[2]/*";
	String retailerConfirmPath = "//*[contains(text(), 'CONFIRM')]";
	String liveOrderTab = "//*[contains(text(), 'LIVE ORDERS')]";
	String approveEstimatePath = "//html/body/ion-app/ng-component/ion-nav/page-estimate/ion-footer/button/span";
	String ApproveEstChkbox = "/html/body/ion-app/ng-component/ion-nav/page-estimate/ion-content/div[2]/ion-grid/div/ion-list/ion-row/ion-col[4]/ion-checkbox";
	String CancelOrder = "//html/body/ion-app/ng-component/ion-nav/page-estimate/ion-footer/button/span";
	String payCashButtonPath = "//*[contains(text(), 'PAY CASH')]";
	String payCashLink = "//html/body/ion-app/ng-component/ion-nav/page-pay-spare/ion-content/div[2]/ion-card/ion-card-content/ion-row/ion-col[1]";
	String jobsCompletePath = "//*[@id=\"nav\"]/page-startjobs/ion-footer/button[2]";
	String jobSubmitButtonPath = "//*[@id=\"nav\"]/page-final-bill/ion-footer/button";
	String jobSubmitBillPath = "//*[contains(text(), 'SUBMIT BILL')]";
	String alertOkPath = "//*[contains(text(), 'OK')]";
	String alertYesButton = "//*[contains(text(), 'YES')]";
	String mechanicThankYouPath = "//*[@id=\"nav\"]/page-rating/ion-content/div[2]/ion-card[2]/ion-card-content/ion-row/span/starrating/span/ion-col[3]/ion-icon";
	String mechanicRatingPath = "//*[@id=\"nav\"]/page-rating/ion-content/div[2]/ion-card[2]/ion-card-content/ion-row/span/starrating/span/ion-col[4]";
	String mechanicFeedBackPath = "//html/body/ion-app/ion-alert/div/div[3]/div/button[1]/span/div[2]";
	String mechanicRatingSubmit = "//html/body/ion-app/ion-alert/div/div[4]/button[2]";
	String fleetManagerRatingPath = "//html/body/ion-app/ng-component/ion-nav/page-rating/ion-content/div[2]/div/div/div[2]/div/ion-icon[3]";
	String fleetManagerFeedBackPath = "//html/body/ion-app/ion-alert/div/div[3]/div/button[1]/span/div[2]";
	String fleetManagerRatingSubmit = "//html/body/ion-app/ion-alert/div/div[4]/button[2]/span";

	String iDontKnowPath = "//html/body/ion-app/ng-component/ion-nav/page-select-service/ion-footer/ion-item";
	String breakdownButton = "//html/body/ion-app/ng-component/ion-nav/page-create-service/ion-footer/div/button[1]/span";
	String confirmGarageButtonPath = "//*[contains(text(), 'CONFIRM GARAGE')]";
	String TrackLocationPath = "//*[contains(text(), 'TRACK LOCATION')]";

	String selectJobPath = "//*[@id=\"undefined\"]";
	String totalJobListPath = "//html/body/ion-app/ion-alert/div/div[3]/div/*";
	String trackLocationLabourPath = "//*[@id=\"nav\"]/page-addjobs/ion-content/div[2]/ion-card/div/ion-row[2]/ion-col[2]/input";
	String noRetailerPath = "checkboxCls";
	String noRetailerAlertPath = "//html/body/ion-app/ion-alert/div/div[3]/button/span";
	String addJobsLinkPath = "//*[@id=\"nav\"]/page-startjobs/ion-content/div[2]/ion-row";

	String submitRevisedEstimatePath = "//*[contains(text(), 'SUBMIT REVISED ESTIMATE')]";
	String revisedPaymentRequestOk = "/html/body/ion-app/ion-alert/div/div[3]/button";
	String approveRevisedEstimatePath = "//html/body/ion-app/ng-component/ion-nav/page-revised-estimate/ion-footer/button";

	String completeJobPath = "//*[@id=\"nav\"]/page-startjobs/ion-footer/button[2]";
	String revisedBillPath1 = "//*[@id=\"nav\"]/page-final-bill/ion-content/div[2]/ion-list/ion-item[2]/div[1]/div/ion-label/ion-row/ion-col[3]/input";
	String revisedBillPath2 = "//*[@id=\"nav\"]/page-final-bill/ion-content/div[2]/ion-list/ion-item[3]/div[1]/div/ion-label/ion-row/ion-col[3]/input";

	String submitBillPath = "//*[@id=\"nav\"]/page-final-bill/ion-footer/button";

	String paymentRequestOk = "//html/body/ion-app/ion-alert/div/div[3]/button";
	String mechanicRejectOrder = "//*[@id=\"nav\"]/page-home/ion-content/div[2]/div/div/div[2]/ion-card/ion-row/ion-col/ion-list/ion-row[2]/button[1]";
	String mechanicRejectReasonList = "//html/body/ion-app/ion-alert/div/div[3]/div/*";
	String myAccountPath = "//*[contains(text(), 'ACCOUNT')]";
	String myAccountListPath = "//html/body/ion-app/ng-component/ion-nav/page-live-orders/ion-content/div[2]/div/div/ion-list/*";
	String editIconPath = "//html/body/ion-app/ng-component/ion-nav/page-profile-info/ion-header/ion-avatar/ion-icon";
	String editNamePath = "//html/body/ion-app/ng-component/ion-nav/page-profile-info/ion-content/div[2]/ion-list/form/ion-row[1]/ion-col/div/ion-input/input[1]";
	String editMobilePath = "//html/body/ion-app/ng-component/ion-nav/page-profile-info/ion-content/div[2]/ion-list/form/ion-row[2]/ion-col/div/ion-input/input[1]";
	String editEmailPath = "//html/body/ion-app/ng-component/ion-nav/page-profile-info/ion-content/div[2]/ion-list/form/ion-row[3]/ion-col/div/ion-input/input[1]";
	String editGSTNPath = "//html/body/ion-app/ng-component/ion-nav/page-profile-info/ion-content/div[2]/ion-list/ion-row[1]/ion-col/div/ion-input/input[1]";
	String editAddressPath = "//html/body/ion-app/ng-component/ion-nav/page-profile-info/ion-content/div[2]/ion-list/ion-row[2]/ion-col/div/ion-input/input[1]";
	String editStatePath = "//html/body/ion-app/ng-component/ion-nav/page-profile-info/ion-content/div[2]/ion-list/ion-row[3]/ion-col/div/ion-select";
	String editTogglePath = "//ion-list/ion-row[4]/ion-col/div/ion-toggle";
	String stateListPath = "//div/div[3]/div/*";
	String savePath = "//page-profile-info/ion-footer/button";
	String addVehiclePath = "//*[contains(text(), 'ADD VEHICLE')]";
	String vehicleSubmitPath = "//html/body/ion-app/ng-component/ion-nav/page-add-vehicle/ion-footer/button";
	String addDriverPath = "//*[contains(text(), 'ADD DRIVER')]";
	String nonElectronicPath = "//*[contains(text(), 'Non-Electronic')]";
	String electronicPath = "//*[contains(text(), 'Electronic')]";
	String registerNoPath = "//ion-content/div[2]/form/div/input";
	String vehicleAlertPath = "//html/body/ion-app/ion-alert/div/div[3]/div/*";
	String cancelOrderPath = "//html/body/ion-app/ng-component/ion-nav/page-live-orders/ion-content/div[2]/div/div/div/div[2]/ion-card[1]/ion-card-content/ion-icon";
	String cancelOrderReasonPath = "/html/body/ion-app/ion-alert/div/div[3]/div";
	String detailedBillPath = "//html/body/ion-app/ion-alert/div/div[3]/button";
	String revisedBillPath = "//*[@id=\"nav\"]/page-final-bill/ion-content/div[2]/ion-list/ion-item[2]/div[1]/div/ion-label/ion-row/ion-col[3]/input";
	String revisedEstimateRevisedBillPath = "//*[@id=\"nav\"]/page-final-bill/ion-content/div[2]/ion-list/ion-item[3]/div[1]/div/ion-label/ion-row/ion-col[3]/input";

	/// -------------- Validation Paths ---------------- //////
	String mechanicGaragePath = "//*[@id=\"nav\"]/page-login/ion-content/div[2]/div[2]/form/ion-item[1]/div[1]/div/ion-input/input[1]";
	String mechanicNamePath = "//*[@id=\"nav\"]/page-login/ion-content/div[2]/div[2]/form/ion-item[2]/div[1]/div/ion-input/input[1]";
	String mechanicAcceptOrderIdPath = "//*[@id=\"nav\"]/page-home/ion-content/div[2]/div/div/div[2]/ion-card/ion-row/div/ion-row[2]";
	String finalServiceBill = "//html/body/ion-app/ng-component/ion-nav/page-bill-page/ion-content/div[2]/div[1]/ion-row[2]/ion-col[2]";
	String billJobListPath = "//html/body/ion-app/ng-component/ion-nav/page-bill-page/ion-content/div[2]/div[2]/ion-list/*/*";

	String userRegisterUpPath = "//ion-content/div[2]/div/button/span";
	String userRegisterName = "//ion-content/div[2]/div/form/ion-item[3]/div[1]/div/ion-input/input[1]";
	String userRegisterEmail = "//ion-content/div[2]/div/form/ion-item[4]/div[1]/div/ion-input/input[1]";
	String userRegisterAddress = "//ion-content/div[2]/div/ion-item[1]/div[1]/div/ion-input/input[1]";
	String userRegisterState = "//*[@id=\"sel-6-0\"]";
	String userRegisterTermsPath = "//ion-content/div[2]/div/ion-item[2]/div[1]/div/ion-label/div/span";
	String userRegisterAccept = "//ion-content/div[2]/div[18]/ion-row/ion-col[2]/button";
	String driverNamePath = "//ion-list/div[1]/ion-input/input[1]";
	String driverMobilePath = "//ion-list/div[2]/ion-input/input[1]";
	String myDriverPath = "//*[contains(text(), 'MY DRIVERS')]";
	String profileDonePath = "//page-service-page/ion-footer/div/div/button[2]";
	String profileAddDriver = "//html/body/ion-app/ng-component/ion-nav/page-add-driver/ion-footer/button";
	String prifileCreateAddVehicle = "//page-service-page/ion-footer/div/div/button";
	String mechanicRegister = "//page-register/ion-content/div[2]/div/form/button";
	String mechanicCheckBox = "//page-register/ion-content/div[2]/div/form/div/a";

	String mechanicAccountListPath = "//page-home/ion-content/div[2]/div/div/div/ion-list/*";
	String newRequestFromMyVehicles = "//page-show-vehicles/ion-content/div[2]/div/div[2]/ion-card[1]/ion-card-content/p[2]/ion-icon[3]";
	String fleetManagerWelcome_H = "//div[contains(text(),'Welcome')]";
	String buildVerion="//div[@class='version-class']";
	String buildVerionMechanic="//div[@class='version-class']";

}
