package com.servicemandi.common;

import com.relevantcodes.extentreports.LogStatus;
import com.servicemandi.common.Configuration.LocatorType;
import com.servicemandi.utils.ExcelInputData;
import com.servicemandi.utils.Utils;

import io.appium.java_client.android.AndroidKeyCode;

import org.apache.commons.lang3.concurrent.BackgroundInitializer;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

/**
 * Created by Shanthakumar on 29-01-2018.
 */

public class Mechanic extends Configuration {

	public Mechanic() {
	}

	public void login(ExcelInputData excelInputData) {

		try {

			mechanicCapabilities();
			switchToMechanicWebView();
			Utils.sleepTimeLow();
			System.out.println("AL Enter the mechanic login method ");
			String BuildVersionMechanic = getElementTextValue(mechanicDriver, LocatorPath.buildVerionMechanic);
			mechanicWait(LocatorPath.languagePath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.languagePath);
			mechanicWait(LocatorPath.signUpPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.signUpPath);
			mechanicWait(LocatorPath.mechanicMobilePath);
			String mechanicMobilePath = "//*[@id=\"nav\"]/page-otp/ion-content/div[2]/div/form/ion-item/div[1]/div/ion-input[1]";

			clickMechanicElement(LocatorType.XPATH, mechanicMobilePath);
			Thread.sleep(1000);
			System.out.println("AL mechanic number ::: " + excelInputData.getMechanicNumber());
			enterValueMechanic(LocatorType.XPATH, LocatorPath.mechanicMobilePath, excelInputData.getMechanicNumber());
			clickMechanicElement(LocatorType.XPATH, LocatorPath.requestOTP);

			mechanicWait(LocatorPath.mechanicGaragePath);

			if (getElementAttributeValue(mechanicDriver, LocatorPath.mechanicGaragePath)
					.equalsIgnoreCase(excelInputData.getSelectGarage()))
				writeReport(LogStatus.INFO, "Mechanic garage name matches");
			else
				writeReport(LogStatus.INFO, "Mechanic garage name does not match");

			if (getElementAttributeValue(mechanicDriver, LocatorPath.mechanicNamePath)
					.equalsIgnoreCase(excelInputData.getMechanicName()))
				writeReport(LogStatus.INFO, "Mechanic Name matches");
			else
				writeReport(LogStatus.INFO, "Mechanic Name does not match");

			Thread.sleep(500);
			mechanicWait(LocatorPath.mechanicLoginPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.mechanicLoginPath);
			writeReport(LogStatus.PASS, "Mechanic Login Passed");
			Thread.sleep(1000);
			mechanicWait(LocatorPath.onlineOffLinePath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.onlineOffLinePath);
			// takeScreenShotMechanicDriver();
			System.out.println("Al Build version number for mechanic :::" + BuildVersionMechanic);
			// writeReport(LogStatus.PASS, "Build Version Number for mechanic :-> " +
			// BuildVersionMechanic);
		} catch (Exception e) {
			e.printStackTrace();
			writeReport(LogStatus.ERROR, "Mechanic Login Failed");
		}
	}

	public void mechanicAcceptOrder(String orderId) {

		try {
			Thread.sleep(2000);
			mechanicWait(LocatorPath.mechanicNewRequest);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.mechanicNewRequest);
			Thread.sleep(3000);

			mechanicWait(LocatorPath.mechanicAcceptOrderPath);
			String mRequestIdNo = getElementTextValue(mechanicDriver, LocatorPath.mechanicAcceptOrderIdPath)
					.replaceAll("[^0-9]", "");
			System.out.println("Al mechanic aceept order no :::" + mRequestIdNo);
			if (mRequestIdNo.equalsIgnoreCase(orderId)) {
				writeReport(LogStatus.PASS, "Mechanic order id verified. Id Is -> " + mRequestIdNo);
			} else
				writeReport(LogStatus.INFO, "Mechanic order id verified failed ");
			clickMechanicElement(LocatorType.XPATH, LocatorPath.mechanicAcceptOrderPath);
			System.out.println("Al Mechanic accept the order ::::");
			writeReport(LogStatus.PASS, "Mechanic accept order ");
		} catch (Exception e) {
			writeReport(LogStatus.FAIL, "Mechanic accept order failed ");
		}
	}

	public void mechanicRejectOrder() {

		try {
			Thread.sleep(2000);
			mechanicWait(LocatorPath.mechanicNewRequest);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.mechanicNewRequest);
			Utils.sleepTimeMedium();
			mechanicWait(LocatorPath.mechanicRejectOrder);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.mechanicRejectOrder);
			Thread.sleep(2000);
			mechanicWait(LocatorPath.mechanicRejectReasonList);
			List<WebElement> reasonList = mechanicDriver.findElements(By.xpath(LocatorPath.mechanicRejectReasonList));
			for (int i = 0; i < reasonList.size(); i++) {
				if (i == 2) {
					reasonList.get(i).click();
					writeReport(LogStatus.PASS, "Reason ::: -> " + reasonList.get(i).getText() + " - Selected");
					Thread.sleep(1000);
					break;
				}
			}
			mechanicWait(LocatorPath.alertYesButton);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.alertYesButton);
			System.out.println("Al Mechanic reject the order ::::");
			writeReport(LogStatus.PASS, "Mechanic reject order ");
		} catch (Exception e) {
			writeReport(LogStatus.FAIL, "Mechanic accept order ");
		}
	}

	public void onGoingOrderPathClick(String vehicleNo) {

		try {

			mechanicWait(LocatorPath.onGoingOrdersTab);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.onGoingOrdersTab);
			Thread.sleep(5000);
			String onGoingPath = "//*[contains(text(), '" + vehicleNo + "')]";
			Thread.sleep(1000);
			mechanicWait(onGoingPath);
			WebElement element = getMechanicWebElement(LocatorType.XPATH, onGoingPath);

			if (element.getText().equalsIgnoreCase(vehicleNo)) {
				mechanicWait(onGoingPath);
				element.click();
				System.out.println("AL Click the Mechanic on going order :- " + element.getText());
				Thread.sleep(1000);
			}
		} catch (Exception e) {

		}
	}

	public void confirmCashReceipt(ExcelInputData excelInputData) {

		try {

			onGoingOrderPathClick(excelInputData.getVehicleNumber());
			mechanicWait(LocatorPath.alertYesButton);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.alertYesButton);
			System.out.println("AL Mechanic confirm cash receipt");
			writeReport(LogStatus.PASS, "Mechanic confirm cash receipt");
		} catch (Exception e) {
			writeReport(LogStatus.FAIL, "Mechanic confirm cash receipt failed");
		}
	}

	public void mechanicRating() {

		try {

			Thread.sleep(3000);
			mechanicWait(LocatorPath.mechanicThankYouPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.mechanicThankYouPath);
			Thread.sleep(2000);
			mechanicWait(LocatorPath.mechanicRatingPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.mechanicRatingPath);
			Thread.sleep(1000);
			mechanicWait(LocatorPath.mechanicFeedBackPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.mechanicFeedBackPath);
			Thread.sleep(1000);
			mechanicWait(LocatorPath.mechanicRatingSubmit);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.mechanicRatingSubmit);
			Thread.sleep(1000);
			writeReport(LogStatus.PASS, "Mechanic rating the order ");
			System.out.println("Al Mechanic finished the order ::");
			mechanicWait(LocatorPath.onGoingOrdersTab);
		} catch (Exception e) {
			writeReport(LogStatus.FAIL, "Mechanic rating the order failed ");
		}
	}

	public void mechanicRateCard() {

		try {
			Utils.sleepTimeLow();
			mechanicWait(LocatorPath.myAccountPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.myAccountPath);
			Thread.sleep(1000);
			mechanicWait(LocatorPath.mechanicAccountListPath);
			List<WebElement> myAccountList = mechanicDriver.findElements(By.xpath(LocatorPath.mechanicAccountListPath));
			for (int i = 0; i < myAccountList.size(); i++) {

				WebElement listElement = myAccountList.get(i);
				String listValue = listElement.getText();
				System.out.println(" AL inside loop text value ::: " + listValue);
				if (listValue.equalsIgnoreCase("My Rate Card")) {
					listElement.click();
					writeReport(LogStatus.PASS, "FM My Vehicles selected");
					Thread.sleep(1000);
					break;
				}
			}
			Utils.sleepTimeLow();

			String rateCardEdit = "//ion-content/div[2]/ion-card[1]/ion-card-content/ion-row[2]/input";
			mechanicWait(rateCardEdit);

			List<WebElement> rateCardElement = mechanicDriver.findElements(By.xpath(rateCardEdit));
			rateCardElement.get(0).clear();
			rateCardElement.get(0).clear();
			rateCardElement.get(0).sendKeys("30");
			hideKeyBoard(mechanicDriver);
			String applyButton = "//page-rate-card/ion-footer/button";
			mechanicWait(applyButton);
			clickMechanicElement(LocatorType.XPATH, applyButton);
			Thread.sleep(2000);
			mechanicWait(LocatorPath.paymentRequestOk);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.paymentRequestOk);
			Thread.sleep(2000);
			mechanicDriver.pressKeyCode(AndroidKeyCode.BACK);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void mechanicCancelOrder() {

		try {
			System.out.println("Mechanic:Inside Cancel Method");
			Utils.sleepTimeLow();
			mechanicWait(LocatorPath.onGoingOrdersTab);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.onGoingOrdersTab);
			Utils.sleepTimeLow();
			String mechaniccancel = "//*[@id=\"nav\"]/page-home/ion-content/div[2]/div/div/div[3]/div/ion-card[1]/ion-card-content/ion-row[5]/ion-col[2]";
			mechanicWait(mechaniccancel);
			clickMechanicElement(LocatorType.XPATH, mechaniccancel);
			Utils.sleepTimeLow();
			String yesBtn = "//html/body/ion-app/ion-alert/div/div[3]/button[1]/span";
			mechanicWait(yesBtn);
			clickMechanicElement(LocatorType.XPATH, yesBtn);
			Thread.sleep(2000);
			String Reason = "/html/body/ion-app/ion-alert/div/div[3]/div/button[3]/span/div[2]";
			mechanicWait(Reason);
			clickMechanicElement(LocatorType.XPATH, Reason);
			Thread.sleep(2000);
			fleetManagerDriver.pressKeyCode(AndroidKeyCode.ENTER);
			writeReport(LogStatus.PASS, "FleetManager: Job is Cancelled");

		} catch (Exception e) {

		}
	}

	public void mechenicScreenShot() {
		// takeScreenShotMechanicDriver();
	}

}
