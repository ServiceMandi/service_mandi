package com.servicemandi.common;

import com.mongodb.util.Util;
import com.relevantcodes.extentreports.LogStatus;
import com.servicemandi.common.Configuration.LocatorType;
import com.servicemandi.utils.ExcelInputData;
import com.servicemandi.utils.ReadingExcelData;
import com.servicemandi.utils.Utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by Shanthakumar on 01-Feb-18.
 */

public class ProfileCreation extends Configuration {

	public ProfileCreation() {

	}

	public void editUserProfile() {

		try {
			Utils.sleepTimeLow();
			fleetManagerWait(LocatorPath.myAccountPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.myAccountPath);
			Thread.sleep(1000);
			fleetManagerWait(LocatorPath.myAccountListPath);
			List<WebElement> myAccountList = fleetManagerDriver.findElements(By.xpath(LocatorPath.myAccountListPath));
			for (int i = 0; i < myAccountList.size(); i++) {

				WebElement listElement = myAccountList.get(i);
				String listValue = listElement.getText();
				System.out.println(" AL inside loop text value ::: " + listValue);
				if (listValue.equalsIgnoreCase("My Profile")) {
					listElement.click();
					writeReport(LogStatus.PASS, "FM MY PROFILE PASS");
					Thread.sleep(1000);
					break;
				}
			}

			LinkedHashMap<String, List<String>> userEditDetails = ReadingExcelData
					.readingFleetManagerData("FMUserData");
			List<String> mobileNumberList = userEditDetails.get("MobileNumber");

			System.out.println(" Al Enter the editFMProfile method ::: ");
			System.out.println(" AL Account list size :::" + myAccountList.size());
			String userName = userEditDetails.get("Name").get(0);
			String mobileNumber = userEditDetails.get("Mobile No").get(0);
			String userEmail = userEditDetails.get("Email").get(0);
			String userGSTIN = userEditDetails.get("GSTIN").get(0);
			String address = userEditDetails.get("Address").get(0);
			String state = userEditDetails.get("State").get(0);

			Utils.sleepTimeLow();
			fleetManagerWait(LocatorPath.editIconPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.editIconPath);
			Thread.sleep(1000);
			hideKeyBoard(fleetManagerDriver);

			Thread.sleep(1000);
			fleetManagerWait(LocatorPath.editNamePath);
			WebElement nameElement = fleetManagerDriver.findElement(By.xpath(LocatorPath.editNamePath));
			nameElement.clear();
			nameElement.clear();
			nameElement.sendKeys(userName);
			writeReport(LogStatus.PASS, "FM User Name Edited");

			/*
			 * fmWait.until(ExpectedConditions.visibilityOfElementLocated(By.
			 * xpath(Constants.editMobilePath))); WebElement mobileElement =
			 * fmDriver.findElement(By.xpath(Constants.editMobilePath));
			 * mobileElement.clear(); mobileElement.clear();
			 * mobileElement.sendKeys(mobileNumber);
			 */

			fleetManagerWait(LocatorPath.editEmailPath);
			WebElement emailElement = fleetManagerDriver.findElement(By.xpath(LocatorPath.editEmailPath));
			emailElement.clear();
			emailElement.clear();
			emailElement.sendKeys(userEmail);
			writeReport(LogStatus.PASS, "FM User E Mail Edited");

			fleetManagerWait(LocatorPath.editGSTNPath);
			WebElement GSTElement = fleetManagerDriver.findElement(By.xpath(LocatorPath.editGSTNPath));
			GSTElement.clear();
			GSTElement.clear();
			GSTElement.sendKeys(userGSTIN);

			fleetManagerWait(LocatorPath.editAddressPath);
			WebElement addressElement = fleetManagerDriver.findElement(By.xpath(LocatorPath.editAddressPath));
			addressElement.clear();
			addressElement.clear();
			addressElement.sendKeys(address);
			writeReport(LogStatus.PASS, "FM User Address Edited");

			fleetManagerWait(LocatorPath.editStatePath);
			WebElement stateElement = fleetManagerDriver.findElement(By.xpath(LocatorPath.editStatePath));
			stateElement.click();

			Thread.sleep(1000);
			fleetManagerWait(LocatorPath.stateListPath);
			List<WebElement> stateList = fleetManagerDriver.findElements(By.xpath(LocatorPath.stateListPath));
			System.out.println(" AL state list size :::" + stateList.size());

			for (int s = 0; s < stateList.size(); s++) {
				String statelist = stateList.get(s).getText();
				if (statelist.equalsIgnoreCase(state)) {
					stateList.get(s).click();
					writeReport(LogStatus.PASS, "FM User State Changed");
					Thread.sleep(1000);
					break;
				}
			}
			Thread.sleep(1000);
			fleetManagerWait(LocatorPath.alertOkPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.alertOkPath);

			/*
			 * Thread.sleep(1000);
			 * fmWait.until(ExpectedConditions.visibilityOfElementLocated(By.
			 * xpath(Constants.editTogglePath)));
			 * clickElement(LocatorType.XPATH, Constants.editTogglePath);
			 * Thread.sleep(1000);
			 */

			Thread.sleep(1000);
			fleetManagerWait(LocatorPath.savePath);
			clickFMElement(LocatorType.XPATH, LocatorPath.savePath);
			writeReport(LogStatus.PASS, "FM Edit Profile successfully");
			Thread.sleep(1000);

		} catch (Exception e) {

		}
	}

	public void addNewVehicle() {

		try {
			Utils.sleepTimeLow();

			fleetManagerWait(LocatorPath.myAccountPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.myAccountPath);
			Thread.sleep(1000);
			fleetManagerWait(LocatorPath.myAccountListPath);
			List<WebElement> myAccountList = fleetManagerDriver.findElements(By.xpath(LocatorPath.myAccountListPath));
			for (int i = 0; i < myAccountList.size(); i++) {

				WebElement listElement = myAccountList.get(i);
				String listValue = listElement.getText();
				System.out.println(" AL inside loop text value ::: " + listValue);
				if (listValue.equalsIgnoreCase("My Vehicles")) {
					listElement.click();
					writeReport(LogStatus.PASS, "FM My Vehicles selected");
					Thread.sleep(1000);
					break;
				}
			}
			Utils.sleepTimeLow();
			fleetManagerWait(LocatorPath.addVehiclePath);
			clickFMElement(LocatorType.XPATH, LocatorPath.addVehiclePath);
			createNewVehicle();

		} catch (Exception e) {

		}
	}

	public void createNewVehicle() {

		try {

			System.out.println("AL Enter the ADD vehicle createNewVehicle method ");

			LinkedHashMap<String, List<String>> vehicleDetailsList = ReadingExcelData
					.readingFleetManagerData("FMVehicle");

			String make = vehicleDetailsList.get("Select Make").get(0);
			String category = vehicleDetailsList.get("Category").get(0);
			String loadRange = vehicleDetailsList.get("Load Range").get(0);
			String tyres = vehicleDetailsList.get("Tyres").get(0);
			String modelYear = vehicleDetailsList.get("Model Year").get(0);
			String type = vehicleDetailsList.get("Type").get(0);
			String registerNo = vehicleDetailsList.get("Register No").get(0);
			String odoReading = vehicleDetailsList.get("ODO Reading").get(0);
			Utils.sleepTimeLow();

			for (int c = 2; c < 6; c++) {

				Thread.sleep(1000);
				String categoryPath = "//div[2]/div[1]/ion-list/div[" + c + "]";
				System.out.println(" AL inside path :::" + categoryPath);
				fleetManagerWait(categoryPath);
				clickFMElement(LocatorType.XPATH, categoryPath);
				Thread.sleep(1000);

				fleetManagerWait(LocatorPath.vehicleAlertPath);
				List<WebElement> categoryList = fleetManagerDriver.findElements(By.xpath(LocatorPath.vehicleAlertPath));
				System.out.println(" AL Category List  size :::" + categoryList.size());

				String checkingValue = "";
				String reportStatus = "";
				switch (c) {

				case 1:
					checkingValue = "10";
					break;
				case 2:
					checkingValue = category;
					reportStatus = "Vehicle category ";
					break;
				case 3:
					checkingValue = loadRange;
					reportStatus = "Vehicle load range ";
					break;
				case 4:
					checkingValue = tyres;
					reportStatus = "Vehicle No of Tyres ";
					break;
				case 5:
					checkingValue = modelYear;
					reportStatus = "Vehicle Model years";
					break;
				}

				for (int i = 0; i < categoryList.size(); i++) {

					WebElement listElement = categoryList.get(i);
					String listValue = listElement.getText();
					System.out.println(" AL inside loop text value ::: " + listValue);
					if (listValue.equalsIgnoreCase(checkingValue)) {
						listElement.click();
						writeReport(LogStatus.PASS, "FM Selected -> " + reportStatus);
						Thread.sleep(1000);
						break;
					}
				}

				Thread.sleep(1000);
				fleetManagerWait(LocatorPath.alertOkPath);
				clickFMElement(LocatorType.XPATH, LocatorPath.alertOkPath);
			}

			Thread.sleep(1000);
			fleetManagerWait(LocatorPath.nonElectronicPath);

			if (type.equalsIgnoreCase("Non-Electronic"))
				clickFMElement(LocatorType.XPATH, LocatorPath.nonElectronicPath);
			else
				clickFMElement(LocatorType.XPATH, LocatorPath.electronicPath);

			writeReport(LogStatus.PASS, "FM Vehicle type selected ");

			fleetManagerWait(LocatorPath.registerNoPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.registerNoPath);
			enterFMValue(LocatorType.XPATH, LocatorPath.registerNoPath, registerNo);
			writeReport(LogStatus.PASS, "FM Register number entered  ");

			/*
			 * fmWait.until(ExpectedConditions.visibilityOfElementLocated(By.
			 * xpath(Constants.readingPath))); clickElement(LocatorType.XPATH,
			 * Constants.readingPath); enterValue(LocatorType.XPATH,
			 * Constants.readingPath, "456456"); Thread.sleep(1000);
			 */
			hideKeyBoard(fleetManagerDriver);
			Thread.sleep(500);
			fleetManagerWait(LocatorPath.vehicleSubmitPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.vehicleSubmitPath);
			writeReport(LogStatus.PASS, "FM Vehicle successfully Added  ");
			Thread.sleep(1000);
		} catch (Exception e) {

		}
	}

	public void fleetManageUserCreation(ExcelInputData excelInputData) {

		try {
			fleetManagerCapabilities();
			Utils.sleepTimeLow();
			switchToFMWebView();
			Utils.sleepTimeLow();
			System.out.println(" AL Enter the Fleet Manager Login method ::: ");
			fleetManagerWait(LocatorPath.languagePath);
			clickFMElement(LocatorType.XPATH, LocatorPath.languagePath);
			fleetManagerWait(LocatorPath.signUpPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.signUpPath);
			fleetManagerWait(LocatorPath.mobilePath);
			clickFMElement(LocatorType.XPATH, LocatorPath.mobilePath);
			Thread.sleep(2000);
			enterFMValue(LocatorType.XPATH, LocatorPath.mobilePath, "8080077700");
			clickFMElement(LocatorType.XPATH, LocatorPath.requestOTP);
			fleetManagerWait(LocatorPath.userRegisterName);
			Thread.sleep(2000);
			hideKeyBoard(fleetManagerDriver);
			Thread.sleep(1000);
			fleetManagerWait(LocatorPath.userRegisterName);
			WebElement nameElement = fleetManagerDriver.findElement(By.xpath(LocatorPath.userRegisterName));
			nameElement.clear();
			nameElement.clear();
			nameElement.sendKeys("Selvam");
			fleetManagerWait(LocatorPath.userRegisterEmail);
			WebElement emailElement = fleetManagerDriver.findElement(By.xpath(LocatorPath.userRegisterEmail));
			emailElement.clear();
			emailElement.clear();
			emailElement.sendKeys("9040@gmail.com");
			fleetManagerWait(LocatorPath.userRegisterAddress);
			WebElement addressElement = fleetManagerDriver.findElement(By.xpath(LocatorPath.userRegisterAddress));
			addressElement.clear();
			addressElement.clear();
			addressElement.sendKeys("Chennai");
			hideKeyBoard(fleetManagerDriver);
			fleetManagerWait(LocatorPath.userRegisterState);
			clickFMElement(LocatorType.XPATH, LocatorPath.userRegisterState);

			Thread.sleep(1000);
			fleetManagerWait(LocatorPath.stateListPath);
			List<WebElement> stateList = fleetManagerDriver.findElements(By.xpath(LocatorPath.stateListPath));
			System.out.println(" AL state list size :::" + stateList.size());

			for (int s = 0; s < stateList.size(); s++) {
				String statelist = stateList.get(s).getText();
				if (statelist.equalsIgnoreCase("Bihar")) {
					stateList.get(s).click();
					writeReport(LogStatus.PASS, "FM User State Changed");
					Thread.sleep(500);
					break;
				}
			}
			Thread.sleep(500);
			fleetManagerWait(LocatorPath.alertOkPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.alertOkPath);
			writeReport(LogStatus.PASS, "FM User Name Edited");
			Thread.sleep(500);
			fleetManagerWait(LocatorPath.userRegisterTermsPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.userRegisterTermsPath);
			Thread.sleep(2000);
			fleetManagerWait(LocatorPath.userRegisterAccept);
			clickFMElement(LocatorType.XPATH, LocatorPath.userRegisterAccept);
			Thread.sleep(1000);
			fleetManagerWait(LocatorPath.userRegisterUpPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.userRegisterUpPath);

		} catch (Exception e) {

		}
	}

	public void createNewDriver() {

		try {

			Thread.sleep(25000);
			System.out.println(" Al Enter the Add vehicle method ::: ");
			fleetManagerWait(LocatorPath.myAccountPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.myAccountPath);
			Thread.sleep(1000);
			fleetManagerWait(LocatorPath.myAccountListPath);
			List<WebElement> myAccountList = fleetManagerDriver.findElements(By.xpath(LocatorPath.myAccountListPath));
			System.out.println(" AL Account list size :::" + myAccountList.size());

			for (int i = 0; i < myAccountList.size(); i++) {

				WebElement listElement = myAccountList.get(i);
				String listValue = listElement.getText();
				System.out.println(" AL inside loop text value ::: " + listValue);
				if (listValue.equalsIgnoreCase("My Drivers")) {
					listElement.click();
					Thread.sleep(2000);
					break;
				}
			}

			addNewDriver();
		} catch (Exception e) {

		}

	}

	public void addNewDriver() {

		try {
			Thread.sleep(5000);
			fleetManagerWait(LocatorPath.addDriverPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.addDriverPath);
			Thread.sleep(4000);
			System.out.println("AL Enter the ADD vehicle method ");

			fleetManagerWait(LocatorPath.driverNamePath);
			WebElement nameElement = fleetManagerDriver.findElement(By.xpath(LocatorPath.driverNamePath));
			nameElement.clear();
			nameElement.clear();
			nameElement.sendKeys("Balu");

			fleetManagerWait(LocatorPath.driverMobilePath);
			WebElement mobileElement = fleetManagerDriver.findElement(By.xpath(LocatorPath.driverMobilePath));
			mobileElement.clear();
			mobileElement.clear();
			mobileElement.sendKeys("6066060660");
			hideKeyBoard(fleetManagerDriver);

			fleetManagerWait(LocatorPath.profileAddDriver);
			clickFMElement(LocatorType.XPATH, LocatorPath.profileAddDriver);

		} catch (Exception e) {

		}
	}

	public void machanicUserCreation(ExcelInputData excelInputData) {

		try {

			mechanicCapabilities();
			switchToMechanicWebView();
			Utils.sleepTimeLow();
			writeReport(LogStatus.PASS, "Machanic creation entered ");
			System.out.println("AL Enter the mechanic login method ");
			mechanicWait(LocatorPath.languagePath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.languagePath);
			mechanicWait(LocatorPath.signUpPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.signUpPath);
			mechanicWait(LocatorPath.mechanicMobilePath);

			String mechanicMobilePath = "//*[@id=\"nav\"]/page-otp/ion-content/div[2]/div/form/ion-item/div[1]/div/ion-input";
			clickMechanicElement(LocatorType.XPATH, mechanicMobilePath);
			enterValueMechanic(LocatorType.XPATH, LocatorPath.mechanicMobilePath, "6326326326");
			clickMechanicElement(LocatorType.XPATH, LocatorPath.requestOTP);

			Thread.sleep(500);
			mechanicWait(LocatorPath.userRegisterName);
			WebElement nameElement = mechanicDriver.findElement(By.xpath(LocatorPath.userRegisterName));
			nameElement.clear();
			nameElement.clear();
			nameElement.sendKeys("Saravanan");
			mechanicWait(LocatorPath.userRegisterEmail);
			writeReport(LogStatus.PASS, "Machanic creation new number enterd ");

			WebElement pincodeElement = mechanicDriver.findElement(By.xpath(LocatorPath.userRegisterEmail));
			pincodeElement.clear();
			pincodeElement.clear();
			pincodeElement.sendKeys("600032");
			hideKeyBoard(mechanicDriver);

			Thread.sleep(500);

			String userRegisterTermsPath = "//page-register/ion-content/div[2]/div/form/div/span";
			mechanicWait(userRegisterTermsPath);
			clickMechanicElement(LocatorType.XPATH, userRegisterTermsPath);
			Thread.sleep(5000);

			String userRegisterAccept = "//page-termsnconditions/ion-content/div[2]/ion-row/button[2]";
			mechanicWait(userRegisterAccept);
			clickMechanicElement(LocatorType.XPATH, userRegisterAccept);
			clickMechanicElement(LocatorType.XPATH, userRegisterAccept);
			Thread.sleep(1000);
			mechanicWait(LocatorPath.mechanicCheckBox);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.mechanicCheckBox);
			mechanicWait(LocatorPath.mechanicRegister);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.mechanicRegister);
			writeReport(LogStatus.PASS, "Machanic creation successfully completed ");
			mechanicWait(LocatorPath.alertOkPath);

			clickMechanicElement(LocatorType.XPATH, mechanicMobilePath);
			enterValueMechanic(LocatorType.XPATH, LocatorPath.mechanicMobilePath, "6326326326");
			clickMechanicElement(LocatorType.XPATH, LocatorPath.requestOTP);

		} catch (Exception e) {

		}
	}
}
