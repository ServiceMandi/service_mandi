package com.servicemandi.common;

import com.servicemandi.common.Configuration.LocatorType;
import com.servicemandi.utils.Utils;

public class ServiceMandiAppLibrary extends Configuration {

	public  ServiceMandiAppLibrary() {

	}
	public void navigateToHomeScreen() throws InterruptedException {
		try {
			fleetManagerCapabilities();
			switchToFMWebView();
			Utils.sleepTimeLow();
			System.out.println(" AL Enter the Fleet Manager Login method ::: ");
			fleetManagerWait(LocatorPath.languagePath);
			clickFMElement(LocatorType.XPATH, LocatorPath.languagePath);
			fleetManagerWait(LocatorPath.signUpPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.signUpPath);
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

	public void loginFleetManager(String mobile) {
		try {
			fleetManagerWait(LocatorPath.mobilePath);
			clickFMElement(LocatorType.XPATH, LocatorPath.mobilePath);
			Thread.sleep(2000);
			enterFMValue(LocatorType.XPATH, LocatorPath.mobilePath, mobile);
			clickFMElement(LocatorType.XPATH, LocatorPath.requestOTP);
			fleetManagerWait(LocatorPath.loginPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.loginPath);
		}catch(Exception e) {
			e.printStackTrace();
		}

	}

	public void verifyOnFleetManagerScreen(String screen) {
		try {
			switch(screen) {
			case " verifyOnFleetManagerScreen":
				String homeScreenHeader=getFMWebElement(LocatorType.XPATH, LocatorPath.fleetManagerWelcome_H).getText();
				if(homeScreenHeader.equalsIgnoreCase("Welcome Hanna")) {
					System.out.println("fleet manager header verified succesfully");
				}else {
					System.out.println("fleet manager header is not present"); 
				}
				break;
			 
			default:

			}  
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

	public void tapONFleetManagerScreen(String screen) {
		try {
			switch(screen){
			case "Create Services":
				Thread.sleep(5000);
				System.out.println("Entered Create New Request method ::: ");
				fleetManagerWait(LocatorPath.createNewRequest);
				clickFMElement(LocatorType.XPATH, LocatorPath.createNewRequest);
				break;
			default:
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

}
