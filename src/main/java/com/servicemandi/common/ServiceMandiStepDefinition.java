package com.servicemandi.common;


import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ServiceMandiStepDefinition extends ServiceMandiAppLibrary {
	
	public void MandiStepDefinition(){
		
	}
	
	@Given("^user navigate to login page in fleet manager$")
	public  void user_navigate_to_login_page_in_fleet_manager()  {
	    try {
	    	navigateToHomeScreen();
	    }catch(Exception e) {
	    	e.printStackTrace();
	    }
	}

	@When("^user enter mobile number \"([^\"]*)\" in login fleet manager$")
	public void user_enter_mobile_number_in_login_fleet_manager(String mobile) {
	try {
	    loginFleetManager(mobile);
	}catch(Exception e) {
		e.printStackTrace();
	}
	}

	@Then("^verify on \"([^\"]*)\" in fleet manager$")
	public void verify_on_in_fleet_manager(String screen) {
	 try {
	   verifyOnFleetManagerScreen(screen);
	 }catch(Exception e) {
		 e.printStackTrace();
	 }
	}

	@Then("^Tap on \"([^\"]*)\" in fleet manager$")
	public void tap_on_in_fleet_manager(String screen) {
	try {
		tapONFleetManagerScreen(screen);
	}catch(Exception e) {
		 e.printStackTrace();
	}
	    
	}

	

}
