package com.servicemandi.utils;

/**
 * Created by Shanthakumar on 29-01-2018.
 */

public class ExcelInputData {

	public String mobileNumber;
	public String vehicleNumber;
	public String driverName;
	public String location;
	public String enterParts;
	public String fmSelectJobs;
	public String selectGarage;
	public String selectRetailer;
	public String labourAmount;
	public String mechanicNumber;
	public String mechanicName;
	public String bdJobSelection;
	public String RevisedBillValue1;
	public String RevisedBillValue2;
	public String multiJobCount;
	public String invalidMobileNo;

	public String getInvalidMobileNo() {
		return invalidMobileNo;
	}

	public void setInvalidMobileNo(String invalidMobileNo) {
		this.invalidMobileNo = invalidMobileNo;
	}

	public String getMultiJobCount() {
		return multiJobCount;
	}

	public void setMultiJobCount(String multiJobCount) {
		this.multiJobCount = multiJobCount;
	}

	public String getRevisedBillValue1() {
		return RevisedBillValue1;
	}

	public void setRevisedBillValue1(String revisedBillValue1) {
		this.RevisedBillValue1 = revisedBillValue1;
	}

	public String getRevisedBillValue2() {
		return RevisedBillValue2;
	}

	public void setRevisedBillValue2(String revisedBillValue2) {
		this.RevisedBillValue2 = revisedBillValue2;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getVehicleNumber() {
		return vehicleNumber;
	}

	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}

	public String getDriverName() {
		return driverName;
	}

	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getEnterParts() {
		return enterParts;
	}

	public void setEnterParts(String enterParts) {
		this.enterParts = enterParts;
	}

	public String getFmSelectJobs() {
		return fmSelectJobs;
	}

	public void setFmSelectJobs(String fmSelectJobs) {
		this.fmSelectJobs = fmSelectJobs;
	}

	public String getSelectGarage() {
		return selectGarage;
	}

	public void setSelectGarage(String selectGarage) {
		this.selectGarage = selectGarage;
	}

	public String getSelectRetailer() {
		return selectRetailer;
	}

	public void setSelectRetailer(String selectRetailer) {
		this.selectRetailer = selectRetailer;
	}

	public String getLabourAmount() {
		return labourAmount;
	}

	public void setLabourAmount(String labourAmount) {
		this.labourAmount = labourAmount;
	}

	public String getMechanicNumber() {
		return mechanicNumber;
	}

	public void setMechanicNumber(String mechanicNumber) {
		this.mechanicNumber = mechanicNumber;
	}

	public String getMechanicName() {
		return mechanicName;
	}

	public void setMechanicName(String mechanicName) {
		this.mechanicName = mechanicName;
	}

	public String getBdJobSelection() {
		return bdJobSelection;
	}

	public void setBdJobSelection(String bdJobSelection) {
		this.bdJobSelection = bdJobSelection;
	}
}
