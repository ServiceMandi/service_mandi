package com.servicemandi.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

/**
 * Created by Shanthakumar on 29-01-2018.
 */

public class ReadingExcelData {

	private static Workbook workbook = null;

	public static Workbook getWorkBook() {

		try {
			if (workbook == null) {
				String newFilePath = System.getProperty("user.dir") + "\\Service_Mandi_Input.xls";
				System.out.println("New file path :::" + newFilePath);
				FileInputStream fs = new FileInputStream(newFilePath);
				workbook = Workbook.getWorkbook(fs);
			}

		} catch (Exception e) {

		}
		return workbook;
	}

	public static void readingWorkFlowData() throws IOException, BiffException {

		if (Utils.excelInputDataList == null) {
			Sheet sheet = getWorkBook().getSheet("WorkFlowData");

			List<String> ExpectedColumns = new ArrayList<String>();

			int masterSheetColumnIndex = sheet.getColumns();

			for (int x = 0; x < masterSheetColumnIndex; x++) {
				Cell celll = sheet.getCell(x, 0);
				String d = celll.getContents();
				ExpectedColumns.add(d);
			}

			LinkedHashMap<String, List<String>> columnDataValues = new LinkedHashMap<String, List<String>>();

			List<String> column1 = new ArrayList<String>();

			/// read values from driver sheet for each column
			for (int j = 0; j < masterSheetColumnIndex; j++) {
				column1 = new ArrayList<String>();
				for (int i = 1; i < sheet.getRows(); i++) {
					Cell cell = sheet.getCell(j, i);
					column1.add(cell.getContents());
				}
				columnDataValues.put(ExpectedColumns.get(j), column1);
			}

			List<String> mobileNumberList = columnDataValues.get("MobileNumber");
			List<String> vehicleNumberList = columnDataValues.get("Vehicle Number");
			List<String> driverNameList = columnDataValues.get("Driver Number");
			List<String> locationList = columnDataValues.get("Location");
			List<String> enterParts = columnDataValues.get("Enter Parts");
			List<String> mechanicNameList = columnDataValues.get("MechanicName");
			List<String> mechanicNumberList = columnDataValues.get("MechanicNumber");
			List<String> fmSelectJobList = columnDataValues.get("FMselectJob");
			List<String> selectGarageList = columnDataValues.get("SelectGarage");
			List<String> selectRetailerList = columnDataValues.get("SelectRetailer");
			List<String> labourAmountList = columnDataValues.get("LabourAmount");
			List<String> bdJobSelectionList = columnDataValues.get("BDJobSelection");
			List<String> revisedBillValue1List = columnDataValues.get("RevisedBillValue1");
			List<String> revisedBillValue2List = columnDataValues.get("RevisedBillValue2");
			List<String> multiJobCountList = columnDataValues.get("MultiJobCount");

			ArrayList<ExcelInputData> userDetailsList = new ArrayList<ExcelInputData>();

			for (int i = 0; i < mobileNumberList.size(); i++) {
				ExcelInputData appInputDetail = new ExcelInputData();
				userDetailsList.add(appInputDetail);
				userDetailsList.get(i).setMobileNumber(mobileNumberList.get(i));
				userDetailsList.get(i).setVehicleNumber(vehicleNumberList.get(i));
				userDetailsList.get(i).setDriverName(driverNameList.get(i));
				userDetailsList.get(i).setLocation(locationList.get(i));
				userDetailsList.get(i).setMechanicNumber(mechanicNumberList.get(i));
				userDetailsList.get(i).setMechanicName(mechanicNameList.get(i));
				userDetailsList.get(i).setFmSelectJobs(fmSelectJobList.get(i));
				userDetailsList.get(i).setSelectGarage(selectGarageList.get(i));
				userDetailsList.get(i).setEnterParts(enterParts.get(i));
				userDetailsList.get(i).setSelectRetailer(selectRetailerList.get(i));
				userDetailsList.get(i).setLabourAmount(labourAmountList.get(i));
				userDetailsList.get(i).setBdJobSelection(bdJobSelectionList.get(i));
				userDetailsList.get(i).setRevisedBillValue1(revisedBillValue1List.get(i));
				userDetailsList.get(i).setRevisedBillValue2(revisedBillValue2List.get(i));
				userDetailsList.get(i).setMultiJobCount(multiJobCountList.get(i));

			}

			Utils.excelInputDataList = userDetailsList;

		}
	}

	public static LinkedHashMap<String, List<String>> readingFleetManagerData(String sheetName) {

		try {

			Sheet sheet = getWorkBook().getSheet(sheetName);
			List<String> ExpectedColumns = new ArrayList<String>();

			int masterSheetColumnIndex = sheet.getColumns();

			for (int x = 0; x < masterSheetColumnIndex; x++) {
				Cell celll = sheet.getCell(x, 0);
				String d = celll.getContents();
				ExpectedColumns.add(d);
			}

			LinkedHashMap<String, List<String>> columnDataValues = new LinkedHashMap<String, List<String>>();

			List<String> column1 = new ArrayList<String>();
			/// read values from user edit sheet for each column
			for (int j = 0; j < masterSheetColumnIndex; j++) {
				column1 = new ArrayList<String>();
				for (int i = 1; i < sheet.getRows(); i++) {
					Cell cell = sheet.getCell(j, i);
					column1.add(cell.getContents());
				}
				columnDataValues.put(ExpectedColumns.get(j), column1);
			}
			return columnDataValues;
		} catch (Exception e) {

		}
		return null;
	}

	public static void readingNegativeData() {

		try {
			if (Utils.excelInputDataList == null) {
				Sheet sheet = getWorkBook().getSheet("NegativeScenarios");

				List<String> ExpectedColumns = new ArrayList<String>();

				int masterSheetColumnIndex = sheet.getColumns();

				for (int x = 0; x < masterSheetColumnIndex; x++) {
					Cell celll = sheet.getCell(x, 0);
					String d = celll.getContents();
					ExpectedColumns.add(d);
				}

				LinkedHashMap<String, List<String>> columnDataValues = new LinkedHashMap<String, List<String>>();

				List<String> column1 = new ArrayList<String>();

				/// read values from driver sheet for each column
				for (int j = 0; j < masterSheetColumnIndex; j++) {
					column1 = new ArrayList<String>();
					for (int i = 1; i < sheet.getRows(); i++) {
						Cell cell = sheet.getCell(j, i);
						column1.add(cell.getContents());
					}
					columnDataValues.put(ExpectedColumns.get(j), column1);
				}

				List<String> mobileNumberList = columnDataValues.get("MobileNumber");
				List<String> vehicleNumberList = columnDataValues.get("Vehicle Number");
				List<String> driverNameList = columnDataValues.get("Driver Number");
				List<String> locationList = columnDataValues.get("Location");
				List<String> enterParts = columnDataValues.get("Enter Parts");
				List<String> mechanicNameList = columnDataValues.get("MechanicName");
				List<String> mechanicNumberList = columnDataValues.get("MechanicNumber");
				List<String> fmSelectJobList = columnDataValues.get("FMselectJob");
				List<String> selectGarageList = columnDataValues.get("SelectGarage");
				List<String> selectRetailerList = columnDataValues.get("SelectRetailer");
				List<String> labourAmountList = columnDataValues.get("LabourAmount");
				List<String> bdJobSelectionList = columnDataValues.get("BDJobSelection");
				List<String> revisedBillValue1List = columnDataValues.get("RevisedBillValue1");
				List<String> revisedBillValue2List = columnDataValues.get("RevisedBillValue2");
				List<String> multiJobCountList = columnDataValues.get("MultiJobCount");
				List<String> invalidMobileList = columnDataValues.get("InvalidMobileNumber");

				ArrayList<ExcelInputData> userDetailsList = new ArrayList<ExcelInputData>();

				for (int i = 0; i < mobileNumberList.size(); i++) {
					ExcelInputData appInputDetail = new ExcelInputData();
					userDetailsList.add(appInputDetail);
					userDetailsList.get(i).setMobileNumber(mobileNumberList.get(i));
					userDetailsList.get(i).setVehicleNumber(vehicleNumberList.get(i));
					userDetailsList.get(i).setDriverName(driverNameList.get(i));
					userDetailsList.get(i).setLocation(locationList.get(i));
					userDetailsList.get(i).setMechanicNumber(mechanicNumberList.get(i));
					userDetailsList.get(i).setMechanicName(mechanicNameList.get(i));
					userDetailsList.get(i).setFmSelectJobs(fmSelectJobList.get(i));
					userDetailsList.get(i).setSelectGarage(selectGarageList.get(i));
					/*userDetailsList.get(i).setEnterParts(enterParts.get(i));
					userDetailsList.get(i).setSelectRetailer(selectRetailerList.get(i));
					userDetailsList.get(i).setLabourAmount(labourAmountList.get(i));
					userDetailsList.get(i).setBdJobSelection(bdJobSelectionList.get(i));
					userDetailsList.get(i).setRevisedBillValue1(revisedBillValue1List.get(i));
					userDetailsList.get(i).setRevisedBillValue2(revisedBillValue2List.get(i));
					userDetailsList.get(i).setMultiJobCount(multiJobCountList.get(i));*/
					userDetailsList.get(i).setInvalidMobileNo(invalidMobileList.get(i));

				}

				Utils.excelInputDataList = userDetailsList;

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
