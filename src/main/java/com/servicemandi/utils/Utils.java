package com.servicemandi.utils;

import com.relevantcodes.extentreports.LogStatus;
import com.servicemandi.common.Configuration;
import com.servicemandi.common.LocatorPath;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by Shanthakumar on 29-01-2018.
 */

public class Utils {

	public static List<ExcelInputData> excelInputDataList;
	public static List<ExcelInputData> appInputDetailsList;
	private static Date previousTime;
	private static long startTime = 0;
	private static long endTime = 0;
	private static long totalTime = 0;

	public static void sleepTimeLong() throws InterruptedException {

		Thread.sleep(20000);
	}

	public static void sleepTimeMedium() throws InterruptedException {
		Thread.sleep(10000);
	}

	public static void sleepTimeLow() throws InterruptedException {
		Thread.sleep(5000);
	}

	public static String timeCalculation() {

		if (startTime == 0) {
			startTime = System.nanoTime();
		}
		endTime = System.nanoTime();
		totalTime = endTime - startTime;  
		startTime = endTime; 
		return "The time duration is " + TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS) + " Seconds";

	}

	public static void checkBillWithOutGST() {

		try {
			String finalServiceBill = Configuration.getElementTextValue(Configuration.getFleetManagerInstance(),
					LocatorPath.finalServiceBill);

			double finalBillAmount = Double.parseDouble(finalServiceBill.substring(1).replaceAll(",", ""));
			List<WebElement> billJobsList = Configuration.getFleetManagerInstance()
					.findElements(By.xpath(LocatorPath.billJobListPath));

			double cumulativeAmount = 0.0;
			double totalLabourAmount = 0.0;
			double discountAmount = 0.0;

			for (int i = 0; i < billJobsList.size(); i++) {
				String validationString = billJobsList.get(i).getText();
				if (i == 0 || i == 1) {

				} else if (validationString.equalsIgnoreCase("Total Labour Value")) {
					totalLabourAmount = Double.parseDouble(billJobsList.get((i + 1)).getText());
				} else if (validationString.equalsIgnoreCase("Discounts")) {
					discountAmount = Double.parseDouble(billJobsList.get((i + 1)).getText());
				} else {

					if (i % 2 == 1) {
						if (!billJobsList.get((i - 1)).getText().equalsIgnoreCase("Total Labour Value")
								&& !billJobsList.get((i - 1)).getText().equalsIgnoreCase("Discounts")) {
							cumulativeAmount = cumulativeAmount + Double.parseDouble(validationString);
						}
					}
				}
			}

			double finalAmount = cumulativeAmount - discountAmount;

			if (cumulativeAmount == totalLabourAmount) {
				Configuration.writeReport(LogStatus.PASS, "View Bill Total Labour Amount is Verified");
			} else
				Configuration.writeReport(LogStatus.FAIL, "View Bill Total Labour Amount is Verified Failed");

			if (finalAmount == finalBillAmount) {
				Configuration.writeReport(LogStatus.PASS, "View bill Final Amount is Verified & matches");
			} else
				Configuration.writeReport(LogStatus.FAIL, "View bill Final Amount does't match");

		} catch (Exception e) {

		}
	}

	public static void checkBillWithIGST() {

		try {

			String finalServiceBill = Configuration.getElementTextValue(Configuration.getFleetManagerInstance(),
					LocatorPath.finalServiceBill);

			double finalBillAmount = Double.parseDouble(finalServiceBill.substring(1).replaceAll(",", ""));

			List<WebElement> billJobsList = Configuration.getFleetManagerInstance()
					.findElements(By.xpath(LocatorPath.billJobListPath));

			double cumulativeAmount = 0.0;
			double totalLabourAmount = 0.0;
			double discountAmount = 0.0;
			double IGSTAmount = 0.0;

			for (int i = 0; i < billJobsList.size(); i++) {

				String validationString = billJobsList.get(i).getText();

				System.out.println("Shiva validationString :::  " + validationString);
				if (i == 0 || i == 1) {

				} else if (validationString.equalsIgnoreCase("Total Labour Value")) {
					totalLabourAmount = Double.parseDouble(billJobsList.get((i + 1)).getText());
				} else if (validationString.equalsIgnoreCase("Discounts")) {
					discountAmount = Double.parseDouble(billJobsList.get((i + 1)).getText());
				} else if (validationString.contains("IGST")) {
					String value = validationString.split("\\s+")[1];
					IGSTAmount = Double.parseDouble(value);
				} else {

					if (i % 2 == 1) {
						if (!billJobsList.get((i - 1)).getText().equalsIgnoreCase("Total Labour Value")
								&& !billJobsList.get((i - 1)).getText().equalsIgnoreCase("Discounts")
								&& !billJobsList.get((i - 1)).getText().contains("IGST")) {
							cumulativeAmount = cumulativeAmount + Double.parseDouble(validationString);
						}
					}
				}
			}

			double IGSTValidationAmount = (totalLabourAmount / 100) * 18;
			double finalAmount = totalLabourAmount + IGSTValidationAmount;
			double finalValidationAmount = finalAmount - discountAmount;

			if (cumulativeAmount == totalLabourAmount) {
				Configuration.writeReport(LogStatus.PASS, "View Bill Total Labour Amount is Verified");
			} else
				Configuration.writeReport(LogStatus.FAIL, "View Bill Total Labour Amount is Verified failed");

			if (IGSTValidationAmount == IGSTAmount) {
				Configuration.writeReport(LogStatus.PASS, "View Bill IGST amount is Verified");
			} else
				Configuration.writeReport(LogStatus.FAIL, "View Bill IGST amount is Verified failed");

			if (finalBillAmount == finalValidationAmount) {
				Configuration.writeReport(LogStatus.PASS, "View Bill Final service bill amount is Verified");
			} else
				Configuration.writeReport(LogStatus.FAIL, "View Bill Final service bill amount is Verified failed");

		} catch (Exception e) {

		}
	}

	public static void checkBillWithGST() {

		try {

			String finalServiceBill = Configuration.getElementTextValue(Configuration.getFleetManagerInstance(),
					LocatorPath.finalServiceBill);

			double finalBillAmount = Double.parseDouble(finalServiceBill.substring(1).replaceAll(",", ""));

			List<WebElement> billJobsList = Configuration.getFleetManagerInstance()
					.findElements(By.xpath(LocatorPath.billJobListPath));

			double cumulativeAmount = 0.0;
			double totalLabourAmount = 0.0;
			double discountAmount = 0.0;
			double SGSTAmount = 0.0;
			double CGSTAmount = 0.0;
			double IGSTAmount = 0.0;

			for (int i = 0; i < billJobsList.size(); i++) {

				String validationString = billJobsList.get(i).getText();

				System.out.println("View Bill Validation String :::  " + validationString);
				if (i == 0 || i == 1) {

				} else if (validationString.equalsIgnoreCase("Total Labour Value")) {
					totalLabourAmount = Double.parseDouble(billJobsList.get((i + 1)).getText());
				} else if (validationString.equalsIgnoreCase("Discounts")) {
					discountAmount = Double.parseDouble(billJobsList.get((i + 1)).getText());
				} else if (validationString.contains("SGST")) {
					String value = validationString.split("\\s+")[1];
					SGSTAmount = Double.parseDouble(value);
				} else if (validationString.contains("CGST")) {
					String value = validationString.split("\\s+")[1];
					CGSTAmount = Double.parseDouble(value);
				} else if (validationString.contains("IGST")) {
					String value = validationString.split("\\s+")[1];
					IGSTAmount = Double.parseDouble(value);
				} else {

					if (i % 2 == 1) {
						if (!billJobsList.get((i - 1)).getText().equalsIgnoreCase("Total Labour Value")
								&& !billJobsList.get((i - 1)).getText().equalsIgnoreCase("Discounts")
								&& !billJobsList.get((i - 1)).getText().contains("SGST")
								&& !billJobsList.get((i - 1)).getText().contains("CGST")) {
							cumulativeAmount = cumulativeAmount + Double.parseDouble(validationString);
						}
					}
				}
			}

			double SGSTValidationAmount = (totalLabourAmount / 100) * 9;
			double CGSTValidationAmount = (totalLabourAmount / 100) * 9;
			double finalAmount = totalLabourAmount + SGSTValidationAmount + CGSTValidationAmount;
			double finalValidationAmount = finalAmount - discountAmount;

			if (cumulativeAmount == totalLabourAmount)
				Configuration.writeReport(LogStatus.PASS, "Total Labour Amount Verified ");
			else
				Configuration.writeReport(LogStatus.FAIL, "Total Labour Amount Verified failed");

			if (SGSTValidationAmount == SGSTAmount)
				Configuration.writeReport(LogStatus.PASS, "SGSTAmount Verified ");
			else
				Configuration.writeReport(LogStatus.FAIL, "SGST Amount Verified failed");

			if (CGSTValidationAmount == CGSTAmount)
				Configuration.writeReport(LogStatus.PASS, "CGST Amount Verified ");
			else
				Configuration.writeReport(LogStatus.FAIL, "CGST Amount Verified failed");

			if (finalBillAmount == finalValidationAmount)
				Configuration.writeReport(LogStatus.PASS, "Final Validation Amount Verified ");
			else
				Configuration.writeReport(LogStatus.FAIL, "Final bill Amount Verified failed");

		} catch (Exception e) {

		}
	}
}
