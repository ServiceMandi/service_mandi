package com.servicemandi.workflow;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.servicemandi.breakdown.BreakDownApproveEstimate;
import com.servicemandi.breakdown.BreakDownStartJob;
import com.servicemandi.breakdown.BreakDownTrackLocation;
import com.servicemandi.breakdown.CreateBreakDownRequest;
import com.servicemandi.common.Configuration;
import com.servicemandi.common.FleetManager;
import com.servicemandi.common.Mechanic;
import com.servicemandi.utils.ReadingExcelData;
import com.servicemandi.utils.Utils;

/**
 * Created by Shanthakumar on 29-01-2018.
 */

public class BreakDownFlow {

	private FleetManager fleetManager;
	private Mechanic mechanic;
	private CreateBreakDownRequest breakDownRequest;
	private BreakDownTrackLocation trackLocation;
	private BreakDownApproveEstimate approveEstimate;
	private BreakDownStartJob startJob;
	private String mRequestOrderId = "";

	@BeforeTest
	public void setUpConfiguration() {

		try {

			ReadingExcelData.readingWorkFlowData();
			fleetManager = new FleetManager();
			mechanic = new Mechanic();
			breakDownRequest = new CreateBreakDownRequest();
			trackLocation = new BreakDownTrackLocation();
			approveEstimate = new BreakDownApproveEstimate();
			startJob = new BreakDownStartJob();
			Configuration.reportConfiguration();
			Configuration.createNewWorkFlowReport("Login Details");
			fleetManager.login(Utils.excelInputDataList.get(1));
			mechanic.login(Utils.excelInputDataList.get(1));
			Configuration.closeWorkFlowReport();

		} catch (Exception e) {

		}
	}

	@BeforeClass
	public void beforeClass() {

	}

	@AfterTest
	public void afterTest() {
		Configuration.saveReport();
	}

	@AfterMethod
	public void afterMethod() {
		Configuration.closeWorkFlowReport();
	}

	@Test(enabled = false)
	public void BDFMCancelAllJobsInApproveEstimate() {

		try {

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== BreakDown Parts Cancel All Jobs =====");
				Configuration.createNewWorkFlowReport("BDFMCancelAllJobsInApproveEstimate");
				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(1));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(1));
				trackLocation.giveEstimate(Utils.excelInputDataList.get(1), mechanic);
				trackLocation.trackLocationWithMultiJobs(Utils.excelInputDataList.get(1));
				trackLocation.selectRetailer(Utils.excelInputDataList.get(1));
				approveEstimate.approveEstimateCancelAllJobs(Utils.excelInputDataList.get(1), fleetManager);
				fleetManager.viewBill(Utils.excelInputDataList.get(1));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(1));
				mechanic.mechanicRating();
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(1));

			}
		} catch (Exception e) {

		}
	}

	@Test
	public void BDWithoutPartsFMCancelJobInAproveEstimate() {

		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== BDWithoutPartsFMCancelJobInAproveEstimate =====");
				Configuration.createNewWorkFlowReport("BDWithoutPartsFMCancelJobInAproveEstimate");
				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(1));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(1));
				trackLocation.giveEstimate(Utils.excelInputDataList.get(1), mechanic);
				trackLocation.trackLocationWithOutParts(Utils.excelInputDataList.get(1));
				approveEstimate.approveEstimateCancelOrder(Utils.excelInputDataList.get(1), fleetManager);
				fleetManager.viewBill(Utils.excelInputDataList.get(1));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(1));
				mechanic.mechanicRating();
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(1));

			}
		} catch (Exception e) {
		}
	}

	@Test
	public void breakDownFMCancelAtConfirmGarage() {

		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Break Down FM Cancel At Confirm Garage =====");
				Configuration.createNewWorkFlowReport("BreakDownFMCancelAtConfirmGarage");

				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(1));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				// fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(1));
				fleetManager.cancelOrder();
				/*
				 * fleetManager.viewBill(Utils.excelInputDataList.get(1));
				 * mechanic.confirmCashReceipt(Utils.excelInputDataList.get(1));
				 * mechanic.mechanicRating();
				 * fleetManager.fleetManagerRating(Utils.excelInputDataList.get(1));
				 */

			}
		} catch (Exception e) {
		}
	}

	@Test
	public void breakDownWithPartsNoRetailerRevisedEstimateWithoutParts() {

		try {

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Break Down With Parts NoRetailer Revised Estimate Without Parts =====");
				Configuration.createNewWorkFlowReport("breakDownWithPartsNoRetailerRevisedEstimateWithoutParts");

				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(1));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(1));
				trackLocation.giveEstimate(Utils.excelInputDataList.get(1), mechanic);
				trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(1));
				trackLocation.withOutRetailer(Utils.excelInputDataList.get(1));
				approveEstimate.approveEstimateWithAlert(Utils.excelInputDataList.get(1), fleetManager);
				startJob.revisedEstimationWithOutParts(Utils.excelInputDataList.get(1), mechanic);
				approveEstimate.ApproveRevisedEstimateWithoutPay(Utils.excelInputDataList.get(1), fleetManager);
				trackLocation.UpdateJobStatus(Utils.excelInputDataList.get(1), mechanic);
				fleetManager.viewBill(Utils.excelInputDataList.get(1));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(1));
				mechanic.mechanicRating();
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(1));

			}
		} catch (Exception e) {
		}
	}

	@Test
	public void BDWithPartsNoRetailerRevisedEstimateWithPartsNoRetailer() {

		try {

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 1) {
				System.out.println(
						"===== Break Down With Parts NoRetailer Revised Estimate With Parts No Retailer =====");
				Configuration.createNewWorkFlowReport("BDWithPartsNoRetailerRevisedEstimateWithPartsNoRetailer");

				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(1));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(1));
				trackLocation.giveEstimate(Utils.excelInputDataList.get(1), mechanic);
				trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(1));
				trackLocation.withOutRetailer(Utils.excelInputDataList.get(1));
				approveEstimate.approveEstimateWithAlert(Utils.excelInputDataList.get(1), fleetManager);
				startJob.revisedEstimationWithParts(Utils.excelInputDataList.get(1), mechanic);
				trackLocation.withOutRetailer(Utils.excelInputDataList.get(1));
				approveEstimate.ApproveRevisedEstimateWithAlert(Utils.excelInputDataList.get(1), fleetManager);
				trackLocation.UpdateJobStatus(Utils.excelInputDataList.get(1), mechanic);
				fleetManager.viewBill(Utils.excelInputDataList.get(1));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(1));
				mechanic.mechanicRating();
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(1));

			}
		} catch (Exception e) {
		}
	}

	@Test
	public void breakDownWithoutPartsRevisedEstimateWithParts() {

		try {

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Break Down Without Parts Revised Estimate With Parts 8=====");
				Configuration.createNewWorkFlowReport("breakDownWithoutPartsRevisedEstimateWithParts");

				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(1));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(1));
				trackLocation.giveEstimate(Utils.excelInputDataList.get(1), mechanic);
				trackLocation.trackLocationWithOutParts(Utils.excelInputDataList.get(1));
				approveEstimate.approveEstimateWithoutPayCash(Utils.excelInputDataList.get(1), fleetManager);
				startJob.revisedEstimationWithParts(Utils.excelInputDataList.get(1), mechanic);
				trackLocation.selectRetailer(Utils.excelInputDataList.get(1));
				approveEstimate.ApproveRevisedEstimateWithPayCash(Utils.excelInputDataList.get(1), fleetManager);
				trackLocation.UpdateJobStatus(Utils.excelInputDataList.get(1), mechanic);
				fleetManager.viewBill(Utils.excelInputDataList.get(1));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(1));
				mechanic.mechanicRating();
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(1));

			}
		} catch (Exception e) {
		}
	}

	@Test
	public void breakDownWithoutPartsRevisedEstimateWithOutParts() {

		try {

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Break Down Without Parts Revised Estimate Without Parts 12=====");
				Configuration.createNewWorkFlowReport("breakDownWithoutPartsRevisedEstimateWithOutParts");

				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(1));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(1));
				trackLocation.giveEstimate(Utils.excelInputDataList.get(1), mechanic);
				trackLocation.trackLocationWithOutParts(Utils.excelInputDataList.get(1));
				approveEstimate.approveEstimateWithoutPayCash(Utils.excelInputDataList.get(1), fleetManager);
				startJob.revisedEstimationWithOutParts(Utils.excelInputDataList.get(1), mechanic);
				approveEstimate.ApproveRevisedEstimateWithoutPay(Utils.excelInputDataList.get(1), fleetManager);
				trackLocation.UpdateJobStatus(Utils.excelInputDataList.get(1), mechanic);
				fleetManager.viewBill(Utils.excelInputDataList.get(1));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(1));
				mechanic.mechanicRating();
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(1));

			}
		} catch (Exception e) {
		}
	}

	@Test
	public void breakDownWithoutPartsRevisedBill() {

		try {

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Break Down Without Parts Revised Bill 16=====");
				Configuration.createNewWorkFlowReport("breakDownWithoutPartsRevisedBill");

				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(1));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(1));
				trackLocation.giveEstimate(Utils.excelInputDataList.get(1), mechanic);
				trackLocation.trackLocationWithOutParts(Utils.excelInputDataList.get(1));
				approveEstimate.approveEstimateWithoutPayCash(Utils.excelInputDataList.get(1), fleetManager);
				trackLocation.UpdateJobStatusRevisedBill(Utils.excelInputDataList.get(1), mechanic);
				fleetManager.viewRevisedBill(Utils.excelInputDataList.get(1));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(1));
				mechanic.mechanicRating();
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(1));

			}
		} catch (Exception e) {
		}
	}

	@Test
	public void breakDownPartsRevisedWithPartsRevisedBill() {

		try {

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Break Down Parts Revised Estimate Parts RevisedBill 22=====");
				Configuration.createNewWorkFlowReport("breakDownPartsRevisedWithPartsRevisedBill");
				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(1));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(1));
				trackLocation.giveEstimate(Utils.excelInputDataList.get(1), mechanic);
				trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(1));
				trackLocation.selectRetailer(Utils.excelInputDataList.get(1));
				approveEstimate.approveEstimateWithPayCash(Utils.excelInputDataList.get(1), fleetManager);
				startJob.revisedEstimationWithParts(Utils.excelInputDataList.get(1), mechanic);
				trackLocation.selectRetailer(Utils.excelInputDataList.get(1));
				approveEstimate.ApproveRevisedEstimateWithPayCash(Utils.excelInputDataList.get(1), fleetManager);
				trackLocation.UpdateJobStatusRevisedBill(Utils.excelInputDataList.get(1), mechanic);
				fleetManager.viewRevisedBill(Utils.excelInputDataList.get(1));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(1));
				mechanic.mechanicRating();
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(1));

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void breakDownWithPartsRevisedEstimateWithOutPartsRevisedBill() {

		try {

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== breakDownWithPartsRevisedEstimateWithOutPartsRevisedBill 22=====");
				Configuration.createNewWorkFlowReport("breakDownWithPartsRevisedEstimateWithOutPartsRevisedBill");

				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(1));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(1));
				trackLocation.giveEstimate(Utils.excelInputDataList.get(1), mechanic);
				trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(1));
				trackLocation.selectRetailer(Utils.excelInputDataList.get(1));
				approveEstimate.approveEstimateWithPayCash(Utils.excelInputDataList.get(1), fleetManager);
				startJob.revisedEstimationWithOutParts(Utils.excelInputDataList.get(1), mechanic);
				approveEstimate.ApproveRevisedEstimateWithoutPay(Utils.excelInputDataList.get(1), fleetManager);
				trackLocation.UpdateJobStatusRevisedBill(Utils.excelInputDataList.get(1), mechanic);
				fleetManager.viewRevisedBill(Utils.excelInputDataList.get(1));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(1));
				mechanic.mechanicRating();
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(1));

			}
		} catch (Exception e) {
		}
	}

	@Test
	public void BDWithoutPartsRevisedEstimateWithoutPartsRevisedBill() {

		try {

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== BreakDown Without Parts Revised Estimate Without Parts Revised Bill 24=====");
				Configuration.createNewWorkFlowReport("BDWithoutPartsRevisedEstimateWithoutPartsRevisedBill");

				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(1));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(1));
				trackLocation.giveEstimate(Utils.excelInputDataList.get(1), mechanic);
				trackLocation.trackLocationWithOutParts(Utils.excelInputDataList.get(1));
				approveEstimate.approveEstimateWithoutPayCash(Utils.excelInputDataList.get(1), fleetManager);
				startJob.revisedEstimationWithOutParts(Utils.excelInputDataList.get(1), mechanic);
				approveEstimate.ApproveRevisedEstimateWithoutPay(Utils.excelInputDataList.get(1), fleetManager);
				trackLocation.UpdateJobStatusRevisedBill(Utils.excelInputDataList.get(1), mechanic);
				fleetManager.viewRevisedBill(Utils.excelInputDataList.get(1));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(1));
				mechanic.mechanicRating();
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(1));

			}
		} catch (Exception e) {
		}
	}
	// 3rd iteration Workflowsover-rest to be taken from SANTA.

	// Starting Rest of the Breakdown Flows from here:
	/// Duplicate flow
	@Test
	public void breakDownWithoutPartsRevisedEstimateWithPartsRevisedbill() {

		try {

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== BreakDownWithoutPartsRevisedEstimateWithPartsRevisedbill 21=====");
				Configuration.createNewWorkFlowReport("BreakDownWithoutPartsRevisedEstimateWithPartsRevisedbill");

				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(1));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(1));
				trackLocation.giveEstimate(Utils.excelInputDataList.get(1), mechanic);
				trackLocation.trackLocationWithOutParts(Utils.excelInputDataList.get(1));
				approveEstimate.approveEstimateWithoutPayCash(Utils.excelInputDataList.get(1), fleetManager);
				startJob.revisedEstimationWithParts(Utils.excelInputDataList.get(1), mechanic);
				trackLocation.selectRetailer(Utils.excelInputDataList.get(1));
				approveEstimate.ApproveRevisedEstimateWithPayCash(Utils.excelInputDataList.get(1), fleetManager);
				trackLocation.UpdateJobStatusRevisedBill(Utils.excelInputDataList.get(1), mechanic);
				fleetManager.viewRevisedBill(Utils.excelInputDataList.get(1));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(1));
				mechanic.mechanicRating();
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(1));

			}
		} catch (Exception e) {
		}
	}

	@Test
	public void breakDownWithPartsWithOutRetailer() {

		try {

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== break Down WithParts With OutRetailer =====");
				Configuration.createNewWorkFlowReport("breakDownWithPartsWithOutRetailer");

				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(1));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(1));
				trackLocation.giveEstimate(Utils.excelInputDataList.get(1), mechanic);
				trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(1));
				trackLocation.withOutRetailer(Utils.excelInputDataList.get(1));
				approveEstimate.approveEstimateWithAlert(Utils.excelInputDataList.get(1), fleetManager);
				startJob.startJobs(Utils.excelInputDataList.get(1), mechanic);
				fleetManager.viewBill(Utils.excelInputDataList.get(1));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(1));
				mechanic.mechanicRating();
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(1));

			}
		} catch (Exception e) {
		}
	}

	@Test
	public void breakDownWithParts() {

		try {

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Break Down With Parts Normal Flow =====");
				Configuration.createNewWorkFlowReport("breakDownWithParts");

				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(1));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(1));
				trackLocation.giveEstimate(Utils.excelInputDataList.get(1), mechanic);
				trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(1));
				trackLocation.selectRetailer(Utils.excelInputDataList.get(1));
				approveEstimate.approveEstimateWithPayCash(Utils.excelInputDataList.get(1), fleetManager);
				startJob.startJobs(Utils.excelInputDataList.get(1), mechanic);
				fleetManager.viewBill(Utils.excelInputDataList.get(1));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(1));
				mechanic.mechanicRating();
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(1));

			}
		} catch (Exception e) {
		}
	}

	@Test
	public void breakDownWithPartsRevisedBill() {

		try {

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Break Down With Parts Revised Bill =====");
				Configuration.createNewWorkFlowReport("breakDownWithPartsRevisedBill");
				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(1));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(1));
				trackLocation.giveEstimate(Utils.excelInputDataList.get(1), mechanic);
				trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(1));
				trackLocation.selectRetailer(Utils.excelInputDataList.get(1));
				approveEstimate.approveEstimateWithPayCash(Utils.excelInputDataList.get(1), fleetManager);
				startJob.startJobs(Utils.excelInputDataList.get(1), mechanic);
				fleetManager.viewRevisedBill(Utils.excelInputDataList.get(1));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(1));
				mechanic.mechanicRating();
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(1));
			}
		} catch (Exception e) {
		}
	}

	@Test
	public void breakDownWithOutParts() {

		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Break Down With out Parts Normal Flow =====");
				Configuration.createNewWorkFlowReport("breakDownWithParts");

				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(1));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(1));
				trackLocation.giveEstimate(Utils.excelInputDataList.get(1), mechanic);
				trackLocation.trackLocationWithOutParts(Utils.excelInputDataList.get(1));
				// trackLocation.selectRetailer(Utils.excelInputDataList.get(1));
				approveEstimate.approveEstimateWithoutPayCash(Utils.excelInputDataList.get(1), fleetManager);
				startJob.startJobs(Utils.excelInputDataList.get(1), mechanic);
				fleetManager.viewBill(Utils.excelInputDataList.get(1));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(1));
				mechanic.mechanicRating();
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(1));

			}
		} catch (Exception e) {
		}
	}

	@Test
	public void breakDownWithOutPartsRevisedEstimateWithPartsNoRetailer() {

		try {

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Break Down With out Parts Revised Estimate With Parts No Retailer =====");
				Configuration.createNewWorkFlowReport("breakDownWithOutPartsRevisedEstimateWithPartsNoRetailer");

				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(1));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(1));
				trackLocation.giveEstimate(Utils.excelInputDataList.get(1), mechanic);
				trackLocation.trackLocationWithOutParts(Utils.excelInputDataList.get(1));
				approveEstimate.approveEstimateWithoutPayCash(Utils.excelInputDataList.get(1), fleetManager);
				startJob.revisedEstimationWithParts(Utils.excelInputDataList.get(1), mechanic);
				trackLocation.withOutRetailer(Utils.excelInputDataList.get(1));
				approveEstimate.ApproveRevisedEstimateWithAlert(Utils.excelInputDataList.get(1), fleetManager);
				trackLocation.UpdateJobStatus(Utils.excelInputDataList.get(1), mechanic);
				fleetManager.viewBill(Utils.excelInputDataList.get(1));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(1));
				mechanic.mechanicRating();
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(1));

			}
		} catch (Exception e) {
		}
	}

	@Test
	public void breakDownWithPartsRevisedEstimateWithOutParts() {

		try {

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Break Down With Parts Revised Estimate With out Parts =====");
				Configuration.createNewWorkFlowReport("breakDownWithPartsRevisedEstimateWithOutParts");

				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(1));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(1));
				trackLocation.giveEstimate(Utils.excelInputDataList.get(1), mechanic);
				trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(1));
				trackLocation.selectRetailer(Utils.excelInputDataList.get(1));
				approveEstimate.approveEstimateWithPayCash(Utils.excelInputDataList.get(1), fleetManager);
				startJob.revisedEstimationWithOutParts(Utils.excelInputDataList.get(1), mechanic);
				approveEstimate.ApproveRevisedEstimateWithoutPay(Utils.excelInputDataList.get(1), fleetManager);
				trackLocation.UpdateJobStatus(Utils.excelInputDataList.get(1), mechanic);
				fleetManager.viewBill(Utils.excelInputDataList.get(1));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(1));
				mechanic.mechanicRating();
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(1));

			}
		} catch (Exception e) {
		}
	}

	@Test
	public void breakDownWithPartsRevisedEstimateWithParts() {

		try {

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Break Down With Parts Revised Estimate With Parts =====");
				Configuration.createNewWorkFlowReport("breakDownWithPartsRevisedEstimateWithParts");

				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(1));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(1));
				trackLocation.giveEstimate(Utils.excelInputDataList.get(1), mechanic);
				trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(1));
				trackLocation.selectRetailer(Utils.excelInputDataList.get(1));
				approveEstimate.approveEstimateWithPayCash(Utils.excelInputDataList.get(1), fleetManager);
				startJob.revisedEstimationWithParts(Utils.excelInputDataList.get(1), mechanic);
				trackLocation.selectRetailer(Utils.excelInputDataList.get(1));
				approveEstimate.ApproveRevisedEstimateWithPayCash(Utils.excelInputDataList.get(1), fleetManager);
				trackLocation.UpdateJobStatus(Utils.excelInputDataList.get(1), mechanic);
				fleetManager.viewBill(Utils.excelInputDataList.get(1));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(1));
				mechanic.mechanicRating();
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(1));

			}
		} catch (Exception e) {
		}
	}

	@Test
	public void breakDownMechanicRejectsOrder() {

		try {

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Break Down Mechanic Rejects Order =====");
				Configuration.createNewWorkFlowReport("breakDownWithParts");
				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(1));
				mechanic.mechanicRejectOrder();

			}
		} catch (Exception e) {

		}
	}

	@Test
	public void BDMultiJobsWithoutPartsFMCancelOneJobInApproveEstimate() {
		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("=====BDMultiJobsWithoutPartsFMCancelOneJobInApproveEstimate 35-NS=====");
				Configuration.createNewWorkFlowReport("BDMultiJobsWithoutPartsFMCancelOneJobInApproveEstimate 35-NS");
				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(1));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(1));
				trackLocation.giveEstimate(Utils.excelInputDataList.get(1), mechanic);
				trackLocation.trackLocationWithMultiJobsWithOutParts(Utils.excelInputDataList.get(1));
				approveEstimate.approveEstimateCancelOneJobs(Utils.excelInputDataList.get(1), fleetManager);
				startJob.startJobs(Utils.excelInputDataList.get(1), mechanic);
				fleetManager.viewBill(Utils.excelInputDataList.get(1));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(1));
				mechanic.mechanicRating();
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(1));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void BDFMDeletesAtVehicleInGarage() throws InterruptedException {

		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("=====BDFMDeleteAtVehicleInGarage=====");
				Configuration.createNewWorkFlowReport("BDFMDeleteAtVehicleInGarage");
				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(1));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(1));
				trackLocation.giveEstimateReturnBack(Utils.excelInputDataList.get(1), mechanic);
				fleetManager.cancelOrder();
				fleetManager.viewBill(Utils.excelInputDataList.get(1));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(1));
				mechanic.mechanicRating();
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(1));
			}
		} catch (Exception e) {
		}
	}

	@Test
	public void BDMechanicDeletesAtTrackLoc() {
		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("=====BDMechanicDeletesAtTrackLoc 47=====");
				Configuration.createNewWorkFlowReport("BDMechanicDeletesAtTrackLoc 47");
				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(1));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(1));
				mechanic.mechanicCancelOrder();
			}
		} catch (Exception e) {

		}
	}

	@Test
	public void BDFMDeletesAtTrackLoc() {
		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== BDMechanicDeletesAtTrackLoc 47=====");
				Configuration.createNewWorkFlowReport("BDMechanicDeletesAtTrackLoc 47");
				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(1));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.fleetManagerConfirmGarage(Utils.excelInputDataList.get(1));
				fleetManager.cancelOrder();
				fleetManager.viewBill(Utils.excelInputDataList.get(1));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(1));
				mechanic.mechanicRating();
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(1));
			}
		} catch (Exception e) {

		}
	}

	@Test
	public void BDFMDeletesAtFindGarage() {
		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("=====BDFMDeletesAtFindGarage 38=====");
				Configuration.createNewWorkFlowReport("BDFMDeletesAtFindGarage 38");
				mRequestOrderId = breakDownRequest.breakDownNewRequest(Utils.excelInputDataList.get(1));
				fleetManager.cancelOrder();
				Configuration.saveReport();
			}
		} catch (Exception e) {

		}
	}
}
