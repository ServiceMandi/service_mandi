package com.servicemandi.workflow;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.servicemandi.common.Configuration;

public class ExampleTest {

	@BeforeClass
	public void beforeClass() {
		System.out.println("AL Enter the beforeClass method :::");
		// Configuration.reportConfiguration("serviceRequestWithoutParts");
	}

	@AfterMethod
	public void afterMethod() {
		// Configuration.closeWorkFlowReport();
		System.out.println("AL Enter the afterMethod method :::");
	}

	@Test
	public void test1() {
		System.out.println("AL Enter the test1 method :::");
	}

	@Test
	public void test2() {
		System.out.println("AL Enter the test2 method :::");
	}

	@Test
	public void test3() {
		System.out.println("AL Enter the test3 method :::");
	}

	@Test
	public void test4() {
		System.out.println("AL Enter the test4 method :::");
	}

	@Test
	public void test5() {
		System.out.println("AL Enter the test5 method :::");
	}

	@AfterTest
	public void afterTest() {

		System.out.println("AL Enter the after Test Test method :::");
		File file = new File(System.getProperty("user.dir") + File.separator + "Reports.html");
		String fileName = new Date().getTime() + "'.html'";

		try {
			file.renameTo(
					new File(System.getProperty("user.dir") + File.separator + "Reports" + File.separator + fileName));
			// if file copied successfully then delete the original file
			System.out.println("File moved successfully");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
