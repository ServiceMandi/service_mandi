package com.servicemandi.workflow;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.servicemandi.common.Configuration;
import com.servicemandi.common.FleetManager;
import com.servicemandi.common.Mechanic;
import com.servicemandi.common.ProfileCreation;
import com.servicemandi.utils.ReadingExcelData;
import com.servicemandi.utils.Utils;
import com.sevicemandi.servicerequest.CreateServiceRequest;
import com.sevicemandi.servicerequest.ServiceRequestApproveEstimate;
import com.sevicemandi.servicerequest.ServiceRequestStartJob;
import com.sevicemandi.servicerequest.ServicesRequestTrackLocation;

/**
 * Created by Shanthakumar on 29-01-2018.
 */

public class ServiceRequestFlow {

	private FleetManager fleetManager;
	private Mechanic mechanic;
	private CreateServiceRequest createServiceRequest;
	private ServicesRequestTrackLocation trackLocation;
	private ServiceRequestApproveEstimate approveEstimate;
	private ServiceRequestStartJob startJob;
	private String mRequestOrderId = "";
	private ProfileCreation mProfileCreation;

	@BeforeTest
	public void setUpConfiguration() {

		try {
			ReadingExcelData.readingWorkFlowData();
			fleetManager = new FleetManager();
			mechanic = new Mechanic();
			createServiceRequest = new CreateServiceRequest();
			trackLocation = new ServicesRequestTrackLocation();
			approveEstimate = new ServiceRequestApproveEstimate();
			startJob = new ServiceRequestStartJob();
			mProfileCreation = new ProfileCreation();
			Configuration.reportConfiguration();
			Configuration.createNewWorkFlowReport("Login Details");
			fleetManager.login(Utils.excelInputDataList.get(1));
			mechanic.login(Utils.excelInputDataList.get(1));
			Configuration.closeWorkFlowReport();
		} catch (Exception e) {

		}
	}

	@AfterTest
	public void afterTest() {
		Configuration.saveReport();
	}

	@AfterSuite
	public void moveFile() {
		/*
		 * File file = new File(System.getProperty("user.dir") + File.separator +
		 * "Reports.html"); String fileName = new
		 * SimpleDateFormat("yyyyMMddHHmm'.txt'").format(new Date()); if
		 * (file.renameTo(new File( System.getProperty("user.dir") + File.separator +
		 * "Reports" + File.separator + fileName + ".html"))) { // if file copied
		 * successfully then delete the original file
		 * System.out.println("File moved successfully"); } else {
		 * System.out.println("Failed to move the file"); }
		 */
	}

	@AfterMethod
	public void afterMethod() {
		Configuration.closeWorkFlowReport();
	}

	@Test()
	public void serviceRequestWithoutParts() {

		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Service Request With out  Parts Normal Flow =====");

				Configuration.createNewWorkFlowReport("serviceRequestWithoutParts");
				createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(1));
				createServiceRequest.selectJobs();
				mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(1));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				trackLocation.giveEstimate(Utils.excelInputDataList.get(1), mechanic);
				trackLocation.trackLocationWithOutParts(Utils.excelInputDataList.get(1));
				startJob.startJobs(Utils.excelInputDataList.get(1), mechanic);
				fleetManager.viewBill(Utils.excelInputDataList.get(1));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(1));
				mechanic.mechanicRating();
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(1));

			}
		} catch (Exception e) {
		}
	}

	@Test
	public void serviceRequestWithParts() {

		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Service Request With Parts Normal Flow =====");
				Configuration.createNewWorkFlowReport("serviceRequestWithParts");
				createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(1));
				createServiceRequest.selectJobs();
				mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(1));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				trackLocation.giveEstimate(Utils.excelInputDataList.get(1), mechanic);
				trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(1));
				trackLocation.selectRetailer(Utils.excelInputDataList.get(1));
				approveEstimate.approveEstimateWithPayCash(Utils.excelInputDataList.get(1), fleetManager);
				startJob.startJobs(Utils.excelInputDataList.get(1), mechanic);
				fleetManager.viewBill(Utils.excelInputDataList.get(1));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(1));
				mechanic.mechanicRating();
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(1));

			}
		} catch (Exception e) {
		}
	}

	@Test
	public void SRWithPartsWithRetailerRevisedPartsNoRetailer() {

		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== service Request WithParts Revised Estimate With Parts no retailer =====");
				Configuration.createNewWorkFlowReport("SRWithPartsWithRetailerRevisedPartsNoRetailer");

				createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(1));
				createServiceRequest.selectJobs();
				mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(1));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				trackLocation.giveEstimate(Utils.excelInputDataList.get(1), mechanic);
				trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(1));
				trackLocation.selectRetailer(Utils.excelInputDataList.get(1));
				approveEstimate.approveEstimateWithPayCash(Utils.excelInputDataList.get(1), fleetManager);
				startJob.revisedEstimationWithParts(Utils.excelInputDataList.get(1), mechanic);
				trackLocation.withOutRetailer(Utils.excelInputDataList.get(1));
				approveEstimate.ApproveRevisedEstimateWithAlert(Utils.excelInputDataList.get(1), fleetManager);
				trackLocation.UpdateJobStatus(Utils.excelInputDataList.get(1), mechanic);
				fleetManager.viewBill(Utils.excelInputDataList.get(1));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(1));
				mechanic.mechanicRating();
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(1));
			}
		} catch (Exception e) {

		}
	}

	@Test
	public void serviceRequestWithPartsRevisedEstimateWithParts() {

		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== service Request WithParts Revised Estimate With Parts =====");
				Configuration.createNewWorkFlowReport("serviceRequestWithPartsRevisedEstimateWithParts");

				createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(1));
				createServiceRequest.selectJobs();
				mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(1));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				trackLocation.giveEstimate(Utils.excelInputDataList.get(1), mechanic);
				trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(1));
				trackLocation.selectRetailer(Utils.excelInputDataList.get(1));
				approveEstimate.approveEstimateWithPayCash(Utils.excelInputDataList.get(1), fleetManager);
				startJob.revisedEstimationWithParts(Utils.excelInputDataList.get(1), mechanic);
				trackLocation.selectRetailer(Utils.excelInputDataList.get(1));
				approveEstimate.ApproveRevisedEstimateWithPayCash(Utils.excelInputDataList.get(1), fleetManager);
				trackLocation.UpdateJobStatus(Utils.excelInputDataList.get(1), mechanic);
				fleetManager.viewBill(Utils.excelInputDataList.get(1));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(1));
				mechanic.mechanicRating();
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(1));

			}
		} catch (Exception e) {

		}
	}

	@Test
	public void serviceRequestWithoutPartsReviseEstimateWithParts() {

		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== service Request Without Parts Revise Estimate With Parts =====");
				Configuration.createNewWorkFlowReport("serviceRequestWithParts");
				createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(1));
				createServiceRequest.selectJobs();
				mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(1));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				trackLocation.giveEstimate(Utils.excelInputDataList.get(1), mechanic);
				trackLocation.trackLocationWithOutParts(Utils.excelInputDataList.get(1));
				startJob.revisedEstimationWithParts(Utils.excelInputDataList.get(1), mechanic);
				trackLocation.selectRetailer(Utils.excelInputDataList.get(1));
				approveEstimate.ApproveRevisedEstimateWithPayCash(Utils.excelInputDataList.get(1), fleetManager);
				trackLocation.UpdateJobStatus(Utils.excelInputDataList.get(1), mechanic);
				fleetManager.viewBill(Utils.excelInputDataList.get(1));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(1));
				mechanic.mechanicRating();
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(1));

			}
		} catch (Exception e) {

		}
	}

	@Test
	public void serviceRequestWithoutPartsRevisedWithoutParts() {

		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== service Request WithoutParts Revised Without Parts =====");
				Configuration.createNewWorkFlowReport("serviceRequestWithoutPartsRevisedWithoutParts");

				createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(1));
				createServiceRequest.selectJobs();
				mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(1));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				trackLocation.giveEstimate(Utils.excelInputDataList.get(1), mechanic);
				trackLocation.trackLocationWithOutParts(Utils.excelInputDataList.get(1));
				startJob.revisedEstimationWithoutParts(Utils.excelInputDataList.get(1), mechanic);
				approveEstimate.ApproveRevisedEstimateWithoutPay(Utils.excelInputDataList.get(1), fleetManager);
				trackLocation.UpdateJobStatus(Utils.excelInputDataList.get(1), mechanic);
				fleetManager.viewBill(Utils.excelInputDataList.get(1));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(1));
				mechanic.mechanicRating();
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(1));

			}
		} catch (Exception e) {

		}
	}

	@Test
	public void SRPartsNoRetailerRevisedwithPartsWithRetailer() {

		try {

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== SR With Parts NoRetailer Revised Estimate With Parts With Retailer =====");
				Configuration.createNewWorkFlowReport("SRPartsNoRetailerRevisedwithPartsWithRetailer");

				createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(1));
				createServiceRequest.selectJobs();
				mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(1));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				trackLocation.giveEstimate(Utils.excelInputDataList.get(1), mechanic);
				trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(1));
				trackLocation.withOutRetailer(Utils.excelInputDataList.get(1));
				approveEstimate.approveEstimateWithAlert(Utils.excelInputDataList.get(1), fleetManager);
				startJob.revisedEstimationWithParts(Utils.excelInputDataList.get(1), mechanic);
				trackLocation.selectRetailer(Utils.excelInputDataList.get(1));
				approveEstimate.ApproveRevisedEstimateWithPayCash(Utils.excelInputDataList.get(1), fleetManager);
				trackLocation.UpdateJobStatus(Utils.excelInputDataList.get(1), mechanic);
				fleetManager.viewBill(Utils.excelInputDataList.get(1));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(1));
				mechanic.mechanicRating();
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(1));
			}
		} catch (Exception e) {

		}
	}

	@Test
	public void SRWithPartsNoRetailerRevisedEstimateWithPartsNoRetailer() {

		try {

			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== SR With Parts NoRetailer Revised Estimate With Parts No Retailer =====");
				Configuration.createNewWorkFlowReport("SRWithPartsNoRetailerRevisedEstimateWithPartsNoRetailer");

				createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(1));
				createServiceRequest.selectJobs();
				mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(1));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				trackLocation.giveEstimate(Utils.excelInputDataList.get(1), mechanic);
				trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(1));
				trackLocation.withOutRetailer(Utils.excelInputDataList.get(1));
				approveEstimate.approveEstimateWithAlert(Utils.excelInputDataList.get(1), fleetManager);
				startJob.revisedEstimationWithParts(Utils.excelInputDataList.get(1), mechanic);
				trackLocation.withOutRetailer(Utils.excelInputDataList.get(1));
				approveEstimate.ApproveRevisedEstimateWithAlert(Utils.excelInputDataList.get(1), fleetManager);
				trackLocation.UpdateJobStatus(Utils.excelInputDataList.get(1), mechanic);
				fleetManager.viewBill(Utils.excelInputDataList.get(1));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(1));
				mechanic.mechanicRating();
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(1));

			}
		} catch (Exception e) {
		}
	}

	@Test
	public void SRPartsRevisedEstimateWithoutPartsRevisedBill() {

		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== service Request WithParts Revised Estimate WithOut Parts Revised bill =====");
				Configuration.createNewWorkFlowReport("SRPartsRevisedEstimateWithoutPartsRevisedBill");
				createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(1));
				createServiceRequest.selectJobs();
				mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(1));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				trackLocation.giveEstimate(Utils.excelInputDataList.get(1), mechanic);
				trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(1));
				trackLocation.selectRetailer(Utils.excelInputDataList.get(1));
				approveEstimate.approveEstimateWithPayCash(Utils.excelInputDataList.get(1), fleetManager);
				startJob.revisedEstimationWithoutParts(Utils.excelInputDataList.get(1), mechanic);
				approveEstimate.ApproveRevisedEstimateWithoutPay(Utils.excelInputDataList.get(1), fleetManager);
				trackLocation.UpdateJobStatusRevisedBill(Utils.excelInputDataList.get(1), mechanic);
				fleetManager.viewRevisedBill(Utils.excelInputDataList.get(1));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(1));
				mechanic.mechanicRating();
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(1));

			}
		} catch (Exception e) {

		}

	}

	@Test
	public void serviceRequestWithPartsRevisedEstimateWithOutParts() {

		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== service Request WithParts Revised Estimate WithOut Parts =====");
				Configuration.createNewWorkFlowReport("serviceRequestWithPartsRevisedEstimateWithOutParts");
				createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(1));
				createServiceRequest.selectJobs();
				mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(1));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				trackLocation.giveEstimate(Utils.excelInputDataList.get(1), mechanic);
				trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(1));
				trackLocation.selectRetailer(Utils.excelInputDataList.get(1));
				approveEstimate.approveEstimateWithPayCash(Utils.excelInputDataList.get(1), fleetManager);
				startJob.revisedEstimationWithoutParts(Utils.excelInputDataList.get(1), mechanic);
				approveEstimate.ApproveRevisedEstimateWithoutPay(Utils.excelInputDataList.get(1), fleetManager);
				trackLocation.UpdateJobStatus(Utils.excelInputDataList.get(1), mechanic);
				fleetManager.viewBill(Utils.excelInputDataList.get(1));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(1));
				mechanic.mechanicRating();
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(1));

			}
		} catch (Exception e) {

		}
	}

	@Test
	public void serviceRequestFMCancelAtConfirmGarage() {

		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Service Request Cancel At ConfirmGarage =====");
				Configuration.createNewWorkFlowReport("serviceRequestFMCancelAtConfirmGarage");
				createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(1));
				createServiceRequest.selectJobs();
				mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(1));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.cancelOrder();

			}
		} catch (Exception e) {

		}
	}

	@Test
	public void serviceRequestWithPartsWithOutRetailer() {

		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== service Reques tWithParts WithOut Retailer =====");
				Configuration.createNewWorkFlowReport("serviceRequestWithPartsWithOutRetailer");
				createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(1));
				createServiceRequest.selectJobs();
				mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(1));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				trackLocation.giveEstimate(Utils.excelInputDataList.get(1), mechanic);
				trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(1));
				trackLocation.withOutRetailer(Utils.excelInputDataList.get(1));
				approveEstimate.approveEstimateWithAlert(Utils.excelInputDataList.get(1), fleetManager);
				startJob.startJobs(Utils.excelInputDataList.get(1), mechanic);
				fleetManager.viewBill(Utils.excelInputDataList.get(1));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(1));
				mechanic.mechanicRating();
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(1));

			}
		} catch (Exception e) {

		}
	}

	@Test
	public void serviceRequestIDontKnowWhatsWrongWithParts() {

		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Service Request I Don't Know Whats Wrong with parts =====");
				Configuration.createNewWorkFlowReport("serviceRequestIDontKnowWhatsWrong");
				createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(1));
				createServiceRequest.selectUnKnowJobs();
				mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(1));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				trackLocation.giveEstimate(Utils.excelInputDataList.get(1), mechanic);
				trackLocation.trackLocationForUnknownIssueWithParts(Utils.excelInputDataList.get(1));
				trackLocation.selectRetailer(Utils.excelInputDataList.get(1));
				approveEstimate.approveEstimateWithPayCash(Utils.excelInputDataList.get(1), fleetManager);
				startJob.startJobs(Utils.excelInputDataList.get(1), mechanic);
				fleetManager.viewBill(Utils.excelInputDataList.get(1));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(1));
				mechanic.mechanicRating();
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(1));

			}
		} catch (Exception e) {

		}
	}

	@Test
	public void serviceRequestIDontKnowWhatsWrongWithOutParts() {

		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Service Request I Don't Know Whats Wrong With out parts =====");
				Configuration.createNewWorkFlowReport("serviceRequestIDontKnowWhatsWrongWithOutParts");
				createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(1));
				createServiceRequest.selectUnKnowJobs();
				mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(1));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				trackLocation.giveEstimate(Utils.excelInputDataList.get(1), mechanic);
				trackLocation.trackLocationForUnknownIssueWithOutParts(Utils.excelInputDataList.get(1));
				approveEstimate.approveEstimate(Utils.excelInputDataList.get(1), fleetManager);
				startJob.startJobs(Utils.excelInputDataList.get(1), mechanic);
				fleetManager.viewBill(Utils.excelInputDataList.get(1));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(1));
				mechanic.mechanicRating();
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(1));

			}
		} catch (Exception e) {

		}
	}

	@Test
	public void serviceRequestMechanicRejectsOrder() {

		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Service Request Mechanic Rejects Order =====");
				Configuration.createNewWorkFlowReport("serviceRequestMechanicRejectsOrder");
				createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(1));
				createServiceRequest.selectJobs();
				createServiceRequest.findGarage(Utils.excelInputDataList.get(1));
				mechanic.mechanicRejectOrder();

			}
		} catch (Exception e) {

		}
	}

	public void serviceRequestWithoutPartsRevisedBill() {

		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== service Request WithoutParts Revised Without Parts =====");
				Configuration.createNewWorkFlowReport("serviceRequestWithoutPartsRevisedWithoutParts");
				createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(1));
				createServiceRequest.selectJobs();
				mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(1));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				trackLocation.giveEstimate(Utils.excelInputDataList.get(1), mechanic);
				trackLocation.trackLocationWithOutParts(Utils.excelInputDataList.get(1));
				trackLocation.UpdateJobStatusRevisedBill(Utils.excelInputDataList.get(1), mechanic);
				fleetManager.viewRevisedBill(Utils.excelInputDataList.get(1));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(0));
				mechanic.mechanicRating();
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));

			}
		} catch (Exception e) {

		}
	}

	@Test
	public void SRWithoutPartsRevisedEstimatePartsRevisedBill() {

		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== service Request WithoutParts Revised Without Parts =====");
				Configuration.createNewWorkFlowReport("SRWithoutPartsRevisedEstimatePartsRevisedBill");
				createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(1));
				createServiceRequest.selectJobs();
				mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(1));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				trackLocation.giveEstimate(Utils.excelInputDataList.get(1), mechanic);
				trackLocation.trackLocationWithOutParts(Utils.excelInputDataList.get(1));
				startJob.revisedEstimationWithParts(Utils.excelInputDataList.get(1), mechanic);
				trackLocation.selectRetailer(Utils.excelInputDataList.get(1));
				approveEstimate.ApproveRevisedEstimateWithPayCash(Utils.excelInputDataList.get(1), fleetManager);
				trackLocation.UpdateJobStatusRevisedBill(Utils.excelInputDataList.get(1), mechanic);
				fleetManager.viewRevisedBill(Utils.excelInputDataList.get(1));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(1));
				mechanic.mechanicRating();
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(1));

			}
		} catch (Exception e) {

		}

	}

	@Test
	public void SRWithPartsRevisedEstimateWithPartsRevisedBill() {

		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== service Request WithParts Revised Estimate WithOut Parts Revised Bill =====");
				Configuration.createNewWorkFlowReport("SRWithPartsRevisedEstimateWithPartsRevisedBill");
				createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(1));
				createServiceRequest.selectJobs();
				mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(1));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				trackLocation.giveEstimate(Utils.excelInputDataList.get(1), mechanic);
				trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(1));
				trackLocation.selectRetailer(Utils.excelInputDataList.get(1));
				approveEstimate.approveEstimateWithPayCash(Utils.excelInputDataList.get(1), fleetManager);
				startJob.revisedEstimationWithParts(Utils.excelInputDataList.get(1), mechanic);
				trackLocation.selectRetailer(Utils.excelInputDataList.get(1));
				approveEstimate.ApproveRevisedEstimateWithPayCash(Utils.excelInputDataList.get(1), fleetManager);
				trackLocation.UpdateJobStatusRevisedBill(Utils.excelInputDataList.get(1), mechanic);
				fleetManager.viewRevisedBill(Utils.excelInputDataList.get(1));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(1));
				mechanic.mechanicRating();
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(1));

			}
		} catch (Exception e) {

		}
	}

	@Test
	public void SRFMCancelAllJobsInApproveEstimate() {

		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Service Request Cancel All Jobs in Approve Estimate =====");
				Configuration.createNewWorkFlowReport("SRFMCancelAllJobsInApproveEstimate");
				createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(1));
				createServiceRequest.selectMultiJob(Utils.excelInputDataList.get(1));
				mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(1));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				trackLocation.giveEstimate(Utils.excelInputDataList.get(1), mechanic);
				trackLocation.tackLocationMultiJobWithParts(Utils.excelInputDataList.get(1));
				trackLocation.selectRetailer(Utils.excelInputDataList.get(1));
				approveEstimate.approveEstimateCancelAllJobs(Utils.excelInputDataList.get(1), fleetManager);

			}
		} catch (Exception e) {

		}
	}

	@Test
	public void SRCreateNewRequestFromMyVehicle() {

		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== FleetManager Add New Vehicle with new request =====");
				Configuration.createNewWorkFlowReport("fleetManagerCreateNewRequestFromMyVehicle");
				mProfileCreation.addNewVehicle();
				createServiceRequest.createServiceRequestFromMyVehicle(Utils.excelInputDataList.get(1));
				createServiceRequest.selectJobs();
				mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(1));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				trackLocation.giveEstimate(Utils.excelInputDataList.get(1), mechanic);
				trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(1));
				trackLocation.selectRetailer(Utils.excelInputDataList.get(1));
				approveEstimate.approveEstimateWithPayCash(Utils.excelInputDataList.get(1), fleetManager);
				startJob.startJobs(Utils.excelInputDataList.get(1), mechanic);
				fleetManager.viewBill(Utils.excelInputDataList.get(1));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(1));
				mechanic.mechanicRating();
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(1));

			}
		} catch (Exception e) {

		}
	}

	@Test
	public void SRFMCancelOneJobInApproveEstimate() {

		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Job Cancel In Approve Estimate =====");
				Configuration.createNewWorkFlowReport("SRFMCancelOneJobInApproveEstimate");
				createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(1));
				createServiceRequest.selectJobs();
				mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(1));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				trackLocation.giveEstimate(Utils.excelInputDataList.get(1), mechanic);
				trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(1));
				trackLocation.selectRetailer(Utils.excelInputDataList.get(1));
				approveEstimate.approveEstimateCancelOrder(Utils.excelInputDataList.get(1), fleetManager);

			}
		} catch (Exception e) {

		}
	}

	@Test
	public void SRFMDeletesAtTrackLocation() {
		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("=====SRFMDeletesAtTrackLoc 59=====");
				Configuration.createNewWorkFlowReport("SRFMDeletesAtTrackLoc 59");
				createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(1));
				createServiceRequest.selectJobs();
				mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(1));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				fleetManager.cancelOrder();
			}
		} catch (Exception e) {

		}
	}

	@Test
	public void SRMechanicDeletesAtTrackLoc() {
		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("=====SRMechanicDeletesAtTrackLoc 57=====");
				Configuration.createNewWorkFlowReport("SRMechanicDeletesAtTrackLoc 57");
				createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(1));
				createServiceRequest.selectJobs();
				mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(1));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				mechanic.mechanicCancelOrder();
				Configuration.saveReport();
			}
		} catch (Exception e) {

		}
	}

	@Test
	public void SRFMDeletesAtFindGarage() {
		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("=====SRFMDeletesAtFindGarage 48=====");
				Configuration.createNewWorkFlowReport("SRFMDeletesAtFindGarage 48");
				createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(1));
				createServiceRequest.selectJobs();
				mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(1));
				fleetManager.cancelOrder();
			}
		} catch (Exception e) {
		}
	}

	@Test
	public void SRFMDeletesAtApproveEstimate() {
		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("=====SRFMDeletesAtApproveEstimate 61=====");
				Configuration.createNewWorkFlowReport("SRFMDeletesAtApproveEstimate 61");
				createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(1));
				createServiceRequest.selectJobs();
				mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(1));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				trackLocation.giveEstimate(Utils.excelInputDataList.get(1), mechanic);
				trackLocation.trackLocationWithParts(Utils.excelInputDataList.get(1));
				trackLocation.selectRetailer(Utils.excelInputDataList.get(1));
				fleetManager.cancelOrder();
			}
		} catch (Exception e) {

		}
	}

	@Test
	public void SRDeleteOneJobByMechanic() {
		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("=====SRDeleteOneJobbyMechanic 43 =====");
				Configuration.createNewWorkFlowReport("SRDeleteOneJobByMechanic 43");
				createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(1));
				createServiceRequest.selectMultiJob(Utils.excelInputDataList.get(1));
				mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(1));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				trackLocation.giveEstimate(Utils.excelInputDataList.get(1), mechanic);
				trackLocation.tackLocationMultiJobWithPartsWithoutSubmit(Utils.excelInputDataList.get(1));
				trackLocation.deleteJobwithSubmitEstimate(Utils.excelInputDataList.get(1), mechanic);
				trackLocation.selectRetailer(Utils.excelInputDataList.get(1));
				approveEstimate.approveEstimateWithPayCash(Utils.excelInputDataList.get(1), fleetManager);
				startJob.startJobs(Utils.excelInputDataList.get(1), mechanic);
				fleetManager.viewBill(Utils.excelInputDataList.get(1));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(1));
				mechanic.mechanicRating();
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(1));
				Configuration.saveReport();

			}
		} catch (Exception e) {

		}
	}

	@Test
	public void SRDeleteOneJobAndAddOneNewJobByMechanic() {
		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("=====SRDeleteOneJobAndAddOneNewJobByMechanic 44=====");
				Configuration.createNewWorkFlowReport("SRDeleteOneJobAndAddOneNewJobByMechanic 44");
				createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(1));
				createServiceRequest.selectMultiJob(Utils.excelInputDataList.get(1));
				mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(1));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				trackLocation.giveEstimate(Utils.excelInputDataList.get(1), mechanic);
				trackLocation.DeletejobwithoutSubmitEstimate(Utils.excelInputDataList.get(1), mechanic);
				trackLocation.trackLocationWithAddOneNewJob(Utils.excelInputDataList.get(1));
				approveEstimate.approveEstimateCancelOneJobs(Utils.excelInputDataList.get(1), fleetManager);
				startJob.startJobs(Utils.excelInputDataList.get(1), mechanic);
				fleetManager.viewBill(Utils.excelInputDataList.get(1));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(1));
				mechanic.mechanicRating();
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(1));
				Configuration.saveReport();

			}
		} catch (Exception e) {

		}
	}

	@Test
	public void SRMultiJobsFMCancelOneJobInApproveEstimate() {
		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("=====SRbyFMEstimatewithPartsRetailerCancelJobs 38=====");
				Configuration.createNewWorkFlowReport("SRMultiJobsFMCancelOneJobInApproveEstimate 38");
				createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(1));
				createServiceRequest.selectMultiJob(Utils.excelInputDataList.get(1));
				mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(1));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				trackLocation.giveEstimate(Utils.excelInputDataList.get(1), mechanic);
				trackLocation.tackLocationMultiJobWithParts(Utils.excelInputDataList.get(1));
				trackLocation.selectRetailer(Utils.excelInputDataList.get(1));
				approveEstimate.approveEstimateCancelOneJobsWithPayCash(Utils.excelInputDataList.get(1), fleetManager);
				startJob.startJobs(Utils.excelInputDataList.get(1), mechanic);
				fleetManager.viewBill(Utils.excelInputDataList.get(1));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(1));
				mechanic.mechanicRating();
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(1));
				Configuration.saveReport();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void SRWithPartsWithOutRetailerCancelOneJob() {

		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("=====SRWithPartsWithOutRetailerCancelOnejob 39=====");
				Configuration.createNewWorkFlowReport("SRWithPartsWithOutRetailerCancelOnejob 39");
				createServiceRequest.createServiceRequest(Utils.excelInputDataList.get(1));
				createServiceRequest.selectMultiJob(Utils.excelInputDataList.get(1));
				mRequestOrderId = createServiceRequest.findGarage(Utils.excelInputDataList.get(1));
				mechanic.mechanicAcceptOrder(mRequestOrderId);
				trackLocation.giveEstimate(Utils.excelInputDataList.get(1), mechanic);
				trackLocation.tackLocationMultiJobWithParts(Utils.excelInputDataList.get(1));
				trackLocation.withOutRetailer(Utils.excelInputDataList.get(1));
				approveEstimate.approveEstimateCancelOneJobsWithPayCashWithAlert(Utils.excelInputDataList.get(1),
						fleetManager);
				startJob.startJobs(Utils.excelInputDataList.get(0), mechanic);
				fleetManager.viewBill(Utils.excelInputDataList.get(0));
				mechanic.confirmCashReceipt(Utils.excelInputDataList.get(0));
				mechanic.mechanicRating();
				fleetManager.fleetManagerRating(Utils.excelInputDataList.get(0));
				Configuration.saveReport();
			}
		} catch (Exception e) {

		}
	}
}
