package com.servicemandi.workflow;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;
import com.servicemandi.common.Configuration;
import com.servicemandi.common.FleetManager;
import com.servicemandi.common.LocatorPath;
import com.servicemandi.common.Mechanic;
import com.servicemandi.common.ProfileCreation;
import com.servicemandi.utils.ReadingExcelData;
import com.servicemandi.utils.Utils;
import com.sevicemandi.servicerequest.CreateServiceRequest;

/**
 * Created by Shanthakumar on 01-Feb-18.
 */

public class UserCreation {

	private FleetManager mFleetManager;
	private Mechanic mMechanic;
	private ProfileCreation mProfileCreation;
	private Configuration mConfiguration;
	private CreateServiceRequest mCreateServiceRequest;

	@BeforeTest
	public void setUpConfiguration() {

		try {
			ReadingExcelData.readingWorkFlowData();
			mFleetManager = new FleetManager();
			mMechanic = new Mechanic();
			mProfileCreation = new ProfileCreation();
			mConfiguration = new Configuration();
			mCreateServiceRequest = new CreateServiceRequest();
		} catch (Exception e) {
		}
	}

	@Test
	public void FleetManagerUserEdit() {

		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== FleetManager User Edit =====");
				Configuration.createNewWorkFlowReport("FleetManagerUserEdit");
				mFleetManager.login(Utils.excelInputDataList.get(1));
				mProfileCreation.editUserProfile();
				Configuration.saveReport();

			}
		} catch (Exception e) {

		}
	}

	@Test
	public void createNewFMProfile() {

		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== FleetManager User creation  =====");
				Configuration.createNewWorkFlowReport("createNewFMProfile");
				mProfileCreation.fleetManageUserCreation(Utils.excelInputDataList.get(1));
				Utils.sleepTimeLong();
				mConfiguration.fleetManagerWait(LocatorPath.prifileCreateAddVehicle);
				mConfiguration.clickFMElement(Configuration.LocatorType.XPATH, LocatorPath.prifileCreateAddVehicle);
				mProfileCreation.createNewVehicle();
				Utils.sleepTimeMedium();
				mConfiguration.fleetManagerWait(LocatorPath.myDriverPath);
				mConfiguration.clickFMElement(Configuration.LocatorType.XPATH, LocatorPath.myDriverPath);
				mProfileCreation.addNewDriver();
				Utils.sleepTimeLow();
				mConfiguration.fleetManagerWait(LocatorPath.profileDonePath);
				mConfiguration.clickFMElement(Configuration.LocatorType.XPATH, LocatorPath.profileDonePath);
				Configuration.saveReport();

			}
		} catch (Exception e) {

		}
	}

	@Test
	public void createMechanicProfile() {

		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== Machanic User creation  =====");
				Configuration.createNewWorkFlowReport("createMechanicProfile");
				mProfileCreation.machanicUserCreation(Utils.excelInputDataList.get(1));
				Configuration.saveReport();

			}
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	@Test
	public void FleetManagerAddNewVehicle() {

		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== FleetManager Add New Vehicle =====");
				Configuration.createNewWorkFlowReport("FleetManagerAddNewVehicle");
				mFleetManager.login(Utils.excelInputDataList.get(1));
				mProfileCreation.addNewVehicle();
				Configuration.saveReport();
			}
		} catch (Exception e) {

		}
	}

	@Test
	public void mechanicRateCardChange() {

		try {
			if (Utils.excelInputDataList != null && Utils.excelInputDataList.size() > 0) {
				System.out.println("===== FleetManager Add New Vehicle =====");
				Configuration.createNewWorkFlowReport("FleetManagerAddNewVehicle");
				mMechanic.login(Utils.excelInputDataList.get(1));
				mMechanic.mechanicRateCard();
				Configuration.saveReport();
			}
		} catch (Exception e) {

		}
	}
}
