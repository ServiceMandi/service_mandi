package com.sevicemandi.servicerequest;

import com.relevantcodes.extentreports.LogStatus;
import com.servicemandi.common.Configuration;
import com.servicemandi.common.LocatorPath;
import com.servicemandi.utils.ExcelInputData;

import io.appium.java_client.android.AndroidKeyCode;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

/**
 * Created by Shanthakumar on 29-01-2018.
 */

public class CreateServiceRequest extends Configuration {

	public CreateServiceRequest() {

	}

	public void createServiceRequest(ExcelInputData excelInputData) {

		try {
			Thread.sleep(5000);
			System.out.println("Entered Create New Request method ::: ");
			fleetManagerWait(LocatorPath.createNewRequest);
			clickFMElement(LocatorType.XPATH, LocatorPath.createNewRequest);
			Thread.sleep(2000);
			fleetManagerWait(LocatorPath.selectVehicle);
			clickFMElement(LocatorType.XPATH, LocatorPath.selectVehicle);
			enterFMValue(LocatorType.XPATH, LocatorPath.selectVehicle, excelInputData.getVehicleNumber());
			Thread.sleep(1000);
			clickFMElement(LocatorType.XPATH, LocatorPath.selectVehicleNo);
			System.out.println("Entered Create New Request vehicle selected  ::: ");
			fleetManagerWait(LocatorPath.selectDriver);
			clickFMElement(LocatorType.XPATH, LocatorPath.selectDriver);
			enterFMValue(LocatorType.XPATH, LocatorPath.selectDriver, excelInputData.getDriverName());
			Thread.sleep(1000);
			clickFMElement(LocatorType.XPATH, LocatorPath.selectDriverNo);
			System.out.println("Entered Create New Request driver selected  ::: ");
			fleetManagerWait(LocatorPath.selectLocation);
			clickFMElement(LocatorType.XPATH, LocatorPath.selectLocation);
			enterFMValue(LocatorType.XPATH, LocatorPath.selectLocation, excelInputData.getLocation());
			Thread.sleep(1000);
			hideKeyBoard(fleetManagerDriver);
			clickFMElement(LocatorType.XPATH, LocatorPath.selectLocationValue);
			System.out.println("Entered Create New Request location selected  ::: ");

			fleetManagerWait(LocatorPath.serviceRepairsButton);
			clickFMElement(LocatorType.XPATH, LocatorPath.serviceRepairsButton);
			fleetManagerWait(LocatorPath.serviceYesBtn);
			clickFMElement(LocatorType.XPATH, LocatorPath.serviceYesBtn);
			//String alreadyInprogress=getFMWebElement(LocatorType.XPATH, locator)
			writeReport(LogStatus.PASS, "New Service request created");
		} catch (Exception e) {
			writeReport(LogStatus.FAIL, "New Service request created failed");
		}
	}

	public void createServiceRequestFromMyVehicle(ExcelInputData excelInputData) {

		try {
			Thread.sleep(5000);
			Thread.sleep(5000);
			System.out.println("Entered Create New Request method ::: ");

			fleetManagerWait(LocatorPath.newRequestFromMyVehicles);
			clickFMElement(LocatorType.XPATH, LocatorPath.newRequestFromMyVehicles);
			Thread.sleep(3000);
			fleetManagerWait(LocatorPath.selectDriver);
			clickFMElement(LocatorType.XPATH, LocatorPath.selectDriver);
			enterFMValue(LocatorType.XPATH, LocatorPath.selectDriver, excelInputData.getDriverName());
			Thread.sleep(1000);
			clickFMElement(LocatorType.XPATH, LocatorPath.selectDriverNo);
			System.out.println("Entered Create New Request driver selected  ::: ");
			fleetManagerWait(LocatorPath.selectLocation);
			clickFMElement(LocatorType.XPATH, LocatorPath.selectLocation);
			enterFMValue(LocatorType.XPATH, LocatorPath.selectLocation, excelInputData.getLocation());
			Thread.sleep(1000);
			hideKeyBoard(fleetManagerDriver);
			clickFMElement(LocatorType.XPATH, LocatorPath.selectLocationValue);
			System.out.println("Entered Create New Request location selected  ::: ");

			fleetManagerWait(LocatorPath.serviceRepairsButton);
			clickFMElement(LocatorType.XPATH, LocatorPath.serviceRepairsButton);
			fleetManagerWait(LocatorPath.serviceYesBtn);
			clickFMElement(LocatorType.XPATH, LocatorPath.serviceYesBtn);
			writeReport(LogStatus.PASS, "New Service request created");
		} catch (Exception e) {
			writeReport(LogStatus.FAIL, "New Service request created failed");
		}
	}

	public void selectUnKnowJobs() {

		try {

			Thread.sleep(3000);
			fleetManagerWait(LocatorPath.selectJob);
			fleetManagerWait(LocatorPath.iDontKnowPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.iDontKnowPath);
			fleetManagerWait(LocatorPath.searchGaragesPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.searchGaragesPath);
			writeReport(LogStatus.PASS, "Service request I don't Whats wrong Jab Selected");
		} catch (Exception e) {

		}
	}

	public void selectJobs() {

		try {

			Thread.sleep(3000);
			fleetManagerWait(LocatorPath.selectJob);
			clickFMElement(LocatorType.XPATH, LocatorPath.selectJob);
			fleetManagerWait(LocatorPath.acJobSelectPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.acJobSelectPath);
			fleetManagerWait(LocatorPath.searchGaragesPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.searchGaragesPath);
			writeReport(LogStatus.PASS, "Service request A/C Service Job Selected");
		} catch (Exception e) {
			writeReport(LogStatus.FAIL, "Service request Service Job Selected failed ");
		}
	}

	public void selectMultiJob(ExcelInputData excelInputData) {

		try {

			fleetManagerWait(LocatorPath.selectJob);
			int loopSize = Integer.parseInt(excelInputData.getMultiJobCount()) + 2;
			System.out.println("AL Multi Job  count ::" + loopSize);
			for (int i = 2; i < loopSize; i++) {

				String jobPath = "//ion-content/div[2]/ion-list/div[" + i + "]/ion-item/div[1]/div/ion-label";
				WebElement jobElement = fleetManagerDriver.findElement(By.xpath(jobPath));
				jobElement.click();
				Thread.sleep(500);

				String jobSelectedPath = "//ion-content/div[2]/ion-list/div[" + i + "]/div/ion-list/ion-item[1]";
				WebElement jobSelectedElement = fleetManagerDriver.findElement(By.xpath(jobSelectedPath));
				jobSelectedElement.click();
				writeReport(LogStatus.PASS, jobSelectedElement.getText() + " --> Job's Selected");
				Thread.sleep(500);
			}

			Thread.sleep(2000);
			fleetManagerWait(LocatorPath.searchGaragesPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.searchGaragesPath);
			writeReport(LogStatus.PASS, "Service request :-" + excelInputData.getMultiJobCount() + " Job's Selected");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String findGarage(ExcelInputData excelInputData) {

		try {
			String orderId = "";
			Thread.sleep(1000);
			fleetManagerWait(LocatorPath.garageListPath);
			List<WebElement> garageTotalList = fleetManagerDriver.findElements(By.xpath(LocatorPath.garageListPath));
			System.out.println("AL Total Garage list size :::" + garageTotalList.size());
			boolean ifFindGarage = false;
			for (int i = 1; i <= garageTotalList.size(); i++) {

				if (!ifFindGarage) {
					String garagePathList = "//*[@id=\"garagebox\"]/div[" + i + "]/*/*/*";
					List<WebElement> garageList = fleetManagerDriver.findElements(By.xpath(garagePathList));
					for (int g = 0; g < garageList.size(); g++) {
						String garageName = garageList.get(g).getText();
						System.out.println("AL garage name ::: " + garageName);
						if (garageName.equalsIgnoreCase(excelInputData.getSelectGarage())) {
							writeReport(LogStatus.PASS,
									excelInputData.getSelectGarage() + " ::: Service request Garage Selected");
							garageTotalList.get((i - 1)).click();
							Thread.sleep(1000);
							ifFindGarage = true;
							break;
						}
					}
				} else
					break;
			}

			fleetManagerWait(LocatorPath.confirmGaragePath);
			clickFMElement(LocatorType.XPATH, LocatorPath.confirmGaragePath);
			writeReport(LogStatus.PASS, "Service request confirm garage");
			fleetManagerWait(LocatorPath.requestOrderIdPath);
			orderId = getFMWebElement(LocatorType.XPATH, LocatorPath.requestOrderIdPath).getText().replaceAll("[^0-9]",
					"");

			writeReport(LogStatus.PASS, "Service Request Order Id ::" + orderId);
			System.out.println(" AL Request number :::" + orderId);
			fleetManagerWait(LocatorPath.manageOrder);
			clickFMElement(LocatorType.XPATH, LocatorPath.manageOrder);
			System.out.println("Al Manage other order clicked");
			Thread.sleep(1000);
			return orderId;
		} catch (Exception e) {
			writeReport(LogStatus.FAIL, "Service request confirm garage failed");
		}
		return "";
	}

	public void findNoGarage(ExcelInputData excelInputData) throws InterruptedException {

		try {

			System.out.println(" User garage name :::" + excelInputData.getSelectGarage());
			String totalGarageListPath = "//*[@id=\"garagebox\"]/*";
			fleetManagerWait(totalGarageListPath);
			List<WebElement> webElements = fleetManagerDriver.findElements(By.xpath(totalGarageListPath));
			boolean ifFindGarage = false;
			for (int i = 1; i <= webElements.size(); i++) {

				if (!ifFindGarage) {
					String garagePathList = "//*[@id=\"garagebox\"]/div[" + i + "]/*/*/*";
					List<WebElement> garageList = fleetManagerDriver.findElements(By.xpath(garagePathList));

					for (int g = 0; g < garageList.size(); g++) {
						String garageName = garageList.get(g).getText();
						if (garageName.equalsIgnoreCase(excelInputData.getSelectGarage())) {
							webElements.get((i - 1)).click();
							writeReport(LogStatus.PASS, "FM: SR -:: " + garageName + " -> selected");
							Thread.sleep(1000);
							ifFindGarage = true;
							break;
						}
					}
				} else
					break;
				; // else break;
			}

			String confirmGaragePath = "//html/body/ion-app/ng-component/ion-nav/page-select-garage/ion-footer/button";
			fleetManagerWait(LocatorPath.confirmGaragePath);
			clickFMElement(LocatorType.XPATH, confirmGaragePath);

			// Garage does not exists validation:

			fleetManagerWait(LocatorPath.confirmGaragePath);
			String PlsSelectGarageMsg = "/html/body/ion-app/ion-alert/div/div[1]";
			fleetManagerWait(PlsSelectGarageMsg);
			String PlsSelectGarageMsg1 = fleetManagerDriver.findElement(By.xpath(PlsSelectGarageMsg)).getText();
			WebElement AlertMsg1 = fleetManagerDriver.findElement(By.xpath(PlsSelectGarageMsg));
			if (AlertMsg1.isDisplayed()) {// Msg can be taken to excel
				System.out.println("Inside Garage Alert" + PlsSelectGarageMsg1);
				fleetManagerDriver.pressKeyCode(AndroidKeyCode.ENTER);
				Thread.sleep(2000);
				fleetManagerDriver.pressKeyCode(AndroidKeyCode.BACK);
				writeReport(LogStatus.PASS, "FM: Garage Not Selected - Verified");
			}

		} catch (Exception e) {
			writeReport(LogStatus.FAIL, "FM: Garage Selected");
		}
	}

}
