package com.sevicemandi.servicerequest;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.relevantcodes.extentreports.LogStatus;
import com.servicemandi.common.Configuration;
import com.servicemandi.common.FleetManager;
import com.servicemandi.common.LocatorPath;
import com.servicemandi.common.Configuration.LocatorType;
import com.servicemandi.utils.ExcelInputData;
import com.servicemandi.utils.Utils;

/**
 * Created by Shanthakumar on 30-01-2018.
 */

public class ServiceRequestApproveEstimate extends Configuration {

	public ServiceRequestApproveEstimate() {

	}

	public void approveEstimate(ExcelInputData excelInputData, FleetManager fleetManager) {

		try {

			fleetManager.selectLiveOrderSearch(excelInputData.getVehicleNumber());
			System.out.println("Entered Approve Estimate method ::");
			Utils.sleepTimeLow();
			fleetManagerWait(LocatorPath.approveEstimatePath);
			clickFMElement(LocatorType.XPATH, LocatorPath.approveEstimatePath);
			writeReport(LogStatus.PASS, "Fleet manager approve the estimate");
			Thread.sleep(2000);
			Thread.sleep(2000);
			writeReport(LogStatus.PASS, "Fleet manager select pay cash method");
			fleetManagerWait(LocatorPath.liveOrderTab);

		} catch (Exception e) {

		}
	}

	public void approveEstimateWithPayCash(ExcelInputData excelInputData, FleetManager fleetManager) {

		try {

			fleetManager.selectLiveOrderSearch(excelInputData.getVehicleNumber());
			System.out.println("Entered Approve Estimate method ::");
			Thread.sleep(3000);
			fleetManagerWait(LocatorPath.approveEstimatePath);
			clickFMElement(LocatorType.XPATH, LocatorPath.approveEstimatePath);
			writeReport(LogStatus.PASS, "Fleet manager approve the estimate");
			Thread.sleep(2000);
			fleetManagerWait(LocatorPath.payCashButtonPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.payCashButtonPath);
			Thread.sleep(2000);
			writeReport(LogStatus.PASS, "Fleet manager select pay cash method");
			fleetManagerWait(LocatorPath.liveOrderTab);

		} catch (Exception e) {

		}
	}

	public void ApproveRevisedEstimateWithPayCash(ExcelInputData excelInputData, FleetManager fleetManager) {

		try {

			fleetManager.selectLiveOrderSearch(excelInputData.getVehicleNumber());
			System.out.println("Entered Approve Revised Estimate Without Pay method ::");
			Thread.sleep(3000);
			fleetManagerWait(LocatorPath.approveRevisedEstimatePath);
			clickFMElement(LocatorType.XPATH, LocatorPath.approveRevisedEstimatePath);
			Thread.sleep(2000);
			fleetManagerWait(LocatorPath.payCashButtonPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.payCashButtonPath);
			fleetManagerWait(LocatorPath.liveOrderTab);

		} catch (Exception e) {

		}
	}

	public void ApproveRevisedEstimateWithoutPay(ExcelInputData excelInputData, FleetManager fleetManager) {

		try {

			fleetManager.selectLiveOrderSearch(excelInputData.getVehicleNumber());
			System.out.println("Entered Approve Revised Estimate Without Pay method ::");
			Utils.sleepTimeLow();
			fleetManagerWait(LocatorPath.approveRevisedEstimatePath);
			clickFMElement(LocatorType.XPATH, LocatorPath.approveRevisedEstimatePath);
			fleetManagerWait(LocatorPath.liveOrderTab);

		} catch (Exception e) {

		}
	}

	public void approveEstimateWithAlert(ExcelInputData excelInputData, FleetManager fleetManager) {

		try {

			fleetManager.selectLiveOrderSearch(excelInputData.getVehicleNumber());
			System.out.println("Entered Approve Estimate method ::");
			Thread.sleep(2000);
			fleetManagerWait(LocatorPath.approveEstimatePath);
			clickFMElement(LocatorType.XPATH, LocatorPath.approveEstimatePath);
			writeReport(LogStatus.PASS, "Fleet manager approve the estimate");
			Thread.sleep(2000);
			fleetManagerWait(LocatorPath.noRetailerAlertPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.noRetailerAlertPath);
			Thread.sleep(2000);
			writeReport(LogStatus.PASS, "Fleet manager select pay cash method");
			fleetManagerWait(LocatorPath.liveOrderTab);

		} catch (Exception e) {

		}
	}

	public void ApproveRevisedEstimateWithAlert(ExcelInputData excelInputData, FleetManager fleetManager) {

		try {

			fleetManager.selectLiveOrderSearch(excelInputData.getVehicleNumber());
			System.out.println("Entered Approve Revised Estimate Without Pay method ::");
			Thread.sleep(1000);
			fleetManagerWait(LocatorPath.approveRevisedEstimatePath);
			clickFMElement(LocatorType.XPATH, LocatorPath.approveRevisedEstimatePath);
			Thread.sleep(2000);
			fleetManagerWait(LocatorPath.noRetailerAlertPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.noRetailerAlertPath);
			Thread.sleep(2000);
			writeReport(LogStatus.PASS, "Fleet manager select pay cash method");
			fleetManagerWait(LocatorPath.liveOrderTab);

		} catch (Exception e) {

		}
	}

	public void approveEstimateCancelOrder(ExcelInputData excelInputData, FleetManager fleetManager) {

		try {
			fleetManager.selectLiveOrderSearch(excelInputData.getVehicleNumber());
			System.out.println("Entered Approve Estimate method ::");
			Thread.sleep(3000);
			fleetManagerWait(LocatorPath.approveEstimatePath);
			// check delete button and click
			clickFMElement(LocatorType.XPATH, LocatorPath.approveEstimateCancelJob);
			fleetManagerWait(LocatorPath.approveEstimateCancelButton);
			Thread.sleep(1000);
			clickFMElement(LocatorType.XPATH, LocatorPath.approveEstimateCancelButton);
			Thread.sleep(1000);
			fleetManagerWait(LocatorPath.serviceYesBtn);
			clickFMElement(LocatorType.XPATH, LocatorPath.serviceYesBtn);

		} catch (Exception e) {

		}
	}

	public void approveEstimateCancelAllJobs(ExcelInputData excelInputData, FleetManager fleetManager) {

		try {

			fleetManager.selectLiveOrderSearch(excelInputData.getVehicleNumber());
			System.out.println("Entered Approve Estimate method ::");
			Thread.sleep(5000);
			fleetManagerWait(LocatorPath.approveEstimatePath);
			System.out.println("Al Enter the cancel job method :::");
			int loopSize = Integer.parseInt(excelInputData.getMultiJobCount());
			for (int i = 1; i <= loopSize; i++) {

				String cancelCheckBox = "//ion-content/div[2]/ion-grid/div/ion-list/ion-row[" + i
						+ "]/ion-col[4]/ion-checkbox/div";
				System.out.println("AL cancel jobs path ::" + cancelCheckBox);
				WebElement cancelElement = fleetManagerDriver.findElement(By.xpath(cancelCheckBox));
				Actions action = new Actions(fleetManagerDriver);
				action.moveToElement(cancelElement).click().perform();
				Thread.sleep(500);
			}

			clickFMElement(LocatorType.XPATH, LocatorPath.approveEstimatePath);
			fleetManagerWait(LocatorPath.serviceYesBtn);
			clickFMElement(LocatorType.XPATH, LocatorPath.serviceYesBtn);
			writeReport(LogStatus.PASS, "Fleet manager cancel all jobs");
			fleetManagerWait(LocatorPath.liveOrderTab);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void approveEstimateCancelOneJobs(ExcelInputData excelInputData, FleetManager fleetManager) {

		try {

			fleetManager.selectLiveOrderSearch(excelInputData.getVehicleNumber());
			System.out.println("Entered Approve Estimate method ::");
			Thread.sleep(3000);
			fleetManagerWait(LocatorPath.approveEstimatePath);
			System.out.println("Al Enter the cancel job method :::");

			String cancelCheckBox = "//ion-content/div[2]/ion-grid/div/ion-list/ion-row[1]/ion-col[4]/ion-checkbox/div";
			System.out.println("AL cancel jobs path ::" + cancelCheckBox);
			WebElement cancelElement = fleetManagerDriver.findElement(By.xpath(cancelCheckBox));
			Actions action = new Actions(fleetManagerDriver);
			action.moveToElement(cancelElement).click().perform();
			Thread.sleep(500);

			clickFMElement(LocatorType.XPATH, LocatorPath.approveEstimatePath);
			writeReport(LogStatus.PASS, "Fleet manager cancel one jobs");
			fleetManagerWait(LocatorPath.liveOrderTab);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void approveEstimateCancelOneJobsWithPayCashWithAlert(ExcelInputData excelInputData,
			FleetManager fleetManager) {

		try {

			fleetManager.selectLiveOrderSearch(excelInputData.getVehicleNumber());
			System.out.println("Entered Approve Estimate method ::");
			Thread.sleep(3000);
			fleetManagerWait(LocatorPath.approveEstimatePath);
			System.out.println("Al Enter the cancel job method :::");

			String cancelCheckBox = "//ion-content/div[2]/ion-grid/div/ion-list/ion-row[1]/ion-col[4]/ion-checkbox/div";
			WebElement cancelElement = fleetManagerDriver.findElement(By.xpath(cancelCheckBox));
			Actions action = new Actions(fleetManagerDriver);
			action.moveToElement(cancelElement).click().perform();
			Thread.sleep(500);

			clickFMElement(LocatorType.XPATH, LocatorPath.approveEstimatePath);
			fleetManagerWait(LocatorPath.payCashButtonPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.payCashButtonPath);

			Thread.sleep(2000);
			fleetManagerWait(LocatorPath.noRetailerAlertPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.noRetailerAlertPath);

			fleetManagerWait(LocatorPath.liveOrderTab);
			writeReport(LogStatus.PASS, "Fleet manager cancel one jobs");
			fleetManagerWait(LocatorPath.liveOrderTab);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void approveEstimateCancelOneJobsWithPayCash(ExcelInputData excelInputData, FleetManager fleetManager) {

		try {

			fleetManager.selectLiveOrderSearch(excelInputData.getVehicleNumber());
			System.out.println("Entered Approve Estimate method ::");
			Thread.sleep(3000);
			fleetManagerWait(LocatorPath.approveEstimatePath);
			System.out.println("Al Enter the cancel job method :::");

			String cancelCheckBox = "//ion-content/div[2]/ion-grid/div/ion-list/ion-row[1]/ion-col[4]/ion-checkbox/div";
			WebElement cancelElement = fleetManagerDriver.findElement(By.xpath(cancelCheckBox));
			Actions action = new Actions(fleetManagerDriver);
			action.moveToElement(cancelElement).click().perform();
			Thread.sleep(500);

			clickFMElement(LocatorType.XPATH, LocatorPath.approveEstimatePath);
			fleetManagerWait(LocatorPath.payCashButtonPath);
			clickFMElement(LocatorType.XPATH, LocatorPath.payCashButtonPath);
			fleetManagerWait(LocatorPath.liveOrderTab);
			writeReport(LogStatus.PASS, "Fleet manager cancel one jobs");
			fleetManagerWait(LocatorPath.liveOrderTab);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
