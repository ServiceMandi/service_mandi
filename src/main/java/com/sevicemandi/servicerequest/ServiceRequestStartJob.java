package com.sevicemandi.servicerequest;

import com.relevantcodes.extentreports.LogStatus;
import com.servicemandi.common.Configuration;
import com.servicemandi.common.LocatorPath;
import com.servicemandi.common.Mechanic;
import com.servicemandi.utils.ExcelInputData;
import com.servicemandi.utils.Utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

import io.appium.java_client.android.AndroidKeyCode;

/**
 * Created by Shanthakumar on 30-01-2018.
 */

public class ServiceRequestStartJob extends Configuration {

	public ServiceRequestStartJob() {

	}

	public void startJobs(ExcelInputData excelInputData, Mechanic mechanic) {

		try {
			mechanic.onGoingOrderPathClick(excelInputData.getVehicleNumber());
			Utils.sleepTimeLow();
			System.out.println("Enter the service request start jobs method :::");
			mechanicWait(LocatorPath.jobsCompletePath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.jobsCompletePath);
			writeReport(LogStatus.PASS, "Mechanic jobs completed");
			Utils.sleepTimeLow();
			mechanicWait(LocatorPath.jobSubmitButtonPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.jobSubmitButtonPath);
			Thread.sleep(1000);
			mechanicWait(LocatorPath.alertOkPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.alertOkPath);
			writeReport(LogStatus.PASS, "Mechanic jobs submitted");
		} catch (Exception e) {
		}
	}

	public void revisedEstimationWithParts(ExcelInputData excelInputData, Mechanic mechanic) {

		try {

			mechanic.onGoingOrderPathClick(excelInputData.getVehicleNumber());
			Thread.sleep(2000);
			System.out.println("Enter the service request start jobs method :::");

			mechanicWait(LocatorPath.addJobsLinkPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.addJobsLinkPath);
			Thread.sleep(2000);
			mechanicWait(LocatorPath.selectJobPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.selectJobPath);
			Thread.sleep(1000);
			mechanicWait(LocatorPath.totalJobListPath);
			List<WebElement> jobList = mechanicDriver.findElements(By.xpath(LocatorPath.totalJobListPath));

			for (int i = 0; i < jobList.size(); i++) {
				String selectJob = jobList.get(i).getText();
				System.out.println("Inside for loop::::" + selectJob);
				if (selectJob.equalsIgnoreCase(excelInputData.getBdJobSelection())) {
					jobList.get(i).click();
					writeReport(LogStatus.PASS, "Mechanic select job:-" + excelInputData.getBdJobSelection());
					Thread.sleep(1000);
					break;
				}
			}

			Thread.sleep(1000);
			mechanicDriver.pressKeyCode(AndroidKeyCode.ENTER);
			Thread.sleep(1000);

			// EnterLabour:
			String labourPath = "//*[@id=\"nav\"]/page-addjobs/ion-content/div[2]/ion-card[2]/div/ion-row[2]/ion-col[2]/input";

			mechanicWait(labourPath);
			Thread.sleep(1000);
			clickMechanicElement(LocatorType.XPATH, labourPath);
			enterValueMechanic(LocatorType.XPATH, labourPath, excelInputData.getLabourAmount());
			Thread.sleep(1000);

			// EnterParts:
			String partsPath = "//*[@id=\"nav\"]/page-addjobs/ion-content/div[2]/ion-card[2]/div/ion-row[2]/ion-col[3]/input";
			mechanicWait(partsPath);
			Thread.sleep(1000);
			clickMechanicElement(LocatorType.XPATH, partsPath);
			Thread.sleep(1000);
			enterValueMechanic(LocatorType.XPATH, partsPath, excelInputData.getEnterParts());
			hideKeyBoard(mechanicDriver);

			mechanicWait(LocatorPath.submitRevisedEstimatePath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.submitRevisedEstimatePath);

			writeReport(LogStatus.PASS, " Revised Estimate with patrs " + excelInputData.getEnterParts() + " entered ");

		} catch (Exception e) {

		}
	}

	public void revisedEstimationWithoutParts(ExcelInputData excelInputData, Mechanic mechanic) {

		try {

			mechanic.onGoingOrderPathClick(excelInputData.getVehicleNumber());
			Thread.sleep(3000);
			System.out.println("Enter the service request start jobs method :::");

			mechanicWait(LocatorPath.addJobsLinkPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.addJobsLinkPath);
			Thread.sleep(2000);
			mechanicWait(LocatorPath.selectJobPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.selectJobPath);
			Thread.sleep(1000);
			mechanicWait(LocatorPath.totalJobListPath);
			List<WebElement> jobList = mechanicDriver.findElements(By.xpath(LocatorPath.totalJobListPath));

			for (int i = 0; i < jobList.size(); i++) {
				String selectJob = jobList.get(i).getText();
				System.out.println("Inside for loop::::" + selectJob);
				if (selectJob.equalsIgnoreCase(excelInputData.getBdJobSelection())) {
					jobList.get(i).click();
					writeReport(LogStatus.PASS, "Mechanic select --> Brake Work jobs ");
					Thread.sleep(1000);
					break;
				}
			}

			Thread.sleep(1000);
			mechanicDriver.pressKeyCode(AndroidKeyCode.ENTER);
			Thread.sleep(1000);

			// EnterLabour:
			String labourPath = "//*[@id=\"nav\"]/page-addjobs/ion-content/div[2]/ion-card[2]/div/ion-row[2]/ion-col[2]/input";
			mechanicWait(labourPath);
			Thread.sleep(1000);
			clickMechanicElement(LocatorType.XPATH, labourPath);
			enterValueMechanic(LocatorType.XPATH, labourPath, excelInputData.getLabourAmount());
			Thread.sleep(1000);
			hideKeyBoard(mechanicDriver);
			Thread.sleep(2000);

			/*
			 * mechanicWait(LocatorPath.trackLocationPartsPath);
			 * clickMechanicElement(LocatorType.XPATH,
			 * LocatorPath.trackLocationPartsPath);
			 * enterValueMechanic(LocatorType.XPATH,
			 * LocatorPath.trackLocationPartsPath,
			 * excelInputData.getEnterParts()); hideKeyBoard(mechanicDriver);
			 * writeReport(LogStatus.PASS, "Mechanic enter the parts value " +
			 * getMechanicWebElement(LocatorType.XPATH,
			 * LocatorPath.trackLocationPartsPath).getText());
			 */

			mechanicWait(LocatorPath.submitRevisedEstimatePath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.submitRevisedEstimatePath);
			System.out.println("Enter the Break down parts :::");
			writeReport(LogStatus.PASS, " Revised Estimate with out patrs entered ");
			/*
			 * Utils.sleepTimeLow();
			 * mechanicWait(LocatorPath.revisedPaymentRequestOk);
			 * clickMechanicElement(LocatorType.XPATH,
			 * LocatorPath.revisedPaymentRequestOk); Thread.sleep(3000);
			 */

		} catch (Exception e) {

		}
	}
}
