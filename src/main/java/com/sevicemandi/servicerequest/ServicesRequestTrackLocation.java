package com.sevicemandi.servicerequest;

import com.relevantcodes.extentreports.LogStatus;
import com.servicemandi.common.Configuration;
import com.servicemandi.common.LocatorPath;
import com.servicemandi.common.Mechanic;
import com.servicemandi.common.Configuration.LocatorType;
import com.servicemandi.utils.ExcelInputData;
import com.servicemandi.utils.Utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

import io.appium.java_client.android.AndroidKeyCode;

/**
 * Created by Shanthakumar on 30-01-2018.
 */

public class ServicesRequestTrackLocation extends Configuration {

	public ServicesRequestTrackLocation() {

	}

	public void giveEstimate(ExcelInputData excelInputData, Mechanic mechanic) {

		try {
			Utils.sleepTimeLow();
			System.out.println("Al Enter the give Estimate method :::");
			mechanic.onGoingOrderPathClick(excelInputData.getVehicleNumber());
			Thread.sleep(1000);
			mechanicWait(LocatorPath.giveEstimatePath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.giveEstimatePath);
			Thread.sleep(1000);
			writeReport(LogStatus.PASS, "Mechanic Track location completed ");
		} catch (Exception e) {
			writeReport(LogStatus.FAIL, "Mechanic Track location failed ");
		}
	}
	public void trackLocationForUnknownIssueWithOutParts(ExcelInputData excelInputData) {

		try {

			Thread.sleep(3000);
			mechanicWait(LocatorPath.selectJobPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.selectJobPath);
			Thread.sleep(1000);

			mechanicWait(LocatorPath.totalJobListPath);
			List<WebElement> jobList = mechanicDriver.findElements(By.xpath(LocatorPath.totalJobListPath));
			for (int i = 0; i < jobList.size(); i++) {
				String selectJob = jobList.get(i).getText();
				System.out.println("Inside for loop::::" + selectJob);
				if (selectJob.equalsIgnoreCase(excelInputData.getBdJobSelection())) {
					jobList.get(i).click();
					writeReport(LogStatus.PASS, "Mechanic select --> " + excelInputData.getBdJobSelection());
					Thread.sleep(1000);
					break;
				}
			}

			Thread.sleep(1000);
			mechanicDriver.pressKeyCode(AndroidKeyCode.ENTER);
			Thread.sleep(1000);

			mechanicWait(LocatorPath.trackLocationLabourPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.trackLocationLabourPath);
			enterValueMechanic(LocatorType.XPATH, LocatorPath.trackLocationLabourPath,
					excelInputData.getLabourAmount());
			Thread.sleep(1000);
			hideKeyBoard(mechanicDriver);
			writeReport(LogStatus.PASS, "Mechanic enter the labour amount "
					+ getMechanicWebElement(LocatorType.XPATH, LocatorPath.trackLocationLabourPath).getText());

			mechanicWait(LocatorPath.partsSubmitPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.partsSubmitPath);
			System.out.println("Enter the Break down parts :::");
			writeReport(LogStatus.PASS, "Service request with out parts ");

		} catch (Exception e) {
		}
	}

	public void trackLocationForUnknownIssueWithParts(ExcelInputData excelInputData) {

		try {
			Utils.sleepTimeLow();
			System.out.println("Al Enter the trackLocationForUnknownIssueWithParts method :: ");
			mechanicWait(LocatorPath.selectJobPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.selectJobPath);
			Thread.sleep(1000);
			mechanicWait(LocatorPath.totalJobListPath);

			List<WebElement> jobList = mechanicDriver.findElements(By.xpath(LocatorPath.totalJobListPath));
			for (int i = 0; i < jobList.size(); i++) {
				String selectJob = jobList.get(i).getText();
				System.out.println("Inside for loop::::" + selectJob);
				if (selectJob.equalsIgnoreCase(excelInputData.getBdJobSelection())) {
					jobList.get(i).click();
					writeReport(LogStatus.PASS, "Mechanic select --> " + excelInputData.getBdJobSelection());
					Thread.sleep(1000);
					break;
				}
			}

			Thread.sleep(1000);
			mechanicDriver.pressKeyCode(AndroidKeyCode.ENTER);
			Thread.sleep(1000);

			mechanicWait(LocatorPath.trackLocationLabourPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.trackLocationLabourPath);
			enterValueMechanic(LocatorType.XPATH, LocatorPath.trackLocationLabourPath,
					excelInputData.getLabourAmount());
			Thread.sleep(1000);
			writeReport(LogStatus.PASS, "Mechanic enter the labour amount "
					+ getMechanicWebElement(LocatorType.XPATH, LocatorPath.trackLocationLabourPath).getText());

			mechanicWait(LocatorPath.trackLocationPartsPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.trackLocationPartsPath);
			enterValueMechanic(LocatorType.XPATH, LocatorPath.trackLocationPartsPath, excelInputData.getEnterParts());
			hideKeyBoard(mechanicDriver);
			writeReport(LogStatus.PASS, "Mechanic enter the parts value "
					+ getMechanicWebElement(LocatorType.XPATH, LocatorPath.trackLocationPartsPath).getText());

			mechanicWait(LocatorPath.partsSubmitPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.partsSubmitPath);
			System.out.println("Enter the Break down parts :::");
			writeReport(LogStatus.PASS, "Service request parts " + excelInputData.getEnterParts() + " entered ");

		} catch (Exception e) {

		}
	}

	public void trackLocationWithParts(ExcelInputData excelInputData) {

		try {
			System.out.println("Al Enter the track location with parts method :::");
			Thread.sleep(2000);
			mechanicWait(LocatorPath.trackLocationPartsPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.trackLocationPartsPath);
			Thread.sleep(1000);
			enterValueMechanic(LocatorType.XPATH, LocatorPath.trackLocationPartsPath, excelInputData.getEnterParts());
			hideKeyBoard(mechanicDriver);
			mechanicWait(LocatorPath.partsSubmitPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.partsSubmitPath);
			System.out.println("Enter the service request parts :::");
			writeReport(LogStatus.PASS, "Service request parts " + excelInputData.getEnterParts() + " entered ");
		} catch (Exception e) {
			writeReport(LogStatus.PASS, "Mechanic Track location failed ");
		}
	}

	public void trackLocationWithOutParts(ExcelInputData excelInputData) {

		try {
			System.out.println("Al Enter the track location with out parts method :::");
			Utils.sleepTimeLow();
			mechanicWait(LocatorPath.trackLocationPartsPath);
			/*
			 * clickMechanicElement(LocatorType.XPATH, LocatorPath.trackLocationPartsPath);
			 * enterValueMechanic(LocatorType.XPATH, LocatorPath.trackLocationPartsPath,
			 * excelInputData.getEnterParts()); hideKeyBoard(mechanicDriver);
			 */
			mechanicWait(LocatorPath.partsSubmitPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.partsSubmitPath);
			System.out.println("Enter the service request parts :::");
			writeReport(LogStatus.PASS, "Service request with out parts ");
		} catch (Exception e) {
			writeReport(LogStatus.PASS, "Mechanic Track location failed ");
		}
	}

	public void tackLocationMultiJobWithParts(ExcelInputData excelInputData) {

		try {
			System.out.println("Al Enter the track location with parts method :::");
			Utils.sleepTimeLow();
			mechanicWait(LocatorPath.trackLocationPartsPath);
			int loopSize = Integer.parseInt(excelInputData.getMultiJobCount());
			for (int i = 1; i <= loopSize; i++) {

				String partsPath = "//div[2]/ion-card[1]/div[" + i + "]/ion-row[2]/ion-col[3]/input";
				WebElement partsElement = mechanicDriver.findElement(By.xpath(partsPath));
				partsElement.clear();
				partsElement.clear();
				partsElement.sendKeys(excelInputData.getEnterParts());
				Thread.sleep(500);

			}
			hideKeyBoard(mechanicDriver);
			Thread.sleep(2000);
			mechanicWait(LocatorPath.partsSubmitPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.partsSubmitPath);
			System.out.println("Enter the service request parts :::");
			writeReport(LogStatus.PASS, "Service request parts " + excelInputData.getEnterParts() + " entered ");
		} catch (Exception e) {
			writeReport(LogStatus.PASS, "Mechanic Track location failed ");
		}

	}

	public void tackLocationMultiJobWithPartsWithoutSubmit(ExcelInputData excelInputData) {

		try {
			System.out.println("Al Enter the track location with parts method :::");
			Utils.sleepTimeLow();
			mechanicWait(LocatorPath.trackLocationPartsPath);
			int loopSize = Integer.parseInt(excelInputData.getMultiJobCount());
			for (int i = 1; i <= loopSize; i++) {

				String partsPath = "//div[2]/ion-card[1]/div[" + i + "]/ion-row[2]/ion-col[3]/input";
				WebElement partsElement = mechanicDriver.findElement(By.xpath(partsPath));
				partsElement.clear();
				partsElement.clear();
				partsElement.sendKeys(excelInputData.getEnterParts());
				Thread.sleep(500);

			}
			hideKeyBoard(mechanicDriver);
			Utils.sleepTimeLow();
			mechanicWait(LocatorPath.partsSubmitPath);
			/*
			 * clickMechanicElement(LocatorType.XPATH, LocatorPath.partsSubmitPath);
			 * System.out.println("Enter the service request parts :::");
			 * writeReport(LogStatus.PASS, "Service request parts " +
			 * excelInputData.getEnterParts() + " entered ");
			 */
		} catch (Exception e) {
			writeReport(LogStatus.PASS, "Mechanic Track location failed ");
		}

	}

	public void withOutRetailer(ExcelInputData excelInputData) {

		try {

			Utils.sleepTimeLow();
			System.out.println("Enter the service request retailer method :::");
			mechanicWait(LocatorPath.retailersListPath);
			mechanicClassPathWait(LocatorPath.noRetailerPath);
			clickMechanicElement(LocatorType.CLASS, LocatorPath.noRetailerPath);
			mechanicWait(LocatorPath.retailerConfirmPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.retailerConfirmPath);
			writeReport(LogStatus.PASS, "Mechanic with out retailer confirmed ");
		} catch (Exception e) {
			writeReport(LogStatus.PASS, "Mechanic retailer selection failed ");
		}
	}

	public void selectRetailer(ExcelInputData excelInputData) {

		try {
			Utils.sleepTimeLow();
			Thread.sleep(5000);
			System.out.println("Enter the service request retailer method :::");
			mechanicWait(LocatorPath.retailersListPath);
			List<WebElement> retailersList = mechanicDriver.findElements(By.xpath(LocatorPath.retailersListPath));
			int loopSize = retailersList.size() - 3;

			boolean isClicked = false;
			for (int i = 1; i <= loopSize; i++) {
				String insideRetailerPath = "//*[@id=\"nav\"]/page-select-retailer/ion-content/div[2]/ion-card[" + i
						+ "]" + "/*";
				List<WebElement> retailersInsideList = mechanicDriver.findElements(By.xpath(insideRetailerPath));

				if (!isClicked) {
					for (int j = 0; j < retailersInsideList.size(); j++) {
						System.out.println("Al retailer name :::" + retailersInsideList.get(j).getText());
						if (retailersInsideList.get(j).getText().equalsIgnoreCase(excelInputData.getSelectRetailer())) {
							retailersInsideList.get((j + 1)).click();
							writeReport(LogStatus.PASS, "Service request retailer :-  "
									+ excelInputData.getSelectRetailer() + " Selected ");
							isClicked = true;
							break;
						}
					}
				} else {
					break;
				}
			}

			mechanicWait(LocatorPath.retailerConfirmPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.retailerConfirmPath);
			writeReport(LogStatus.PASS, "Mechanic retailer confirmed ");

		} catch (Exception e) {
			writeReport(LogStatus.PASS, "Mechanic retailer selection failed ");
		}
	}

	public void UpdateJobStatusRevisedBill(ExcelInputData excelInputData, Mechanic mechanic)
			throws InterruptedException {

		mechanic.onGoingOrderPathClick(excelInputData.getVehicleNumber());
		Thread.sleep(5000);
		System.out.println("Enter the service request Update Job Status method :::");
		mechanicWait(LocatorPath.completeJobPath);
		clickMechanicElement(LocatorType.XPATH, LocatorPath.completeJobPath);
		Thread.sleep(5000);

		/*
		 * mechanicWait(LocatorPath.jobSubmitButtonPath); Thread.sleep(1000); WebElement
		 * revisedBill =
		 * mechanicDriver.findElement(By.xpath(LocatorPath.revisedBillPath)) ;
		 * revisedBill.clear(); revisedBill.clear(); revisedBill.sendKeys("2000");
		 */

		Thread.sleep(1000);
		WebElement revisedBillElement = mechanicDriver
				.findElement(By.xpath(LocatorPath.revisedEstimateRevisedBillPath));
		revisedBillElement.clear();
		revisedBillElement.clear();
		revisedBillElement.sendKeys(excelInputData.getRevisedBillValue2());
		writeReport(LogStatus.PASS, "Mechanic - Revised bill ");

		mechanicWait(LocatorPath.submitBillPath);
		clickMechanicElement(LocatorType.XPATH, LocatorPath.submitBillPath);
		Thread.sleep(2000);
		mechanicWait(LocatorPath.paymentRequestOk);
		clickMechanicElement(LocatorType.XPATH, LocatorPath.paymentRequestOk);

	}

	public void UpdateJobStatus(ExcelInputData excelInputData, Mechanic mechanic) {

		try {

			mechanic.onGoingOrderPathClick(excelInputData.getVehicleNumber());
			Utils.sleepTimeLow();
			System.out.println("Enter the service request Update Job Status method :::");
			mechanicWait(LocatorPath.completeJobPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.completeJobPath);
			Utils.sleepTimeLow();
			System.out.println("Enter the machanic submit bill method :::");
			mechanicWait(LocatorPath.submitBillPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.submitBillPath);
			Thread.sleep(2000);
			mechanicWait(LocatorPath.paymentRequestOk);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.paymentRequestOk);

		} catch (Exception e) {
			writeReport(LogStatus.PASS, "Mechanic update job failed ");
		}
	}

	public void deleteJobwithSubmitEstimate(ExcelInputData excelInputData, Mechanic mechanic) {

		try {

			System.out.println("Entered the Delete jobs method :::");
			String deleteicon = "//div[1]/ion-row[2]/ion-col[3]/ion-icon";
			mechanicWait(deleteicon);
			System.out.println("deleteicon:");
			clickMechanicElement(LocatorType.XPATH, deleteicon);
			String YES = "/html/body/ion-app/ion-alert/div/div[3]/button[1]";
			mechanicWait(YES);
			clickMechanicElement(LocatorType.XPATH, YES);
			Thread.sleep(1000);
			mechanicWait(LocatorPath.partsSubmitPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.partsSubmitPath);
			writeReport(LogStatus.PASS, "Mechanic: One Job is Deleted");
		} catch (Exception e) {
			writeReport(LogStatus.FAIL, "Mechanic: One Job is not Deleted");
		}
	}

	public void DeletejobwithoutSubmitEstimate(ExcelInputData excelInputData, Mechanic mechanic) {

		try {
			Utils.sleepTimeLow();
			System.out.println("Entered the Delete jobs method :::");
			mechanicWait(LocatorPath.trackLocationPartsPath);
			String deleteicon = "//div[1]/ion-row[2]/ion-col[3]/ion-icon";
			mechanicWait(deleteicon);
			System.out.println("deleteicon:");
			clickMechanicElement(LocatorType.XPATH, deleteicon);
			Thread.sleep(1000);
			String YES = "//html/body/ion-app/ion-alert/div/div[3]/button[1]";
			mechanicWait(YES);
			clickMechanicElement(LocatorType.XPATH, YES);
			writeReport(LogStatus.PASS, "Mechanic: One Job is Deleted");
		} catch (Exception e) {
			writeReport(LogStatus.FAIL, "Mechanic: One Job is not Deleted");
		}
	}

	public void trackLocationWithAddOneNewJob(ExcelInputData excelInputData) throws InterruptedException {
		try {

			Thread.sleep(3000);
			// String addJobPath =
			// "//*[@id=\"nav\"]/page-addjobs/ion-footer/ion-row";
			String addJobPath = "//*[contains(text(), 'ADD JOBS')]";
			mechanicWait(addJobPath);
			System.out.println("AL enter the add job link  ");
			clickMechanicElement(LocatorType.XPATH, addJobPath);
			Thread.sleep(3000);
			// Select Job:
			mechanicWait(LocatorPath.selectJobPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.selectJobPath);
			Thread.sleep(1000);
			mechanicWait(LocatorPath.totalJobListPath);
			List<WebElement> jobList = mechanicDriver.findElements(By.xpath(LocatorPath.totalJobListPath));

			for (int i = 0; i < jobList.size(); i++) {
				String selectJob = jobList.get(i).getText();
				System.out.println("Inside for loop::::" + selectJob);
				if (selectJob.equalsIgnoreCase(excelInputData.getBdJobSelection())) {
					jobList.get(i).click();
					writeReport(LogStatus.PASS, "Mechanic select job:-" + excelInputData.getBdJobSelection());
					Thread.sleep(1000);
					break;
				}
			}

			Thread.sleep(1000);
			mechanicDriver.pressKeyCode(AndroidKeyCode.ENTER);
			Thread.sleep(1000);

			// EnterLabour:
			String labourPath = "//*[@id=\"nav\"]/page-addjobs/ion-content/div[2]/ion-card[2]/div/ion-row[2]/ion-col[2]/input";

			mechanicWait(labourPath);
			Thread.sleep(1000);
			clickMechanicElement(LocatorType.XPATH, labourPath);
			enterValueMechanic(LocatorType.XPATH, labourPath, excelInputData.getLabourAmount());
			Thread.sleep(1000);
			Thread.sleep(2000);
			mechanicWait(LocatorPath.partsSubmitPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.partsSubmitPath);
			System.out.println("Mechanic::::Added One job :::");
			writeReport(LogStatus.PASS, "Mechanic Added One job passed ");
		} catch (Exception e) {
			writeReport(LogStatus.FAIL, "Mechanic Added One job failed ");
		}
	}

	public void trackLocationWithMultiJobs(ExcelInputData excelInputData) {

		try {

			Thread.sleep(3000);
			mechanicWait(LocatorPath.selectJobPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.selectJobPath);
			Thread.sleep(1000);

			for (int j = 0; j < 4; j++) {

				mechanicWait(LocatorPath.totalJobListPath);
				List<WebElement> jobList = mechanicDriver.findElements(By.xpath(LocatorPath.totalJobListPath));

				for (int i = 0; i < jobList.size(); i++) {
					String selectJob = jobList.get(i).getText();
					System.out.println("Inside for loop::::" + selectJob);

					if (i == j) {
						jobList.get(i).click();
						Thread.sleep(500);
						writeReport(LogStatus.PASS, "Mechanic select job :- " + jobList.get(i).getText());
						break;
					}
				}

				Thread.sleep(500);
				mechanicDriver.pressKeyCode(AndroidKeyCode.ENTER);
				Thread.sleep(500);

				mechanicWait(LocatorPath.trackLocationLabourPath);
				clickMechanicElement(LocatorType.XPATH, LocatorPath.trackLocationLabourPath);
				enterValueMechanic(LocatorType.XPATH, LocatorPath.trackLocationLabourPath,
						excelInputData.getLabourAmount());
				Thread.sleep(500);
				writeReport(LogStatus.PASS, "Mechanic enter the labour amount "
						+ getMechanicWebElement(LocatorType.XPATH, LocatorPath.trackLocationLabourPath).getText());

				mechanicWait(LocatorPath.trackLocationPartsPath);
				clickMechanicElement(LocatorType.XPATH, LocatorPath.trackLocationPartsPath);
				enterValueMechanic(LocatorType.XPATH, LocatorPath.trackLocationPartsPath,
						excelInputData.getEnterParts());
				hideKeyBoard(mechanicDriver);
				writeReport(LogStatus.PASS, "Mechanic enter the parts value "
						+ getMechanicWebElement(LocatorType.XPATH, LocatorPath.trackLocationPartsPath).getText());

				if (j < 3) {
					Thread.sleep(1000);
					String addJobButton = "//*[@id=\"nav\"]/page-addjobs/ion-footer/ion-row";
					clickMechanicElement(LocatorType.XPATH, addJobButton);
					Thread.sleep(2000);
					mechanicWait(LocatorPath.selectJobPath);
					clickMechanicElement(LocatorType.XPATH, LocatorPath.selectJobPath);
				}
			}

			Thread.sleep(1000);
			mechanicWait(LocatorPath.partsSubmitPath);
			clickMechanicElement(LocatorType.XPATH, LocatorPath.partsSubmitPath);
			Thread.sleep(3000);

		} catch (Exception e) {

		}
	}
}
