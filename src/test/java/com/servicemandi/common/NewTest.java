package com.servicemandi.common;

import java.io.File;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.servicemandi.common.Configuration.LocatorType;
import com.servicemandi.utils.Utils;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;

public class NewTest extends Configuration {
	private static DesiredCapabilities desiredCapabilities;
	public static AndroidDriver fleetManagerDriver=null;
	public static AndroidDriver mechanicDriver = null;
	protected static WebDriverWait fmWait = null;
	public static String destDir;
	static DateFormat dateFormat;
	
	
  @Test
  public void fleetManager() throws InterruptedException {
	  destDir = "screenshots";
	  System.out.println("Start Take ScreenShot");
	  File scrFile = ((TakesScreenshot) fleetManagerDriver).getScreenshotAs(OutputType.FILE);
	  System.out.println("Inprogress Take ScreenShot");
	  dateFormat = new SimpleDateFormat("dd-MMM-yyyy__hh_mm_ssaa");
	  new File(destDir).mkdirs();
	  String destFile = dateFormat.format(new Date()) + ".png";
	  try {
	   FileUtils.copyFile(scrFile, new File(destDir+"FleetManager"+ "/" + destFile));
	   System.out.println("Start Take ScreenShot Completed for fleet manager");
	  } catch (Exception e) {
	   e.printStackTrace();
	  }
  }
  @Test
  public void mechenich() throws InterruptedException {	
	  destDir = "screenshots";
	  System.out.println("Start Take ScreenShot");
	  File scrFile = ((TakesScreenshot) mechanicDriver).getScreenshotAs(OutputType.FILE);
	  System.out.println("Inprogress Take ScreenShot");
	  dateFormat = new SimpleDateFormat("dd-MMM-yyyy__hh_mm_ssaa");
	  new File(destDir).mkdirs();
	  String destFile = dateFormat.format(new Date()) + ".png";
	  try {
	   FileUtils.copyFile(scrFile, new File(destDir+"Mechenic"+ "/" + destFile));
	   System.out.println("Start Take ScreenShot Completed for fleet manager");
	  } catch (Exception e) {
	   e.printStackTrace();
	  }
  }
  @BeforeMethod
  public void beforeMethod() {
	  try {
			desiredCapabilities = new DesiredCapabilities();
			desiredCapabilities.setCapability("deviceName", "Samsung SM-J120G"); // Fleet
																					// Manager
			desiredCapabilities.setCapability("appium-version", "1.7.1");
			desiredCapabilities.setCapability(MobileCapabilityType.UDID, "32011c38412e3479");
			// Santa
			// desiredCapabilities.setCapability(MobileCapabilityType.UDID,
			// "42005e66ec7524d3");
			// desiredCapabilities.setCapability(MobileCapabilityType.UDID,
			// "420015a3b2fa1449"); // Priya
			// FM
			desiredCapabilities.setCapability("platformName", "Android");
			desiredCapabilities.setCapability("platformVersion", "5.1.1");
			desiredCapabilities.setCapability("recreateChromeDriverSessions", "true");
			desiredCapabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, "100000");
			desiredCapabilities.setCapability("appPackage", "com.servicemandi.fm");
			desiredCapabilities.setCapability("appActivity", "com.servicemandi.fm.MainActivity");
			desiredCapabilities.setCapability("automationName", "uiautomator2");
			desiredCapabilities.setCapability("autoAcceptAlerts", true);
			desiredCapabilities.setCapability("autoDismissAlerts", true);
			fleetManagerDriver = new AndroidDriver(new URL("http://0.0.0.0:4723/wd/hub"), desiredCapabilities);

			fleetManagerDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			fmWait = new WebDriverWait(fleetManagerDriver, 120);

		} catch (Exception e) {
		}
	  try {

			desiredCapabilities = new DesiredCapabilities();
			desiredCapabilities.setCapability("deviceName", "Samsung SM-J120G"); // Mechanic
																					// App
			desiredCapabilities.setCapability("appium-version", "1.7.1");
			// Deepa
			desiredCapabilities.setCapability(MobileCapabilityType.UDID, "4200145bf0e11499");

			// Santa
			// desiredCapabilities.setCapability(MobileCapabilityType.UDID,
			// "4200d9904ed114f3");

			// Priya Mech
			// desiredCapabilities.setCapability(MobileCapabilityType.UDID,
			// "42005e40ec9e2415");
			desiredCapabilities.setCapability("platformName", "Android");
			desiredCapabilities.setCapability("platformVersion", "5.1.1");
			desiredCapabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, "100000");
			desiredCapabilities.setCapability("recreateChromeDriverSessions", "true");
			desiredCapabilities.setCapability("appPackage", "com.servicemandi.mechanic");
			desiredCapabilities.setCapability("appActivity", "com.servicemandi.mechanic.MainActivity");
			desiredCapabilities.setCapability("automationName", "uiautomator2");
			desiredCapabilities.setCapability("autoAcceptAlerts", true);
			desiredCapabilities.setCapability("autoDismissAlerts", true);
			mechanicDriver = new AndroidDriver(new URL("http://127.0.0.1:4273/wd/hub"), desiredCapabilities);
			mechanicDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			//mechWait = new WebDriverWait(mechanicDriver, 120);
		} catch (Exception e) {
		}
	
  }

  @AfterMethod
  public void afterMethod() {
  }

}
