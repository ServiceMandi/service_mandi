package com.servicemandi.cucumber.Framework.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(features ="src/test/resources/featurefile/ServiceMandi.feature")
public class ServiceMandiRunner extends AbstractTestNGCucumberTests {

}
